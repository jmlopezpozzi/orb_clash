builddir = ./
bind = $(builddir)bin/
srcd = $(builddir)src/
srcd_adt = $(srcd)adt/
srcd_editor = $(srcd)editor/
copt = -Wall -Wextra -std=c99 -pedantic


########
# MAIN
########

orb_clash:\
 $(bind)main.o $(bind)readline.o \
 $(bind)io_files.o $(bind)io_files_text.o \
 $(bind)clash.o $(bind)clash_mode.o \
 $(bind)player_class.o \
 $(bind)linked_list.o \
 $(bind)player_classes_adt.o \
 $(bind)player_character_adt.o $(bind)pch_list_adt.o \
 $(bind)player_party_adt.o $(bind)party_list_adt.o \
 $(bind)spell_adt.o $(bind)spell_list_adt.o \
 $(bind)orb_adt.o $(bind)orb_list_adt.o \
 $(bind)g_item_adt.o $(bind)item_list_adt.o \
 $(bind)bag_adt.o $(bind)bag_list_adt.o \
 $(bind)editor_public.o \
 $(bind)classes_chars.o \
 $(bind)character_class_editor.o $(bind)character_list_editor.o \
 $(bind)player_party_editor.o  \
 $(bind)spells_orbs.o $(bind)spell_list_editor.o $(bind)orbs_manager.o \
 $(bind)items_bags.o $(bind)item_list_editor.o $(bind)bag_manager.o \

	gcc -o orb_clash $(bind)main.o $(bind)readline.o $(bind)io_files.o $(bind)io_files_text.o $(bind)clash.o $(bind)clash_mode.o $(bind)player_class.o $(bind)linked_list.o $(bind)player_classes_adt.o $(bind)player_character_adt.o $(bind)pch_list_adt.o $(bind)player_party_adt.o $(bind)party_list_adt.o $(bind)spell_adt.o $(bind)spell_list_adt.o $(bind)orb_adt.o $(bind)orb_list_adt.o $(bind)g_item_adt.o $(bind)item_list_adt.o $(bind)bag_adt.o $(bind)bag_list_adt.o $(bind)editor_public.o $(bind)classes_chars.o $(bind)character_class_editor.o $(bind)character_list_editor.o $(bind)player_party_editor.o $(bind)spells_orbs.o $(bind)spell_list_editor.o $(bind)orbs_manager.o $(bind)items_bags.o $(bind)item_list_editor.o $(bind)bag_manager.o

$(bind)main.o:\
 $(srcd)main.c \
 $(srcd)player_class.h \
 $(srcd)io_files.h $(srcd)readline.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)party_list_adt.h \
 $(srcd_adt)spell_list_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)item_list_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_editor)editor_public.h \

	gcc -c $(copt) -o $(bind)main.o $(srcd)main.c

$(bind)readline.o:\
 $(srcd)readline.c $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)readline.o $(srcd)readline.c

$(bind)io_files.o:\
 $(srcd)io_files.c $(srcd)io_files.h \
 $(srcd)player_class.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)g_item_adt.h $(srcd_adt)item_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)party_list_adt.h \

	gcc -c $(copt) -o $(bind)io_files.o $(srcd)io_files.c

$(bind)io_files_text.o:\
 $(srcd)io_files_text.c $(srcd)io_files_text.h \
 $(srcd)player_class.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)g_item_adt.h $(srcd_adt)item_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_editor)editor_public.h \

	gcc -c $(copt) -o $(bind)io_files_text.o $(srcd)io_files_text.c

$(bind)clash.o:\
 $(srcd)clash.c $(srcd)clash.h \
 $(srcd)player_class.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)player_party_adt.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)orb_adt.h $(srcd_adt)bag_adt.h \

	gcc -c $(copt) -o $(bind)clash.o $(srcd)clash.c

$(bind)clash_mode.o:\
 $(srcd)clash_mode.c $(srcd)clash.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)spell_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)bag_adt.h \
 $(srcd_editor)editor_public.h \
 $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)clash_mode.o $(srcd)clash_mode.c

$(bind)player_class.o:\
 $(srcd)player_class.c $(srcd)player_class.h \

	gcc -c $(copt) -o $(bind)player_class.o $(srcd)player_class.c


########
# ADT
########

$(bind)linked_list.o:\
 $(srcd_adt)linked_list.c $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)linked_list.o $(srcd_adt)linked_list.c

$(bind)player_classes_adt.o:\
 $(srcd_adt)player_classes_adt.c $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)linked_list.h \
 $(srcd)player_class.h \

	gcc -c $(copt) -o $(bind)player_classes_adt.o $(srcd_adt)player_classes_adt.c

$(bind)player_character_adt.o:\
 $(srcd_adt)player_character_adt.c $(srcd_adt)player_character_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)bag_adt.h \
 $(srcd)player_class.h \

	gcc -c $(copt) -o $(bind)player_character_adt.o $(srcd_adt)player_character_adt.c

$(bind)pch_list_adt.o:\
 $(srcd_adt)pch_list_adt.c $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)player_character_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)bag_adt.h \
 $(srcd_adt)linked_list.h \
 $(srcd)player_class.h \

	gcc -c $(copt) -o $(bind)pch_list_adt.o $(srcd_adt)pch_list_adt.c

$(bind)player_party_adt.o:\
 $(srcd_adt)player_party_adt.c $(srcd_adt)player_party_adt.h \
 $(srcd_adt)player_character_adt.h \
 $(srcd)player_class.h \

	gcc -c $(copt) -o $(bind)player_party_adt.o $(srcd_adt)player_party_adt.c

$(bind)party_list_adt.o:\
 $(srcd_adt)party_list_adt.c $(srcd_adt)party_list_adt.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)player_character_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)party_list_adt.o $(srcd_adt)party_list_adt.c

$(bind)spell_adt.o:\
 $(srcd_adt)spell_adt.c $(srcd_adt)spell_adt.h \

	gcc -c $(copt) -o $(bind)spell_adt.o $(srcd_adt)spell_adt.c

$(bind)spell_list_adt.o:\
 $(srcd_adt)spell_list_adt.c $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)spell_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)spell_list_adt.o $(srcd_adt)spell_list_adt.c

$(bind)orb_adt.o:\
 $(srcd_adt)orb_adt.c $(srcd_adt)orb_adt.h \
 $(srcd_adt)spell_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)orb_adt.o $(srcd_adt)orb_adt.c

$(bind)orb_list_adt.o:\
 $(srcd_adt)orb_list_adt.c $(srcd_adt)orb_list_adt.h  \
 $(srcd_adt)spell_adt.h $(srcd_adt)orb_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)orb_list_adt.o $(srcd_adt)orb_list_adt.c

$(bind)g_item_adt.o:\
 $(srcd_adt)g_item_adt.c $(srcd_adt)g_item_adt.h \

	gcc -c $(copt) -o $(bind)g_item_adt.o $(srcd_adt)g_item_adt.c

$(bind)item_list_adt.o:\
 $(srcd_adt)item_list_adt.c $(srcd_adt)item_list_adt.h \
 $(srcd_adt)g_item_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)item_list_adt.o $(srcd_adt)item_list_adt.c

$(bind)bag_adt.o:\
 $(srcd_adt)bag_adt.c $(srcd_adt)bag_adt.h \
 $(srcd_adt)g_item_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)bag_adt.o $(srcd_adt)bag_adt.c

$(bind)bag_list_adt.o:\
 $(srcd_adt)bag_list_adt.c $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)g_item_adt.h \
 $(srcd_adt)linked_list.h \

	gcc -c $(copt) -o $(bind)bag_list_adt.o $(srcd_adt)bag_list_adt.c


########
# EDITOR
########

$(bind)editor_public.o:\
 $(srcd_editor)editor_public.c $(srcd_editor)editor_public.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)party_list_adt.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)g_item_adt.h $(srcd_adt)item_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd)player_class.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)editor_public.o $(srcd_editor)editor_public.c

$(bind)classes_chars.o:\
 $(srcd_editor)classes_chars.c $(srcd_editor)editor_public.h \
 $(srcd_adt)player_classes_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)orb_list_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)player_party_adt.h \
 $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)classes_chars.o $(srcd_editor)classes_chars.c

$(bind)character_class_editor.o:\
 $(srcd_editor)character_class_editor.c $(srcd_editor)editor_public.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd)player_class.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)character_class_editor.o $(srcd_editor)character_class_editor.c

$(bind)character_list_editor.o:\
 $(srcd_editor)character_list_editor.c $(srcd_editor)editor_public.h \
 $(srcd_adt)player_classes_adt.h \
 $(srcd_adt)player_character_adt.h $(srcd_adt)pch_list_adt.h \
 $(srcd_adt)orb_list_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)party_list_adt.h \
 $(srcd)player_class.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)character_list_editor.o $(srcd_editor)character_list_editor.c

$(bind)player_party_editor.o:\
 $(srcd_editor)player_party_editor.c $(srcd_editor)editor_public.h \
 $(srcd_adt)player_party_adt.h $(srcd_adt)party_list_adt.h \
 $(srcd_adt)pch_list_adt.h $(srcd_adt)player_character_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)player_party_editor.o $(srcd_editor)player_party_editor.c

$(bind)spells_orbs.o:\
 $(srcd_editor)spells_orbs.c $(srcd_editor)editor_public.h \
 $(srcd_adt)spell_list_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)pch_list_adt.h \
 $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)spells_orbs.o $(srcd_editor)spells_orbs.c

$(bind)spell_list_editor.o:\
 $(srcd_editor)spell_list_editor.c $(srcd_editor)editor_public.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)orb_list_adt.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)spell_list_editor.o $(srcd_editor)spell_list_editor.c

$(bind)orbs_manager.o:\
 $(srcd_editor)orbs_manager.c $(srcd_editor)editor_public.h \
 $(srcd_adt)spell_adt.h $(srcd_adt)spell_list_adt.h \
 $(srcd_adt)orb_adt.h $(srcd_adt)orb_list_adt.h \
 $(srcd_adt)pch_list_adt.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)orbs_manager.o $(srcd_editor)orbs_manager.c

$(bind)items_bags.o:\
 $(srcd_editor)items_bags.c $(srcd_editor)editor_public.h \
 $(srcd_adt)item_list_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)pch_list_adt.h \
 $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)items_bags.o $(srcd_editor)items_bags.c

$(bind)item_list_editor.o:\
 $(srcd_editor)item_list_editor.c $(srcd_editor)editor_public.h \
 $(srcd_adt)g_item_adt.h $(srcd_adt)item_list_adt.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)item_list_editor.o $(srcd_editor)item_list_editor.c

$(bind)bag_manager.o:\
 $(srcd_editor)bag_manager.c $(srcd_editor)editor_public.h \
 $(srcd_adt)g_item_adt.h $(srcd_adt)item_list_adt.h \
 $(srcd_adt)bag_adt.h $(srcd_adt)bag_list_adt.h \
 $(srcd_adt)pch_list_adt.h \
 $(srcd)io_files.h $(srcd)io_files_text.h $(srcd)readline.h \

	gcc -c $(copt) -o $(bind)bag_manager.o $(srcd_editor)bag_manager.c


########
# CLEAN
########

clean:\

	rm -v orb_clash bin/*

clean_bin:\

	rm -v bin/*