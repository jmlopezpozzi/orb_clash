/* l_list_test_3.c */
/* Tests a linked list with contents that are dynamically allocated on the    */
/* heap and that uses a custom destructor on such contents when removing nodes*/

#include "../../src/adt/linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct string {
	int len;
	char *str;
};

void ret_value(int ret)
{
	printf("Returned %d\n", ret);
}

// This function tests l_list_get_node_contents and l_list_qty_nodes
void show_contents(Linked_list list)
{
	const int num_nodes = l_list_qty_nodes(list);

	printf("--num_nodes = %d\n", num_nodes);
	for (int i = 0; i < num_nodes; ++i) {
		const struct string *node_contents = l_list_get_node_contents(list, i);

		printf("Node %d: %s(len=%d), ", i, node_contents->str,
		       node_contents->len);
	}
	putchar('\n');
}

// Creates node contents on the heap
void create_content(struct string **array_address, const char *content_src)
{
	struct string *content_obj;

	content_obj = malloc(sizeof(*content_obj));
	if (content_obj == NULL) {
		printf("Error in test, could not create content object\n");
		return;
	}

	content_obj->len = strlen(content_src);
	content_obj->str = malloc(content_obj->len + 1);
	if (content_obj->str == NULL) {
		printf("Error in test, could not create content sub-object\n");
		free(content_obj);
		return;
	}
	strncpy(content_obj->str, content_src, content_obj->len + 1);

	*array_address = content_obj;
}

// Custom destructor for struct string objects
void content_destructor(void *content)
{
	struct string *content_out = content;

	free(content_out->str);
	free(content);
}

int main(void)
{
	const char *contents_src[] = {"a", "bc", "def"};
	const int iterations = sizeof(contents_src) / sizeof(contents_src[0]);
	struct string *test_contents[iterations];
	int i;
	int ret;
	Linked_list list;

	printf("Creating linked list\n");
	list = l_list_create(content_destructor);
	show_contents(list);
	printf("Destroying linked list\n");
	list = l_list_destroy(list);

	printf("Creating linked list\n");
	list = l_list_create(content_destructor);
	show_contents(list);

	for (i = 0; i < iterations; ++i) {
		create_content(&test_contents[i], contents_src[i]);
		printf("Adding %s(%d) (ptr) to top\n", test_contents[i]->str,
		       test_contents[i]->len);
		ret = l_list_add_top(list, test_contents[i]);
		ret_value(ret);
		show_contents(list);
	}

	for (i = 0; i < iterations; ++i) {
		int node_position = l_list_find_node_pos(list, test_contents[i]);

		printf("Node position of %s(%d) is %d\n", test_contents[i]->str,
		       test_contents[i]->len, node_position);
	}

	for (i = 0; i < iterations; ++i) {
		printf("Removing from top\n");
		ret = l_list_remove_top(list);
		ret_value(ret);
		show_contents(list);
	}

	for (i = 0; i < iterations; ++i) {
		create_content(&test_contents[i], contents_src[i]);
		printf("Adding %s(%d) (ptr) to end\n", test_contents[i]->str,
		       test_contents[i]->len);
		ret = l_list_add_end(list, test_contents[i]);
		ret_value(ret);
		show_contents(list);
	}

	for (i = iterations; i > 0; --i) {
		printf("Removing from position %d\n", i - 1);
		ret = l_list_remove_pos(list, i - 1);
		ret_value(ret);
		show_contents(list);
	}

	for (i = 0; i < iterations; ++i) {
		create_content(&test_contents[i], contents_src[i]);
		printf("Adding %s(%d) (ptr) to end\n", test_contents[i]->str,
		       test_contents[i]->len);
		l_list_add_end(list, test_contents[i]);
	}
	show_contents(list);

	struct string **flushed_array;

	printf("Flushing contents to array\n");
	flushed_array = (struct string **)l_list_to_array(list, L_FLUSH);  // List is emptied but contents are not deleted
	show_contents(list);

	printf("Flushed array contents:\n");
	for (i = 0; i < iterations; ++i) {
		printf("%s(%d) ", flushed_array[i]->str, flushed_array[i]->len);
	}
	putchar('\n');

	for (i = 0; i < iterations; ++i) {
		printf("Adding %s(%d) (ptr from flushed_array) to top\n",
		       flushed_array[i]->str, flushed_array[i]->len);
		l_list_add_top(list, flushed_array[i]);
	}
	show_contents(list);

	printf("Freeing flushed array\n");
	free(flushed_array);

	printf("Destroying list\n");
	list = l_list_destroy(list);

	printf("Creating list\n");
	list = l_list_create(content_destructor);

	for (i = 0; i < iterations; ++i) {
		create_content(&test_contents[i], contents_src[i]);
	}

	printf("Testing wrong input\n");

	printf("l_list_destroy(NULL)\n");
	l_list_destroy(NULL);
	show_contents(list);

	printf("l_list_add_top(NULL, NULL)\n");
	ret = l_list_add_top(NULL, NULL);
	ret_value(ret);
	show_contents(list);
	printf("l_list_add_top(NULL, ok)\n");
	ret = l_list_add_top(NULL, test_contents[0]);
	ret_value(ret);
	show_contents(list);
	printf("l_list_add_top(ok, NULL)\n");
	ret = l_list_add_top(list, NULL);
	ret_value(ret);
	show_contents(list);

	printf("l_list_add_end(NULL, NULL)\n");
	ret = l_list_add_end(NULL, NULL);
	ret_value(ret);
	show_contents(list);
	printf("l_list_add_end(NULL, ok)\n");
	ret = l_list_add_end(NULL, test_contents[0]);
	ret_value(ret);
	show_contents(list);
	printf("l_list_add_end(ok, NULL)\n");
	ret = l_list_add_end(list, NULL);
	ret_value(ret);
	show_contents(list);

	printf("l_list_remove_top(NULL)\n");
	ret = l_list_remove_top(NULL);
	ret_value(ret);
	show_contents(list);

	printf("l_list_remove_pos(NULL, wrong)\n");
	ret = l_list_remove_pos(NULL, -11);
	ret_value(ret);
	show_contents(list);
	printf("l_list_remove_pos(ok, wrong)\n");
	ret = l_list_remove_pos(list, 0);
	ret_value(ret);
	show_contents(list);

	printf("l_list_find_node_pos(NULL, NULL)\n");
	ret = l_list_find_node_pos(NULL, NULL);
	ret_value(ret);
	show_contents(list);
	printf("l_list_find_node_pos(NULL, ok)\n");
	ret = l_list_find_node_pos(NULL, test_contents[0]);
	ret_value(ret);
	show_contents(list);
	printf("l_list_find_node_pos(ok, NULL)\n");
	ret = l_list_find_node_pos(list, NULL);
	ret_value(ret);
	show_contents(list);

	printf("l_list_to_array(NULL, L_FLUSH)\n");
	flushed_array = (struct string **)l_list_to_array(NULL, L_FLUSH);
	printf("flushed_array is %s\n", flushed_array == NULL ? "NULL" : "Not NULL");
	show_contents(list);
	printf("l_list_to_array(ok, L_FLUSH) --but list is empty--\n");
	flushed_array = (struct string **)l_list_to_array(list, L_FLUSH);
	printf("flushed_array is %s\n", flushed_array == NULL ? "NULL" : "Not NULL");
	show_contents(list);

	printf("l_list_qty_nodes(NULL)\n");
	ret = l_list_qty_nodes(NULL);
	ret_value(ret);

	printf("Destroying list\n");
	l_list_destroy(list);

	for (i = 0; i < iterations; ++i) {
		content_destructor(test_contents[i]);
	}

	return 0;
}