/* test_btb.c */
/* Tests integrity between files processed by io_files and io_text_files      */
/* Sequence modes (enter as second argument):
 *     default: bin->txt->bin
 *     "t":     txt->bin->txt
 * Type modes (enter via stdin):
 *     1 - Spells and orbs
 *     2 - Items and bags
 *     3 - Classes and characters
 */

#include "../../src/io_files.h"
#include "../../src/io_files_text.h"
#include "../../src/readline.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define EXT_MARGIN 6
// Do not define the next to be longer than EXT_MARGIN - 2
#define BIN_EXT ".bin"
#define TXT_EXT ".txt"

enum type_modes {SPL_ORB, ITM_BAG, CLS_PCH, NUM_TYPE_MODES};

const struct {
	char *type_name;
	int (*bin_loader)(const char *, void **, void **);
	long (*bin_saver)(const char *, void *, void *);
	int (*txt_loader)(const char *, void **, void **);
	long (*txt_saver)(const char *, void *, void *);
	void *(*destroy_primary)(void *);
	void *(*destroy_secondary)(void *);
}
funcs[NUM_TYPE_MODES] = {

	[SPL_ORB] = {
		.type_name = "spells and orbs",
		.bin_loader = (int (*)(const char *, void **, void **)) io_load_spells_orbs_file,
		.bin_saver  = (long (*)(const char *, void *, void *)) io_save_spells_orbs_file,
		.txt_loader = (int (*)(const char *, void **, void **)) io_text_load_spells_orbs_file,
		.txt_saver  = (long (*)(const char *, void *, void *)) io_text_save_spells_orbs_file,
		.destroy_primary   = (void *(*)(void *)) spell_list_destroy,
		.destroy_secondary = (void *(*)(void *)) orb_list_destroy,
	},

	[ITM_BAG] = {
		.type_name = "items and bags",
		.bin_loader = (int (*)(const char *, void **, void **)) io_load_items_bags_file,
		.bin_saver  = (long (*)(const char *, void *, void *)) io_save_items_bags_file,
		.txt_loader = (int (*)(const char *, void **, void **)) io_text_load_items_bags_file,
		.txt_saver  = (long (*)(const char *, void *, void *)) io_text_save_items_bags_file,
		.destroy_primary   = (void *(*)(void *)) item_list_destroy,
		.destroy_secondary = (void *(*)(void *)) bag_list_destroy,
	},

	[CLS_PCH] = {
		.type_name = "classes and characters",
		.bin_loader = (int (*)(const char *, void **, void **)) io_load_classes_chars_file,
		.bin_saver  = (long (*)(const char *, void *, void *)) io_save_classes_chars_file,
		.txt_loader = (int (*)(const char *, void **, void **)) io_text_load_classes_chars_file,
		.txt_saver  = (long (*)(const char *, void *, void *)) io_text_save_classes_chars_file,
		.destroy_primary   = (void *(*)(void *)) player_classes_destroy,
		.destroy_secondary = (void *(*)(void *)) pch_list_destroy,
	},
};


long load(char *shell_arg, void **primary_list_ptr, void **secondary_list_ptr,
          int (*loader)(const char *, void **, void **), char *ext)
{
	char filename[FILENAME_MAX];
	long ret;

	strcpy(filename, shell_arg);
	strcat(filename, ext);

	ret = (*loader)(filename, primary_list_ptr, secondary_list_ptr);

	if (ret >= 0L) {
		printf("Loaded %s\n", filename);
	}
	else {
		printf("Error loading %s\n", filename);
	}

	return ret;
}

long save(char *shell_arg, void *primary_list, void *secondary_list,
          long (*saver)(const char *, void *, void *), char *ext)
{
	char filename[FILENAME_MAX];
	long ret;

	strcpy(filename, shell_arg);
	strcat(filename, ext);

	ret = (*saver)(filename, primary_list, secondary_list);

	if (ret >= 0L) {
		printf("Saved %s\n", filename);
	}
	else {
		printf("Error saving %s\n", filename);
	}

	return ret;
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Tests integrity between files processed by io_files "
		                "and io_text_files\n"
		                "usage: test_btb filename [t]\n"
		                "  filename: name of original file without extension\n"
		                "  t (optional): test sequence txt->bin->txt\n"
		                "                  (default is bin->txt->bin)\n");
		return -1;
	}

	void *primary_list = NULL;
	void *secondary_list = NULL;
	int type;
	long ret;
	bool t_arg;

	if (argc > 2 && (strcmp(argv[2], "t") == 0)) {
		t_arg = true;
	}
	else {
		t_arg = false;
	}

	printf("Enter file type category to test: ");
	type = flushed_scanf_d(FSD_ERR_VAL);
	if (type < 0 || type >= (int) NUM_TYPE_MODES) {
		fprintf(stderr, "Type %d does not exist\n", type);
		return -2;
	}
	printf("Testing %s file type category\n", funcs[type].type_name);

	ret = load(argv[1], &primary_list, &secondary_list,
	           t_arg ? funcs[type].txt_loader : funcs[type].bin_loader,
	           t_arg ? TXT_EXT"1"             : BIN_EXT"1");
	if (ret < 0L) {
		return -3;
	}

	ret = save(argv[1], primary_list, secondary_list,
	           t_arg ? funcs[type].bin_saver : funcs[type].txt_saver,
	           t_arg ? BIN_EXT               : TXT_EXT);
	if (ret < 0L) {
		return -4;
	}

	// Clears loaded objects
	primary_list   = funcs[type].destroy_primary(primary_list);
	secondary_list = funcs[type].destroy_secondary(secondary_list);

	ret = load(argv[1], &primary_list, &secondary_list,
	           t_arg ? funcs[type].bin_loader : funcs[type].txt_loader,
	           t_arg ? BIN_EXT                : TXT_EXT);
	if (ret < 0L) {
		return -5;
	}

	ret = save(argv[1], primary_list, secondary_list,
	           t_arg ? funcs[type].txt_saver : funcs[type].bin_saver,
	           t_arg ? TXT_EXT"2"            : BIN_EXT"2");
	if (ret < 0L) {
		return -6;
	}

	primary_list   = funcs[type].destroy_primary(primary_list);
	secondary_list = funcs[type].destroy_secondary(secondary_list);

	return 0;
}