itemlist
"2" "Potion" "Restores part of an ally HPT stat",
"5" "Super Potion" "Restores a significant part of an ally HPT stat",
"3" "Antidote" "Cures poisoned status",
"7" "Revive" "Brings back a KO'd ally";
bag.list
"20" "Small Bag A" "0""0""0""0""0""2""2""2",
"20" "Small Bag B" "0""0""0""0""2""3",
"40" "Medium Bag A" "1""1""1""2",
"40" "Medium Bag B" "1""1""1""1""3""3",
"60" "Big Bag A" "0""0""0""0""3""3""3",
"60" "Big Bag B" "0""1""1""1""2""2""3""3"