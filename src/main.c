/* main.c */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "io_files.h"
#include "readline.h"
#include "player_class.h"
#include "adt/player_classes_adt.h"
#include "adt/player_character_adt.h"
#include "adt/pch_list_adt.h"
#include "adt/player_party_adt.h"
#include "adt/party_list_adt.h"
#include "adt/spell_list_adt.h"
#include "adt/orb_list_adt.h"
#include "adt/item_list_adt.h"
#include "adt/bag_list_adt.h"
#include "editor/editor_public.h"

// Editor function prototypes (should they go in a header?)
Party_list player_parties_editor(Party_list pl_parties, Pch_list pl_chars,
                                 Orb_list orb_list, Bag_list bag_list);
void classes_chars(Player_classes *pc_ptr, Pch_list *pl_chars_ptr,
                   Orb_list orb_list, Bag_list bag_list,
                   Party_list parties);
void spells_orbs(Spell_list *spell_list_ptr, Orb_list *orb_list_ptr,
                   Pch_list pl_chars);
void items_bags(Item_list *item_list_ptr, Bag_list *bag_list_ptr,
                Pch_list pl_chars);
int clash_mode(Player_party sides[]);

static void option_save_editor_state(Spell_list spell_list, Orb_list orb_list,
                                     Item_list item_list, Bag_list bag_list,
                                     Player_classes pc, Pch_list pl_chars,
                                     Party_list pl_parties);
static void option_load_editor_state(Spell_list *spell_list_ptr,
                                     Orb_list *orb_list_ptr,
                                     Item_list *item_list_ptr,
                                     Bag_list *bag_list_ptr,
                                     Player_classes *pc_ptr,
                                     Pch_list *pl_chars_ptr,
                                     Party_list *pl_parties_ptr);

static void option_launch_clash_mode(Party_list pl_parties);
static int count_clash_characters(Player_party sides[], int num_sides);
static Player_character *get_clash_chars(Player_party sides[], int num_sides,
                                         int num_chars);
static Player_character copy_pch(Player_character pch);
static Player_party copy_party(Player_party party);
static void clash_party_error_cleanup(Player_party sides[], int num_sides);
static bool check_repeated_bags(Player_character *clash_chars, int num_chars);
static bool check_dummy_class_chars(Player_character *clash_chars,
                                    int num_chars);


int main(void)
{
	enum {EXIT = 0, PARTY_ED, CLASS_CHARS, SPELL_ORB, ITEM_BAG, SAVE_ED,
	                LOAD_ED, CLASH};
	int option = -1;

	Party_list     pl_parties = NULL;
	Player_classes pc         = NULL;
	Pch_list       pl_chars   = NULL;
	Spell_list     spell_list = NULL;
	Orb_list       orb_list   = NULL;
	Item_list      itm_list   = NULL;
	Bag_list       fls_list   = NULL;

	for (;;) {

		printf("\nOrb Clash!\n");
		printf("(%d - Player party editor, %d - Classes and Characters,\n"
		       " %d - Spells and Orbs, %d - Items and Bags,\n"
		       " %d - Save editor state, %d - Load editor state,\n"
		       " %d - Clash!, %d - Exit)\n:",
		       PARTY_ED, CLASS_CHARS, SPELL_ORB, ITEM_BAG, SAVE_ED, LOAD_ED,
		       CLASH, EXIT);

		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {

		case PARTY_ED:
			pl_parties = player_parties_editor(pl_parties, pl_chars, orb_list,
			                                   fls_list);
			break;

		case CLASS_CHARS:
			classes_chars(&pc, &pl_chars, orb_list, fls_list, pl_parties);
			break;

		case SPELL_ORB:
			spells_orbs(&spell_list, &orb_list, pl_chars);
			break;

		case ITEM_BAG:
			items_bags(&itm_list, &fls_list, pl_chars);
			break;

		case SAVE_ED:
			option_save_editor_state(spell_list, orb_list, itm_list, fls_list,
			                         pc, pl_chars, pl_parties);
			break;

		case LOAD_ED:
			option_load_editor_state(&spell_list, &orb_list, &itm_list,
			                         &fls_list, &pc, &pl_chars, &pl_parties);
			break;

		case CLASH:
			option_launch_clash_mode(pl_parties);
			break;

		case EXIT:
			party_list_destroy(pl_parties);
			pch_list_destroy(pl_chars);
			player_classes_destroy(pc);
			bag_list_destroy(fls_list);
			item_list_destroy(itm_list);
			orb_list_destroy(orb_list);
			spell_list_destroy(spell_list);

			return 0;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}

static void option_launch_clash_mode(Party_list pl_parties)
{
	if (pl_parties == NULL) {
		printf("Player parties not ready!\n");
		return;
	}

	int num_chars = 0;
	Player_character *clash_chars;  // Dynamic array
	Player_party sides[CLASH_NUM_SIDES];
	bool bag_copied = false;

	for (int i = 0; i < CLASH_NUM_SIDES; ++i) {
		Player_party party;

		printf("\nSide %d:\n", i);

		party = ep_choose_party_from_list_obj(pl_parties, "join clash");
		if (party == NULL) {
			clash_party_error_cleanup(sides, i);  // Does cleanup before clash chars array exists
			return;
		}

		sides[i] = copy_party(party);
		if (sides[i] == NULL) {
			clash_party_error_cleanup(sides, i);
			return;
		}
	}

	num_chars = count_clash_characters(sides, CLASH_NUM_SIDES);
	clash_chars = get_clash_chars(sides, CLASH_NUM_SIDES, num_chars);

	if (clash_chars == NULL) {
		printf("Could not create clash_chars array\n");
		clash_party_error_cleanup(sides, CLASH_NUM_SIDES);
		return;
	}

	if (check_dummy_class_chars(clash_chars, num_chars) == true) {

		printf("There is at least one character with a dummy class\n"
		       "(a reference to a removed class) attempting to join\n"
		       "the clash. Update the characters in your parties to\n"
		       "existing classes and try again.\n");

		goto cleanup_return;
	}

	printf("Should bag modifications persist after clash? (y)\n:");

	if (accept_prompt('y') == true) {

		// Bag modifications persist
		if (check_repeated_bags(clash_chars, num_chars) == true) {

			printf("Two or more characters share a Bag. Enter clash without\n"
			       "the Bag persistence option or change repeated Bags\n"
			       "in the Player Party Editor. Alternatively, you can also\n"
			       "use the Copy Bag option in the Bag Manager to create\n"
			       "unique instances of other existing Bags.\n");

			goto cleanup_return;
		}
	}
	else {
		// Bag modifications do not persist
		Bag cur_bag;

		bag_copied = true;

		for (int i = 0; i < num_chars; ++i) {
			cur_bag = player_character_get_bag(clash_chars[i]);

			if (cur_bag == NULL) {
				continue;
			}

			cur_bag = bag_copy(cur_bag);

			if (cur_bag == NULL) {
				printf("Could not copy bag for character %d\n", i);

				for (int j = 0; j < i; ++j) {
					cur_bag = player_character_get_bag(clash_chars[j]);
					bag_destroy(cur_bag);
				}

				goto cleanup_return;
			}

			player_character_set_bag(clash_chars[i], cur_bag);
		}
	}

	// Clash mode launch!
	unsigned long clash_turns = clash_mode(sides);
	printf("Clash ended in %lu turns\n", clash_turns);

	// After clash
	if (bag_copied == true) {
		for (int i = 0; i < num_chars; ++i) {
			bag_destroy(player_character_get_bag(clash_chars[i]));
		}
	}

cleanup_return:
	for (int i = 0; i < num_chars; ++i) {
		player_character_destroy(clash_chars[i]);
	}
	for (int i = 0; i < CLASH_NUM_SIDES; ++i) {
		player_party_destroy(sides[i]);
	}

	free(clash_chars);
}

/* Assumes player parties that are not NULL nor empty */
static int count_clash_characters(Player_party sides[], int num_sides)
{
	int total = 0;

	for (int i = 0; i < num_sides; ++i) {
		total += player_party_get_num_chars(sides[i]);
	}

	return total;
}

static Player_character *get_clash_chars(Player_party sides[], int num_sides,
                                         int num_chars)
{
	Player_character *clash_chars;
	int clash_chars_idx = 0;
	Player_character cur_pch;

	clash_chars = malloc(sizeof(*clash_chars) * num_chars);
	if (clash_chars == NULL) {
		return NULL;
	}

	for (int i = 0; i < num_sides; ++i) {
		for (int j = 0; j < PARTY_NUM_POSITIONS; ++j) {
			cur_pch = player_party_get_char(sides[i], j);
			if (cur_pch != NULL) {
				clash_chars[clash_chars_idx++] = cur_pch;
			}
		}
	}

	return clash_chars;
}

static Player_character copy_pch(Player_character pch)
{
	Player_character pch_copy;

	pch_copy = player_character_create();
	if (pch_copy == NULL) {
		fprintf(stderr, "%s: Could not copy Player_character %s for clash\n",
		                __func__, player_character_get_name(pch));
		return NULL;
	}

	player_character_set_name(pch_copy, player_character_get_name(pch));
	player_character_set_class(pch_copy, player_character_get_class(pch));
	player_character_set_level(pch_copy, player_character_get_level(pch));
	player_character_set_orb(pch_copy, player_character_get_orb(pch));
	player_character_set_bag(pch_copy, player_character_get_bag(pch));

	return pch_copy;
}

static Player_party copy_party(Player_party party)
{
	Player_party party_copy;
	Player_character pch;
	Player_character pch_copy;

	party_copy = player_party_create();
	if (party_copy == NULL) {
		fprintf(stderr, "%s: Could not copy Player_party %s for clash\n",
		                __func__, player_party_get_name(party));
		return NULL;
	}

	for (int i = 0; i < PARTY_NUM_POSITIONS; ++i) {
		pch = player_party_get_char(party, i);

		if (pch != NULL) {
			int pch_pos;

			pch_copy = copy_pch(pch);

			if (pch_copy == NULL) {
				fprintf(stderr, "%s: Player_party %s copy for clash failed\n",
				               __func__, player_party_get_name(party));
				clash_party_error_cleanup(&party_copy, 1);
				return NULL;
			}

			pch_pos = player_party_add_char(party_copy, pch_copy);
			player_party_swap_position(party_copy, pch_pos, i);
		}
	}

	player_party_set_name(party_copy, player_party_get_name(party));

	return party_copy;
}

static void clash_party_error_cleanup(Player_party sides[], int num_sides)
{
	for (int i = 0; i < num_sides; ++i) {
		for (int j = 0; j < PARTY_NUM_POSITIONS; ++j) {
			player_character_destroy(player_party_get_char(sides[i], j));
		}
		player_party_destroy(sides[i]);
	}
}

static bool check_repeated_bags(Player_character *clash_chars, int num_chars)
{
	Bag *clash_bags;  // Dynamic array
	Bag cur_bag;
	int cur_char;

	clash_bags = malloc(sizeof(*clash_bags) * num_chars);
	if (clash_bags == NULL) {
		printf("Could not create clash_bags array\n");
		return false;
	}

	for (cur_char = 0; cur_char < num_chars; ++cur_char) {
		cur_bag = player_character_get_bag(clash_chars[cur_char]);

		if (cur_bag != NULL) {
			for (int i = 0; i < cur_char; ++i) {
				if (cur_bag == clash_bags[i]) {
					free(clash_bags);
					return true;
				}
			}
		}

		clash_bags[cur_char] = cur_bag;
	}

	free(clash_bags);
	return false;
}

static bool check_dummy_class_chars(Player_character *clash_chars,
                                    int num_chars)
{
	for (int i = 0; i < num_chars; ++i) {
		if (player_character_get_class(clash_chars[i]) == player_class_dummy())
		{
			return true;
		}
	}

	return false;
}

static void option_save_editor_state(Spell_list spell_list, Orb_list orb_list,
                                     Item_list item_list, Bag_list bag_list,
                                     Player_classes pc, Pch_list pl_chars,
                                     Party_list pl_parties)
{
	char filename[FILENAME_MAX];

	printf("Enter file name: ");
	read_line(filename, FILENAME_MAX);

	io_save_editor_state(filename, spell_list, orb_list, item_list, bag_list,
                         pc, pl_chars, pl_parties);
}

static void option_load_editor_state(Spell_list *spell_list_ptr,
                                     Orb_list *orb_list_ptr,
                                     Item_list *item_list_ptr,
                                     Bag_list *bag_list_ptr,
                                     Player_classes *pc_ptr,
                                     Pch_list *pl_chars_ptr,
                                     Party_list *pl_parties_ptr)
{
	char filename[FILENAME_MAX];

	printf("Current editor state will be lost, continue? (y)\n:");
	if (accept_prompt('y') == false) {
		return;
	}

	*pl_parties_ptr = party_list_destroy(*pl_parties_ptr);
	*pl_chars_ptr   = pch_list_destroy(*pl_chars_ptr);
	*pc_ptr         = player_classes_destroy(*pc_ptr);
	*bag_list_ptr   = bag_list_destroy(*bag_list_ptr);
	*item_list_ptr  = item_list_destroy(*item_list_ptr);
	*orb_list_ptr   = orb_list_destroy(*orb_list_ptr);
	*spell_list_ptr = spell_list_destroy(*spell_list_ptr);

	printf("Enter file name: ");
	read_line(filename, FILENAME_MAX);

	io_load_editor_state(filename, spell_list_ptr, orb_list_ptr, item_list_ptr,
	                     bag_list_ptr, pc_ptr, pl_chars_ptr, pl_parties_ptr);
}