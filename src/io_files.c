/* io_files.c */

#include "io_files.h"
#include "player_class.h"
#include "adt/spell_adt.h"
#include "adt/spell_list_adt.h"
#include "adt/orb_adt.h"
#include "adt/orb_list_adt.h"
#include "adt/g_item_adt.h"
#include "adt/item_list_adt.h"
#include "adt/bag_adt.h"
#include "adt/bag_list_adt.h"
#include "adt/player_classes_adt.h"
#include "adt/player_character_adt.h"
#include "adt/pch_list_adt.h"
#include "adt/player_party_adt.h"
#include "adt/party_list_adt.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool error_flag_sg = false;  // Set to true when a read function cannot create an object, used to abort load operations

enum {SPL_LIST = 0, SPL_ORB_LIST, ITM_LIST, ITM_BAG_LIST, CLS_LIST, ED_STATE,
                    CLS_PCH_LIST};  // Used for subscripting headers array

// Information provided to file_saver_general by its clients
struct io_saver_info {
	const char *header_single;
	const char *header_combi;
	void *(*write_primary)(FILE *, void *);
	void *(*write_secondary)(FILE *, void *, void *);
};

// Information provided to file_loader_general by its clients
struct io_loader_info {
	const char *header_single;
	const char *header_combi;
	void *(*read_primary)(FILE *);
	void *(*read_secondary)(FILE *, void *);
	void *(*destroy_primary)(void *);
	void *(*destroy_secondary)(void *);
	char *type_name_single;
	char *type_name_combi;
};

static const char headers[7][8] = {{0, 's', 'p', 'l', 'l', 'i', 's', 't'},
                                   {1, 'o', 'r', 'b', 'l', 'i', 's', 't'},
                                   {2, 'i', 't', 'e', 'm', 'l', 's', 't'},
                                   {3, 'b', 'a', 'g', 'l', 'i', 's', 't'},
                                   {4, 'c', 'l', 's', 'l', 'i', 's', 't'},
                                   {5, 'c', 'h', 'r', 'l', 'i', 's', 't'},
                                   {9, 'e', 'd', 'i', 't', 'o', 'r', 's'}};


static long file_saver_general(const struct io_saver_info *info,
                               const char *filename, void *primary_list,
                               void *secondary_list, const char *caller);
static int file_loader_general(const struct io_loader_info *info,
                               const char *filename, void **primary_list_ptr,
                               void **secondary_list_ptr, const char *caller);
static void file_error_messages(FILE *fp, const char *filename,
                                const char *caller);

static void write_spell_list_data(FILE *fp_out, Spell_list spell_list);
static void write_orb_list_data(FILE *fp_out, Orb_list orb_list,
                                Spell_list spell_list);
static void write_item_list_data(FILE *fp_out, Item_list item_list);
static void write_bag_list_data(FILE *fp_out, Bag_list bag_list,
                                Item_list item_list);
static void write_player_classes_data(FILE *fp_out, Player_classes pc);
static void write_pch_list_data(FILE *fp_out, Pch_list pl_chars,
                                Player_classes pc, Orb_list orb_list,
                                Bag_list bag_list);
static void write_pch_list_data_simple(FILE *fp_out, Pch_list pl_chars,
                                       Player_classes pc);  // Does not save Orb nor Bag data, can be used with file_saver_general
static void write_player_character_data(FILE *fp_out, Player_character pch,
                                        Player_classes pc,
                                        Orb_list orb_list,
                                        Bag_list bag_list);
static void write_party_list_data(FILE *fp_out, Party_list pl_parties,
                                  Pch_list pl_chars);
static void write_player_party_data(FILE *fp_out, Player_party pl_party,
                                    Pch_list pl_chars);

static Spell_list      read_spell_list_data(FILE *fp_in);
static Orb_list        read_orb_list_data(FILE *fp_in, Spell_list spell_list);
static Item_list       read_item_list_data(FILE *fp_in);
static Bag_list        read_bag_list_data(FILE *fp_in, Item_list item_list);
static Player_classes  read_player_classes_data(FILE *fp_in);
static Pch_list        read_pch_list_data(FILE *fp_in, Player_classes pc,
                                          Orb_list orb_list,
                                          Bag_list bag_list);
static Pch_list        read_pch_list_data_simple(FILE *fp_in,
                                                 Player_classes pc);  // Doesn't read Orb or Bag data, can be used with file_loader_general
static Player_character read_player_character_data(FILE *fp_in,
                                                   Player_classes pc,
                                                   Orb_list orb_list,
                                                   Bag_list bag_list);
static Party_list      read_party_list_data(FILE *fp_in, Pch_list pl_chars);
static Player_party    read_player_party_data(FILE *fp_in, Pch_list pl_chars);


long io_save_editor_state(const char *filename, Spell_list spell_list,
                          Orb_list orb_list, Item_list item_list,
                          Bag_list bag_list, Player_classes pc,
                          Pch_list pl_chars, Party_list pl_parties)
{
	if (filename == NULL) {
		return -1L;
	}
	if (filename[0] == '\0') {
		fprintf(stderr, "%s: Empty filename\n", __func__);
		return -2L;
	}

	FILE *fp_out;
	long file_size;

	fp_out = fopen(filename, "wb");
	if (fp_out == NULL) {
		fprintf(stderr, "%s: Could not create file %s\n", __func__, filename);
		return -3L;
	}

	fwrite(headers[ED_STATE], sizeof(headers[0]), 1, fp_out);
	write_spell_list_data(fp_out, spell_list);
	write_orb_list_data(fp_out, orb_list, spell_list);
	write_item_list_data(fp_out, item_list);
	write_bag_list_data(fp_out, bag_list, item_list);
	write_player_classes_data(fp_out, pc);
	write_pch_list_data(fp_out, pl_chars, pc, orb_list, bag_list);
	write_party_list_data(fp_out, pl_parties, pl_chars);

	file_error_messages(fp_out, filename, __func__);
	file_size = ftell(fp_out);

	fclose(fp_out);
	return file_size;
}

long io_save_spells_orbs_file(const char *filename, Spell_list spell_list,
                              Orb_list orb_list)
{
	const struct io_saver_info info = {
		.header_single = headers[SPL_LIST],
		.header_combi  = headers[SPL_ORB_LIST],
		.write_primary   = (void *(*)(FILE *, void *)) write_spell_list_data,
		.write_secondary = (void *(*)(FILE *, void *, void *))
			write_orb_list_data,
	};

	return file_saver_general(&info, filename, spell_list, orb_list, __func__);
}

long io_save_items_bags_file(const char *filename, Item_list item_list,
                             Bag_list bag_list)
{
	const struct io_saver_info info = {
		.header_single = headers[ITM_LIST],
		.header_combi  = headers[ITM_BAG_LIST],
		.write_primary   = (void *(*)(FILE *, void *)) write_item_list_data,
		.write_secondary = (void *(*)(FILE *, void *, void *))
			write_bag_list_data,
	};

	return file_saver_general(&info, filename, item_list, bag_list, __func__);
}

long io_save_classes_chars_file(const char *filename, Player_classes pc,
                                Pch_list pl_chars)
{
	const struct io_saver_info info = {
		.header_single = headers[CLS_LIST],
		.header_combi  = headers[CLS_PCH_LIST],
		.write_primary   = (void *(*)(FILE *, void *))
			write_player_classes_data,
		.write_secondary = (void *(*)(FILE *, void *, void *))
			write_pch_list_data_simple,
	};

	return file_saver_general(&info, filename, pc, pl_chars, __func__);
}

/* Unifies saving for all file types except editor_state */
static long file_saver_general(const struct io_saver_info *info,
                               const char *filename, void *primary_list,
                               void *secondary_list, const char *caller)
{
	if (filename == NULL) {
		return -1L;
	}
	if (filename[0] == '\0') {
		return -2L;
	}
	if (primary_list == NULL) {
		return -3L;
	}
	// This function will accept empty lists (check externally if that should not happen)

	FILE *fp_out;
	long file_size;

	fp_out = fopen(filename, "wb");
	if (fp_out == NULL) {
		fprintf(stderr, "%s: Could not create file %s\n", caller, filename);
		return -4L;
	}

	// Write file
	if (secondary_list == NULL) {
		fwrite(info->header_single, sizeof(headers[0]), 1, fp_out);
		info->write_primary(fp_out, primary_list);
	}
	else {
		fwrite(info->header_combi, sizeof(headers[0]), 1, fp_out);
		info->write_primary(fp_out, primary_list);
		info->write_secondary(fp_out, secondary_list, primary_list);
	}

	file_error_messages(fp_out, filename, caller);
	file_size = ftell(fp_out);

	fclose(fp_out);
	return file_size;
}


int io_load_editor_state(const char *filename,  Spell_list *spell_list_ptr,
                         Orb_list *orb_list_ptr, Item_list *item_list_ptr,
                         Bag_list *bag_list_ptr, Player_classes *pc_ptr,
                         Pch_list *pl_chars_ptr, Party_list *pl_parties_ptr)
{
	if (filename == NULL) {
		return -1;
	}
	if (filename[0] == '\0') {
		fprintf(stderr, "%s: Empty filename\n", __func__);
		return -2;
	}

	char header_check[sizeof(headers[0])];
	FILE *fp_in;

	fp_in = fopen(filename, "rb");
	if (fp_in == NULL) {
		fprintf(stderr, "%s: Could not open file %s\n", __func__, filename);
		return -3;
	}

	fread(header_check, sizeof(*header_check), sizeof(headers[0]), fp_in);

	if (memcmp(header_check, headers[ED_STATE], sizeof(headers[0])) == 0) {
		printf("%s: File %s is editor_state\n", __func__, filename);
	}
	else {
		printf("%s: Invalid header for file %s\n", __func__, filename);
		return -4;
	}

	*spell_list_ptr = read_spell_list_data(fp_in);
	*orb_list_ptr   = read_orb_list_data(fp_in, *spell_list_ptr);
	*item_list_ptr  = read_item_list_data(fp_in);
	*bag_list_ptr   = read_bag_list_data(fp_in, *item_list_ptr);
	*pc_ptr         = read_player_classes_data(fp_in);
	*pl_chars_ptr   = read_pch_list_data(fp_in, *pc_ptr, *orb_list_ptr,
	                                     *bag_list_ptr);
	*pl_parties_ptr = read_party_list_data(fp_in, *pl_chars_ptr);

	if (error_flag_sg == true) {
		fprintf(stderr, "%s: Operation aborted\n", __func__);

		*pl_parties_ptr = party_list_destroy(*pl_parties_ptr);
		*pl_chars_ptr   = pch_list_destroy(*pl_chars_ptr);
		*pc_ptr         = player_classes_destroy(*pc_ptr);
		*bag_list_ptr   = bag_list_destroy(*bag_list_ptr);
		*item_list_ptr  = item_list_destroy(*item_list_ptr);
		*orb_list_ptr   = orb_list_destroy(*orb_list_ptr);
		*spell_list_ptr = spell_list_destroy(*spell_list_ptr);

		error_flag_sg = false;
	}

	file_error_messages(fp_in, filename, __func__);

	fclose(fp_in);
	return 0;
}

int io_load_spells_orbs_file(const char *filename, Spell_list *spell_list_ptr,
                             Orb_list *orb_list_ptr)
{
	const struct io_loader_info info = {
		.header_single  = headers[SPL_LIST],
		.header_combi   = headers[SPL_ORB_LIST],
		.read_primary   = (void *(*)(FILE *)) read_spell_list_data,
		.read_secondary = (void *(*)(FILE *, void *)) read_orb_list_data,
		.destroy_primary   = (void *(*)(void *)) spell_list_destroy,
		.destroy_secondary = (void *(*)(void *)) orb_list_destroy,
		.type_name_single  = "Spell_list",
		.type_name_combi   = "Spell_list+Orb_list"
	};

	return file_loader_general(&info, filename, (void **)spell_list_ptr,
	                           (void **)orb_list_ptr, __func__);
}

int io_load_items_bags_file(const char *filename, Item_list *item_list_ptr,
                            Bag_list *bag_list_ptr)
{
	const struct io_loader_info info = {
		.header_single  = headers[ITM_LIST],
		.header_combi   = headers[ITM_BAG_LIST],
		.read_primary   = (void *(*)(FILE *)) read_item_list_data,
		.read_secondary = (void *(*)(FILE *, void *)) read_bag_list_data,
		.destroy_primary   = (void *(*)(void *)) item_list_destroy,
		.destroy_secondary = (void *(*)(void *)) bag_list_destroy,
		.type_name_single  = "Item_list",
		.type_name_combi   = "Item_list+Bag_list"
	};

	return file_loader_general(&info, filename, (void **)item_list_ptr,
	                           (void **)bag_list_ptr, __func__);
}

int io_load_classes_chars_file(const char *filename, Player_classes *pc_ptr,
                               Pch_list *pl_chars_ptr)
{
	const struct io_loader_info info = {
		.header_single  = headers[CLS_LIST],
		.header_combi   = headers[CLS_PCH_LIST],
		.read_primary   = (void *(*)(FILE *)) read_player_classes_data,
		.read_secondary = (void *(*)(FILE *, void *)) read_pch_list_data_simple,  // Won't read Orb or Bag info
		.destroy_primary   = (void *(*)(void *)) player_classes_destroy,
		.destroy_secondary = (void *(*)(void *)) pch_list_destroy,
		.type_name_single  = "Player_classes",
		.type_name_combi   = "Player_classes+Pch_list"
	};

	return file_loader_general(&info, filename, (void **)pc_ptr,
	                           (void **)pl_chars_ptr, __func__);
}

/* Unifies loading for all file types except editor_state */
static int file_loader_general(const struct io_loader_info *info,
                               const char *filename, void **primary_list_ptr,
                               void **secondary_list_ptr, const char *caller)
{
	if (filename == NULL) {
		return -1;
	}
	if (filename[0] == '\0') {
		return -2;
	}
	if (primary_list_ptr == NULL) {
		printf("%s: out by NULL\n", __func__);
		return -3;
	}

	char header_check[sizeof(headers[0])];
	FILE *fp_in;

	fp_in = fopen(filename, "rb");
	if (fp_in == NULL) {
		fprintf(stderr, "%s: Could not open file %s\n", caller, filename);
		return -4;
	}

	fread(header_check, sizeof(*header_check), sizeof(headers[0]), fp_in);

	if (memcmp(header_check, info->header_single, sizeof(headers[0])) == 0) {
		// File is single (primary list)
		printf("%s: File %s is %s\n", caller, filename, info->type_name_single);
		*primary_list_ptr = info->read_primary(fp_in);
	}
	else
	if (memcmp(header_check, info->header_combi, sizeof(headers[0])) == 0) {
		// File is combined (primary and secondary lists)
		printf("%s: File %s is %s\n", caller, filename, info->type_name_combi);
		*primary_list_ptr = info->read_primary(fp_in);

		if (secondary_list_ptr != NULL) {
			*secondary_list_ptr =
				info->read_secondary(fp_in, *primary_list_ptr);
		}
		else {
			fprintf(stderr,
			        "%s: secondary_list_ptr is NULL, loading only the %s\n",
			        caller, info->type_name_single);
		}
	}
	else {
		printf("%s: Invalid header for file %s\n", caller, filename);
		*primary_list_ptr = NULL;
		*secondary_list_ptr = NULL;

		fclose(fp_in);
		return -5;
	}

	if (error_flag_sg == true) {
		fprintf(stderr, "%s: Operation aborted\n", caller);

		*primary_list_ptr = info->destroy_primary(*primary_list_ptr);
		*secondary_list_ptr = info->destroy_secondary(*secondary_list_ptr);

		error_flag_sg = false;
	}

	file_error_messages(fp_in, filename, __func__);

	fclose(fp_in);
	return 0;
}


static void file_error_messages(FILE *fp, const char *filename,
                                const char *caller)
{
	if (ferror(fp)) {
		fprintf(stderr, "%s: Error indicator was set for file %s\n",
		                caller, filename);
	}
	if (feof(fp)) {
		fprintf(stderr, "%s: End of file indicator was set for file %s\n",
		                caller, filename);
	}
}

static void write_spell_list_data(FILE *fp_out, Spell_list spell_list)
{
	int qty_spells;
	int description_size;
	int spell_cost;
	Spell spell;

	qty_spells = spell_list_total(spell_list);
	fwrite(&qty_spells, sizeof(qty_spells), 1, fp_out);

	// Write list contents
	for (int i = 0; i < qty_spells; ++i) {
		spell = spell_list_get_spell(spell_list, i);
		spell_cost = spell_get_cost(spell);
		description_size = strlen(spell_get_description(spell)) + 1;

		fwrite(&spell_cost, sizeof(spell_cost), 1, fp_out);
		fwrite(spell_get_name(spell), sizeof(char), SPELL_NAME_LEN + 1, fp_out);
		fwrite(&description_size, sizeof(description_size), 1, fp_out);
		fwrite(spell_get_description(spell), sizeof(char), description_size,
		       fp_out);
	 }
}

static Spell_list read_spell_list_data(FILE *fp_in)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Spell_list spell_list;
	Spell spell;
	int num_spells;
	int cost_in_mp;
	char name[SPELL_NAME_LEN+1];
	int description_size;
	char *description;

	fread(&num_spells, sizeof(num_spells), 1, fp_in);

	if (num_spells < 0) {
		fprintf(stderr,
		        "%s: Spell list file yields NULL Spell_list object\n",
		        __func__);
		return NULL;
	}

	spell_list = spell_list_create();
	if (spell_list == NULL) {
		fprintf(stderr, "%s: Could not create Spell_list object\n", __func__);
		goto error_return_1;
	}

	// Load list from file
	for (int i = 0; i < num_spells; ++i) {
		fread(&cost_in_mp, sizeof(cost_in_mp), 1, fp_in);
		fread(name, sizeof(name), 1, fp_in);
		fread(&description_size, sizeof(description_size), 1, fp_in);

		description = malloc(description_size);
		if (description == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for description\n",
			                __func__);
			goto error_return_2;
		}
		fread(description, sizeof(*description), description_size, fp_in);

		spell = spell_create(name, description, cost_in_mp);
		if (spell == NULL) {
			fprintf(stderr, "%s: Could not create Spell object %d\n",
			                __func__, i);
			goto error_return_3;
		}

		if (spell_list_add(spell_list, spell) < 0) {
			fprintf(stderr, "%s: Could not add spell to list\n", __func__);
			goto error_return_4;
		}

		free(description);
	}

	return spell_list;

error_return_4:
	spell_destroy(spell);
error_return_3:
	free(description);
error_return_2:
	spell_list_destroy(spell_list);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

static void write_orb_list_data(FILE *fp_out, Orb_list orb_list,
                                Spell_list spell_list)
{
	int num_orbs;
	int num_spells;
	int spell_idx;
	Orb orb;

	num_orbs = orb_list_total(orb_list);
	fwrite(&num_orbs, sizeof(num_orbs), 1, fp_out);

	for (int i = 0; i < num_orbs; ++i) {
		orb = orb_list_get_orb(orb_list, i);
		num_spells = orb_get_num_spells(orb);

		fwrite(orb_get_name(orb), sizeof(char), ORB_NAME_LEN + 1, fp_out);
		fwrite(&num_spells, sizeof(num_spells), 1, fp_out);

		for (int j = 0; j < num_spells; ++j) {
			spell_idx = spell_list_find_spell_pos(spell_list,
			                                      orb_get_spell(orb, j));
			fwrite(&spell_idx, sizeof(spell_idx), 1, fp_out);
		}
	}
}

static Orb_list read_orb_list_data(FILE *fp_in, Spell_list spell_list)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Orb_list orb_list;
	int num_orbs;
	Orb orb;
	char orb_name[ORB_NAME_LEN+1];
	int num_spells;
	int *spell_idx;  // Will hold dynamic arrays

	fread(&num_orbs, sizeof(num_orbs), 1, fp_in);

	if (num_orbs < 0) {
		fprintf(stderr, "%s: Orb list file yields NULL Orb_list object\n",
		                __func__);
		return NULL;
	}

	orb_list = orb_list_create();
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Could not create Orb_list\n", __func__);
		goto error_return_1;
	}

	for (int i = 0; i < num_orbs; ++i) {
		fread(orb_name, sizeof(orb_name), 1, fp_in);
		fread(&num_spells, sizeof(num_spells), 1, fp_in);

		spell_idx = malloc(sizeof(*spell_idx) * num_spells);
		if (spell_idx == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for spell_idx array\n",
			        __func__);
			goto error_return_2;
		}
		fread(spell_idx, sizeof(*spell_idx), num_spells, fp_in);

		orb = orb_create(orb_name);
		if (orb == NULL) {
			fprintf(stderr, "%s: Could not create orb %s\n", __func__,
			                orb_name);
			goto error_return_3;
		}

		for (int j = 0; j < num_spells; ++j) {
			Spell spell = spell_list_get_spell(spell_list, spell_idx[j]);

			if (orb_add_spell(orb, spell) < 0) {
				fprintf(stderr, "%s: Couldn't add spell %d(%s) to orb %s\n",
				                __func__, j, spell_get_name(spell),
				                orb_get_name(orb));
				goto error_return_4;
			}
		}

		if (orb_list_add(orb_list, orb) < 0) {
			fprintf(stderr, "%s: Could not add orb %s to list\n", __func__,
			                orb_get_name(orb));
			goto error_return_4;
		}

		free(spell_idx);
	}

	return orb_list;

error_return_4:
	orb_destroy(orb);
error_return_3:
	free(spell_idx);
error_return_2:
	orb_list_destroy(orb_list);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

static void write_item_list_data(FILE *fp_out, Item_list item_list)
{
	int qty_items;
	int description_size;
	int item_size;
	Item item;

	qty_items = item_list_total(item_list);
	fwrite(&qty_items, sizeof(qty_items), 1, fp_out);

	// Write list contents
	for (int i = 0; i < qty_items; ++i) {
		item = item_list_get_item(item_list, i);
		item_size = item_get_size(item);
		description_size = strlen(item_get_description(item)) + 1;

		fwrite(&item_size, sizeof(item_size), 1, fp_out);
		fwrite(item_get_name(item), sizeof(char), ITEM_NAME_LEN + 1, fp_out);
		fwrite(&description_size, sizeof(description_size), 1, fp_out);
		fwrite(item_get_description(item), sizeof(char), description_size,
		       fp_out);
	 }
}

static Item_list read_item_list_data(FILE *fp_in)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Item_list item_list;
	Item item;
	int num_items;
	int size_in_bag;
	char name[SPELL_NAME_LEN+1];
	int description_size;
	char *description;

	fread(&num_items, sizeof(num_items), 1, fp_in);

	if (num_items < 0) {
		fprintf(stderr,
		        "%s: Item list file yields NULL Item_list object\n",
		        __func__);
		return NULL;
	}

	item_list = item_list_create();
	if (item_list == NULL) {
		fprintf(stderr, "%s: Could not create Item_list object\n", __func__);
		goto error_return_1;
	}

	// Load list from file
	for (int i = 0; i < num_items; ++i) {
		fread(&size_in_bag, sizeof(size_in_bag), 1, fp_in);
		fread(name, sizeof(name), 1, fp_in);
		fread(&description_size, sizeof(description_size), 1, fp_in);

		description = malloc(description_size);
		if (description == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for description\n",
			                __func__);
			goto error_return_2;
		}
		fread(description, sizeof(*description), description_size, fp_in);

		item = item_create(name, description, size_in_bag);
		if (item == NULL) {
			fprintf(stderr, "%s: Could not create Item object %d\n",
			                __func__, i);
			goto error_return_3;
		}

		if (item_list_add(item_list, item) < 0) {
			fprintf(stderr, "%s: Could not add item to list\n", __func__);
			goto error_return_4;
		}

		free(description);
	}

	return item_list;

error_return_4:
	item_destroy(item);
error_return_3:
	free(description);
error_return_2:
	item_list_destroy(item_list);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

static void write_bag_list_data(FILE *fp_out, Bag_list bag_list,
                                Item_list item_list)
{
	int num_bags;
	int bag_size;
	int num_items;
	int item_idx;
	Bag bag;

	num_bags = bag_list_total(bag_list);
	fwrite(&num_bags, sizeof(num_bags), 1, fp_out);

	for (int i = 0; i < num_bags; ++i) {
		bag = bag_list_get_bag(bag_list, i);
		bag_size = bag_get_size(bag);
		num_items = bag_get_num_items(bag);

		fwrite(bag_get_name(bag), sizeof(char), BAG_NAME_LEN + 1, fp_out);
		fwrite(&bag_size, sizeof(bag_size), 1, fp_out);
		fwrite(&num_items, sizeof(num_items), 1, fp_out);

		for (int j = 0; j < num_items; ++j) {
			item_idx = item_list_find_item_pos(item_list,
			                                   bag_get_item(bag, j));
			fwrite(&item_idx, sizeof(item_idx), 1, fp_out);
		}
	}
}

static Bag_list read_bag_list_data(FILE *fp_in, Item_list item_list)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Bag_list bag_list;
	int num_bags;
	Bag bag;
	char bag_name[BAG_NAME_LEN+1];
	int bag_size;
	int num_items;
	int *item_idx;  // Will hold dynamic arrays

	fread(&num_bags, sizeof(num_bags), 1, fp_in);

	if (num_bags < 0) {
		fprintf(stderr, "%s: Bag list file yields NULL Bag_list object\n",
		                __func__);
		return NULL;
	}

	bag_list = bag_list_create();
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Could not create bag list\n", __func__);
		goto error_return_1;
	}

	for (int i = 0; i < num_bags; ++i) {
		fread(bag_name, sizeof(bag_name), 1, fp_in);
		fread(&bag_size, sizeof(bag_size), 1, fp_in);
		fread(&num_items, sizeof(num_items), 1, fp_in);

		item_idx = malloc(sizeof(*item_idx) * num_items);
		if (item_idx == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for item_idx array\n",
			        __func__);
			goto error_return_2;
		}
		fread(item_idx, sizeof(*item_idx), num_items, fp_in);

		bag = bag_create(bag_size, bag_name);
		if (bag == NULL) {
			fprintf(stderr, "%s: Could not create bag %s\n", __func__,
			                bag_get_name(bag));
			goto error_return_3;
		}

		for (int j = 0; j < num_items; ++j) {
			Item item = item_list_get_item(item_list, item_idx[j]);

			if (bag_add_item(bag, item) < 0) {
				fprintf(stderr, "%s: Couldn't add item %d (%s) to bag %s\n",
				                __func__, j, item_get_name(item),
				                bag_get_name(bag));
				goto error_return_4;
			}
		}

		if (bag_list_add(bag_list, bag) < 0) {
			fprintf(stderr, "%s: Could not add bag %s to list\n", __func__,
			                bag_get_name(bag));
			goto error_return_4;
		}

		free(item_idx);
	}

	return bag_list;

error_return_4:
	bag_destroy(bag);
error_return_3:
	free(item_idx);
error_return_2:
	bag_list_destroy(bag_list);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

static void write_player_classes_data(FILE *fp_out, Player_classes pc)
{
	int num_pclasses;
	const struct player_class *cur_pclass;

	num_pclasses = player_classes_total(pc);
	fwrite(&num_pclasses, sizeof(num_pclasses), 1, fp_out);

	for (int i = 0; i < num_pclasses; ++i) {
		cur_pclass = player_classes_get_class(pc, i);
		fwrite(cur_pclass, sizeof(*cur_pclass), 1, fp_out);
	}
}

static Player_classes read_player_classes_data(FILE *fp_in)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Player_classes pc;
	struct player_class cur_pclass;
	int num_pclasses;
	int read_pclasses;

	fread(&(num_pclasses), sizeof(num_pclasses), 1, fp_in);

	if (num_pclasses < 0) {
		fprintf(stderr, "%s: Classes file yields NULL Player_classes object\n",
                        __func__);
		return NULL;
	}

	pc = player_classes_create();
	if (pc == NULL) {
		fprintf(stderr, "%s: Can't create Player_classes object\n", __func__);
		error_flag_sg = true;
		return NULL;
	}

	for (read_pclasses = 0; read_pclasses < num_pclasses; ++read_pclasses) {
		fread(&cur_pclass, sizeof(cur_pclass), 1, fp_in);
		player_classes_add(pc, &cur_pclass);
	}

	// Keep this part?
	if (read_pclasses < num_pclasses) {
		fprintf(stderr, "%s: only %d classes from %d loaded\n", __func__,
                        read_pclasses, num_pclasses);
	}

	return pc;
}

static void write_pch_list_data(FILE *fp_out, Pch_list pl_chars,
                                Player_classes pc, Orb_list orb_list,
                                Bag_list bag_list)
{
	Player_character pch;
	int num_characters = pch_list_total(pl_chars);

	fwrite(&num_characters, sizeof(num_characters), 1, fp_out);

	for (int i = 0; i < num_characters; ++i) {
		pch = pch_list_get_pch(pl_chars, i);
		write_player_character_data(fp_out, pch, pc, orb_list, bag_list);
	}
}

/* Does not save Orb nor Bag data */
static void write_pch_list_data_simple(FILE *fp_out, Pch_list pl_chars,
                                       Player_classes pc)
{
	write_pch_list_data(fp_out, pl_chars, pc, NULL, NULL);
}

static Pch_list read_pch_list_data(FILE *fp_in, Player_classes pc,
                                   Orb_list orb_list, Bag_list bag_list)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Pch_list pl_chars;
	Player_character pch;
	int num_characters;

	fread(&num_characters, sizeof(num_characters), 1, fp_in);

	if (num_characters < 0) {
		fprintf(stderr,
		        "%s: Pch list file yields NULL Pch_list object\n", __func__);
		return NULL;
	}

	pl_chars = pch_list_create();
	if (pl_chars == NULL) {
		fprintf(stderr, "%s: Could not create Pch_list object\n", __func__);
		goto error_return_1;
	}

	// Load list from file
	for (int i = 0; i < num_characters; ++i) {
		pch = read_player_character_data(fp_in, pc, orb_list, bag_list);
		if (pch == NULL) {
			fprintf(stderr, "%s: Player_character %d data not read\n", __func__,
			                i);
			goto error_return_2;
		}

		pch_list_add(pl_chars, pch);
	}

	return pl_chars;

error_return_2:
	pch_list_destroy(pl_chars);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

/* Does not read Orb or Bag data, can be used with file_loader_general */
static Pch_list read_pch_list_data_simple(FILE *fp_in, Player_classes pc)
{
	return read_pch_list_data(fp_in, pc, NULL, NULL);
}

static void write_player_character_data(FILE *fp_out, Player_character pch,
                                        Player_classes pc,
                                        Orb_list orb_list,
                                        Bag_list bag_list)
{
	int level = player_character_get_level(pch);
	int pclass_idx = player_classes_find_class_pos(pc,
	                                           player_character_get_class(pch));
	int orb_idx = orb_list_find_orb_pos(orb_list,
	                                    player_character_get_orb(pch));
	int bag_idx = bag_list_find_bag_pos(bag_list,
	                                    player_character_get_bag(pch));
	char name[CHAR_NAME_LEN+1];

	strncpy(name, player_character_get_name(pch), CHAR_NAME_LEN);
	name[CHAR_NAME_LEN] = '\0';

	fwrite(&level, sizeof(level), 1, fp_out);
	fwrite(&pclass_idx, sizeof(pclass_idx), 1, fp_out);
	fwrite(&orb_idx, sizeof(orb_idx), 1, fp_out);  // Will store a negative number if no orb is loaded for character or orb_list is NULL
	fwrite(&bag_idx, sizeof(bag_idx), 1, fp_out);  // Will store a negative number if no bag is loaded for character or bag_list is NULL
	fwrite(name, sizeof(name), 1, fp_out);
}

/*
* Currently this function is only called from read_pch_list_data and it is not
* concerned about checking or setting error_flag_sg, the caller manages that
*/
static Player_character read_player_character_data(FILE *fp_in,
                                                   Player_classes pc,
                                                   Orb_list orb_list,
                                                   Bag_list bag_list)
{
	Player_character pch;
	int level;
	int pclass_idx;
	int orb_idx;
	int bag_idx;
	const struct player_class *cur_pclass;
	Orb cur_orb;
	Bag cur_bag;
	char name[CHAR_NAME_LEN+1];

	pch = player_character_create();
	if (pch == NULL) {
		fprintf(stderr, "%s: Could not create Player_character object\n",
		                __func__);
		return NULL;
	}

	fread(&level, sizeof(level), 1, fp_in);
	fread(&pclass_idx, sizeof(pclass_idx), 1, fp_in);
	fread(&orb_idx, sizeof(orb_idx), 1, fp_in);
	fread(&bag_idx, sizeof(bag_idx), 1, fp_in);
	fread(name, sizeof(name), 1, fp_in);

	cur_pclass = player_classes_get_class(pc, pclass_idx);
	cur_orb = orb_list_get_orb(orb_list, orb_idx);
	cur_bag = bag_list_get_bag(bag_list, bag_idx);

	player_character_set_level(pch, level);
	player_character_set_class(pch, cur_pclass);
	player_character_set_orb(pch, cur_orb);
	player_character_set_bag(pch, cur_bag);
	player_character_set_name(pch, name);

	return pch;
}

static void write_party_list_data(FILE *fp_out, Party_list pl_parties,
                                  Pch_list pl_chars)
{
	Player_party party;
	int num_parties = party_list_total(pl_parties);

	fwrite(&num_parties, sizeof(num_parties), 1, fp_out);

	if (num_parties <= 0) {
		return;
	}

	for (int i = 0; i < num_parties; ++i) {
		party = party_list_get_party(pl_parties, i);
		write_player_party_data(fp_out, party, pl_chars);
	}
}

static void write_player_party_data(FILE *fp_out, Player_party pl_party,
                                    Pch_list pl_chars)
{
	Player_character pch;
	char party_name[PARTY_NAME_LEN+1];
	int pch_idx;
	int num_characters = player_party_get_num_chars(pl_party);

	strncpy(party_name, player_party_get_name(pl_party), PARTY_NAME_LEN);
	party_name[PARTY_NAME_LEN] = '\0';

	fwrite(party_name, sizeof(*party_name), PARTY_NAME_LEN + 1, fp_out);
	fwrite(&num_characters, sizeof(num_characters), 1, fp_out);

	if (num_characters <= 0) {
		return;
	}

	for (int char_pos = 0; char_pos < PARTY_NUM_POSITIONS; ++char_pos) {
		pch = player_party_get_char(pl_party, char_pos);
		if (pch == NULL) {
			continue;
		}

		pch_idx = pch_list_find_pch_pos(pl_chars, pch);

		fwrite(&pch_idx, sizeof(pch_idx), 1, fp_out);
		fwrite(&char_pos, sizeof(char_pos), 1, fp_out);  // Position in party
	}
}

static Party_list read_party_list_data(FILE *fp_in, Pch_list pl_chars)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Party_list pl_parties;
	Player_party party;
	int num_parties;

	fread(&num_parties, sizeof(num_parties), 1, fp_in);
	if (num_parties < 0) {
		fprintf(stderr, "%s: Party list data gets NULL Party_list object\n",
		                __func__);
		return NULL;
	}

	pl_parties = party_list_create();
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Could not create Party_list object\n", __func__);
		goto error_return_1;
	}

	for (int i = 0; i < num_parties; ++i) {
		party = read_player_party_data(fp_in, pl_chars);
		if (party_list_add(pl_parties, party) < 0) {
			goto error_return_2;
		}
	}

	return pl_parties;

error_return_2:
	party_list_destroy(pl_parties);
error_return_1:
	error_flag_sg = true;
	return NULL;
}

static Player_party read_player_party_data(FILE *fp_in, Pch_list pl_chars)
{
	if (error_flag_sg == true) {
		return NULL;
	}

	Player_party pl_party;
	Player_character pch;
	char party_name[PARTY_NAME_LEN+1];
	int num_characters;
	int pch_idx;
	int char_pos;

	fread(party_name, sizeof(party_name), 1, fp_in);

	fread(&num_characters, sizeof(num_characters), 1, fp_in);
	if (num_characters < 0) {
		fprintf(stderr, "%s: Player party data gets NULL Player_party object\n",
		                __func__);
		return NULL;
	}
	if (num_characters > PARTY_NUM_POSITIONS) {
		fprintf(stderr, "%s: %d is not a valid number of characters\n",
		                __func__, num_characters);
		goto error_return_1;
	}

	pl_party = player_party_create();
	if (pl_party == NULL) {
		fprintf(stderr, "%s: Could not create Player_party object\n", __func__);
		goto error_return_1;
	}

	player_party_set_name(pl_party, party_name);

	for (int i = 0; i < num_characters; ++i) {
		fread(&pch_idx, sizeof(pch_idx), 1, fp_in);
		fread(&char_pos, sizeof(char_pos), 1, fp_in);

		pch = pch_list_get_pch(pl_chars, pch_idx);

		player_party_add_char(pl_party, pch);
		player_party_swap_position(pl_party, i, char_pos);  // Assumes player_party_add_char starts from pos 0 in empty party (could use its return value for not assuming)
	}

	return pl_party;

error_return_1:
	error_flag_sg = true;
	return NULL;
}