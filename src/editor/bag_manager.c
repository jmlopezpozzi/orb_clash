/* bag_manager.c */

#include "editor_public.h"
#include "../adt/g_item_adt.h"
#include "../adt/item_list_adt.h"
#include "../adt/bag_adt.h"
#include "../adt/bag_list_adt.h"
#include "../adt/pch_list_adt.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static Bag_list option_add_bag(Bag_list bag_list, Item_list item_list);
static int add_items_to_bag(Item_list item_list, Bag bag);
static void option_edit_bag(Bag_list bag_list, Item_list item_list);
static void option_list_bags(Bag_list bag_list);
static void option_copy_bag(Bag_list bag_list);
static void option_save_bag_list(Bag_list bag_list, Item_list item_list);

Bag_list bag_manager(Item_list item_list, Bag_list bag_list, Pch_list pl_chars)
{
	enum {BACK = 0, ADD_BAG, REMOVE_BAG, LIST_BAGS, EDIT_BAG, COPY_BAG,
	                CLEAR_BAGS, SAVE_BAGS};
	int option;

	for (;;) {
		printf("\nBag manager\n");
		printf("(%d-Add bag, %d-Remove bag, %d-List bags,\n"
		       " %d-Edit bag, %d-Copy bag, %d-Clear bag list,\n"
		       " %d-Save bag list, %d-Back)\n:",
		       ADD_BAG, REMOVE_BAG, LIST_BAGS, EDIT_BAG, COPY_BAG, CLEAR_BAGS,
		       SAVE_BAGS, BACK);

		option = flushed_scanf_d(FSD_ERR_VAL);
		switch (option) {
		case BACK:
			return bag_list;

		case ADD_BAG:
			bag_list = option_add_bag(bag_list, item_list);
			break;

		case REMOVE_BAG:
			ep_option_remove_bag(bag_list, pl_chars);
			break;

		case LIST_BAGS:
			option_list_bags(bag_list);
			break;

		case EDIT_BAG:
			option_edit_bag(bag_list, item_list);
			break;

		case COPY_BAG:
			option_copy_bag(bag_list);
			break;

		case CLEAR_BAGS:
			pch_list_update_removed_bags(pl_chars, NULL, 0);
			bag_list = bag_list_destroy(bag_list);
			break;

		case SAVE_BAGS:
			option_save_bag_list(bag_list, item_list);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Destroys empty list
		if (bag_list != NULL && bag_list_total(bag_list) == 0) {
			bag_list = bag_list_destroy(bag_list);  // bag_list becomes NULL
		}
	}
}

static Bag_list option_add_bag(Bag_list bag_list, Item_list item_list)
{
	char bag_name[BAG_NAME_LEN+1];
	Bag new_bag;
	int bag_size;

	if (bag_list == NULL) {
		bag_list = bag_list_create();
		if (bag_list == NULL) {
			printf("Could not create bag list\n");
			return NULL;
		}
	}

	printf("Enter name for bag: ");
	read_line(bag_name, BAG_NAME_LEN);
	if (bag_name[0] == '\0') {
		printf("Bag name cannot be empty\n");
		return bag_list;
	}

	printf("Enter bag size: ");
	bag_size = flushed_scanf_d(FSD_ERR_VAL);
	if (bag_size < 1) {
		printf("Bag size must be positive\n");
		return bag_list;
	}

	new_bag = bag_create(bag_size, bag_name);
	if (new_bag == NULL) {
		printf("Could not create bag %s\n", bag_name);
		return bag_list;
	}

	if (bag_list_add(bag_list, new_bag) < 0) {
		printf("Could not add bag %s to list\n", bag_name);
		bag_destroy(new_bag);
		return bag_list;
	}

	add_items_to_bag(item_list, new_bag);  // Bags can be empty

	return bag_list;
}

static int add_items_to_bag(Item_list item_list, Bag bag)
{
	if (item_list == NULL) {
		printf("No items defined yet!\n");
		return -1;
	}
	if (item_list_total(item_list) < 1) {
		printf("Item list is empty\n");
		return -2;
	}

	int items_added = 0;
	int choice = 0;

	ep_show_item_list(item_list, false);
	for (;;) {
		printf("Choose item to add: ");
		choice = flushed_scanf_d(FSD_ERR_VAL);

		if (choice > 0 && choice <= item_list_total(item_list)) {
			Item item = item_list_get_item(item_list, --choice);

			if (item_get_size(item) > bag_get_free_space(bag)) {
				printf("Not enough space in bag %s (%d) for item %s (%d)\n",
				       bag_get_name(bag), bag_get_free_space(bag),
				       item_get_name(item), item_get_size(item));
			}
			else
			if (bag_add_item(bag, item) < 0) {
				printf("Error adding item to bag\n");
				return -1;
			}
			else {
				printf("Added %s to %s\n", item_get_name(item),
				                           bag_get_name(bag));
				++items_added;
			}
		}
		else {
			printf("No item in position %d\n", choice);
			printf("Add another item? (y)\n:");
			if (accept_prompt('y') == false) {
				break;
			}
		}
	}

	return items_added;
}

static void option_edit_bag(Bag_list bag_list, Item_list item_list)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return;
	}

	enum {BACK = 0, ADD_ITEM, REMOVE_ITEM, SHOW_CONTENTS, CHANGE_SIZE,
	                CHANGE_NAME};
	int choice;
	Bag bag;

	bag = ep_choose_bag_from_list_obj(bag_list, "edit");
	if (bag == NULL) {
		return;
	}

	for (;;) {
		printf("\nEditing bag %s\n", bag_get_name(bag));
		printf("(%d-Add Item, %d-Remove Item, %d-Show contents,\n"
		       " %d-Change size, %d-Change name, %d-Back)\n:",
		       ADD_ITEM, REMOVE_ITEM, SHOW_CONTENTS, CHANGE_SIZE, CHANGE_NAME,
		       BACK);
		choice = flushed_scanf_d(FSD_ERR_VAL);

		switch (choice) {
		case BACK:
			return;

		case ADD_ITEM:
			add_items_to_bag(item_list, bag);
			break;

		case REMOVE_ITEM:
			ep_option_remove_item_from_bag(bag);
			// Bags can be empty
			break;

		case SHOW_CONTENTS:
			printf("Show descriptions? (y)\n:");
			ep_show_bag_contents(bag, accept_prompt('y'));
			printf("Free space: %d\nTotal size: %d\n",
			       bag_get_free_space(bag), bag_get_size(bag));
			break;

		case CHANGE_SIZE:
			{
				int new_size;

				printf("Current size: %d\n", bag_get_size(bag));
				printf("Enter new size: ");
				new_size = flushed_scanf_d(FSD_ERR_VAL);
				if (new_size < 1) {
					printf("Size must be positive\n");
					break;
				}
				if (new_size < bag_get_used_space(bag)) {
					printf("New size is less than used space, operation will\n"
					       "empty items from bag, continue? (y)\n:");
					if (accept_prompt('y') == false) {
						break;
					}
					while (bag_remove_item(bag, 0) > 0) { ; }
				}
				bag_set_size(bag, new_size);
			}
			break;

		case CHANGE_NAME:
			{
				char new_name[BAG_NAME_LEN+1];

				printf("Enter new name: ");
				read_line(new_name, BAG_NAME_LEN);
				if (new_name[0] == '\0') {
					printf("Bag name cannot be empty\n");
				}
				else {
					bag_set_name(bag, new_name);
				}
			}
			break;

		default:
			printf("Option %d not recognised\n", choice);
			break;
		}
	}
}

static void option_list_bags(Bag_list bag_list)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return;
	}

	printf("Show contents? (y)\n:");
	ep_show_bag_list(bag_list, accept_prompt('y'));
}

static void option_copy_bag(Bag_list bag_list)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return;
	}

	Bag src_bag;
	Bag new_bag;

	src_bag = ep_choose_bag_from_list_obj(bag_list, "copy");
	if (src_bag == NULL) {
		return;
	}
	new_bag = bag_copy(src_bag);
	printf("New bag added in position %d\n",
	       bag_list_add(bag_list, new_bag) - 1);  // Function returns quantity, position is last!
}

static void option_save_bag_list(Bag_list bag_list, Item_list item_list)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return;
	}
	if (bag_list_total(bag_list) < 1) {
		printf("Bag list is empty\n");
		return;
	}
	if (item_list == NULL) {
		printf("No items defined yet!\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Enter file name: ");
	read_line(filename, FILENAME_MAX - 1);

	if (filename[0] == '\0') {
		printf("File name cannot be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_items_bags_file(filename, item_list, bag_list);
	}
	else {
		io_save_items_bags_file(filename, item_list, bag_list);
	}
}