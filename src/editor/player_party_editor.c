/* player_party_editor.c */

#include "editor_public.h"
#include "../adt/player_party_adt.h"
#include "../adt/party_list_adt.h"
#include "../adt/player_character_adt.h"
#include "../adt/pch_list_adt.h"
#include "../adt/orb_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/bag_adt.h"
#include "../adt/bag_list_adt.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>

/* Party list editor functions */
static Party_list option_remove_empty_parties(Party_list pl_parties);
static Party_list option_new_party(Party_list pl_parties);
static void option_remove_party(Party_list pl_parties);
static void option_list_parties(Party_list pl_parties);
static Party_list option_clear_parties(Party_list pl_parties);

/* Party editor functions */
static void option_edit_party(Party_list pl_parties, Pch_list pl_chars,
                              Orb_list orb_list, Bag_list bag_list);
static void option_list_party(Player_party pl_party);
static Player_party option_add_char(Player_party pl_party, Pch_list pl_chars);
static Player_party option_remove_char(Player_party pl_party);
static void option_move_char(Player_party pl_party);
static void option_edit_char(Player_party pl_party, Orb_list orb_list,
                             Bag_list bag_list);
static void option_equal_levels(Player_party pl_party);
static Player_party option_clear_party(Player_party pl_party);


Party_list player_parties_editor(Party_list pl_parties, Pch_list pl_chars,
                                 Orb_list orb_list, Bag_list bag_list)
{
	enum {BACK = 0, EDIT_PARTY, NEW_PARTY, LIST, REMOVE_PARTY, REMOVE_EMPTY,
	                CLEAR};
	int option;

	for (;;) {
		printf("\nPlayer Parties Editor\n");
		printf("(%d-Edit party, %d-New party, %d-List parties,\n"
		       " %d-Remove party, %d-Remove empty parties, %d-Clear parties,\n"
		       " %d-Back)\n:",
		       EDIT_PARTY, NEW_PARTY, LIST, REMOVE_PARTY, REMOVE_EMPTY, CLEAR,
		       BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return pl_parties;

		case EDIT_PARTY:
			option_edit_party(pl_parties, pl_chars, orb_list, bag_list);
			break;

		case NEW_PARTY:
			pl_parties = option_new_party(pl_parties);
			break;

		case LIST:
			option_list_parties(pl_parties);
			break;

		case REMOVE_PARTY:
			option_remove_party(pl_parties);
			break;

		case REMOVE_EMPTY:
			pl_parties = option_remove_empty_parties(pl_parties);
			break;

		case CLEAR:
			pl_parties = option_clear_parties(pl_parties);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		if (pl_parties != NULL && party_list_total(pl_parties) < 1) {
			pl_parties = party_list_destroy(pl_parties);
		}
	}
}


static Party_list option_remove_empty_parties(Party_list pl_parties)
{
	if (pl_parties == NULL) {
		return NULL;
	}

	Player_party party;
	int *removed_parties_idx;  // Dynamic array
	int qty_removed = 0;
	int num_parties = party_list_total(pl_parties);

	if (num_parties < 1) {
		party_list_destroy(pl_parties);
		return NULL;
	}

	removed_parties_idx = malloc(sizeof(*removed_parties_idx) * num_parties);
	if (removed_parties_idx == NULL) {
		fprintf(stderr, "%s: Can't allocate memory for index array, will not\n"
		                "remove empty parties from list\n", __func__);
		return pl_parties;
	}

	for (int i = 0; i < num_parties; ++i) {
		party = party_list_get_party(pl_parties, i);

		if (player_party_get_num_chars(party) < 1) {
			removed_parties_idx[qty_removed++] = i;
			printf("Party %s is now empty and removed\n",
			       player_party_get_name(party));
		}
	}

	for (int i = 0; i < qty_removed; ++i) {
		party_list_remove(pl_parties, removed_parties_idx[i] - i);
	}

	free(removed_parties_idx);

	if (party_list_total(pl_parties) < 1) {
		pl_parties = party_list_destroy(pl_parties);  // pl_parties is now NULL
	}

	return pl_parties;
}

static Party_list option_new_party(Party_list pl_parties)
{
	char name[PARTY_NAME_LEN+1];
	Player_party party;

	if (pl_parties == NULL) {
		pl_parties = party_list_create();
		if (pl_parties == NULL) {
			fprintf(stderr, "%s: Error creating Party_list\n", __func__);
			return NULL;
		}
	}

	printf("Enter name to identify party: ");
	read_line(name, PARTY_NAME_LEN);

	party = player_party_create();
	if (party == NULL) {
		fprintf(stderr, "%s: Error creating Player_party object\n", __func__);
		return pl_parties;
	}

	player_party_set_name(party, name);
	party_list_add(pl_parties, party);  // Maybe print a message with the position of the party

	return pl_parties;
}

static void option_remove_party(Party_list pl_parties)
{
	if (pl_parties == NULL) {
		printf("No Party List defined yet!\n");
		return;
	}

	int pos = ep_choose_party_from_list_idx(pl_parties, "remove");

	if (pos < 0) {
		return;
	}

	party_list_remove(pl_parties, pos);
}

static void option_list_parties(Party_list pl_parties)
{
	if (pl_parties == NULL) {
		printf("No Party List defined yet!\n");
		return;
	}

	printf("Show details? (y)\n:");
	ep_show_party_list(pl_parties, accept_prompt('y'));
}

static Party_list option_clear_parties(Party_list pl_parties)
{
	return party_list_destroy(pl_parties);  // Returns NULL
}

static void option_edit_party(Party_list pl_parties, Pch_list pl_chars,
                              Orb_list orb_list, Bag_list bag_list)
{
	if (pl_parties == NULL) {
		printf("No Party List defined yet!\n");
		return;
	}
	if (party_list_total(pl_parties) < 1) {
		printf("Party list is empty\n");
		return;
	}

	enum {BACK = 0, ADD_CHAR, REMOVE_CHAR, LIST_PARTY, EDIT_CHAR,
	                EQUAL_LEVELS, MOVE_CHAR, CLEAR_PARTY};
	int option;
	Player_party pl_party;

	pl_party = ep_choose_party_from_list_obj(pl_parties, "edit");
	if (pl_party == NULL) {
		return;
	}

	for (;;) {
		printf("\nEditing party %s\n", player_party_get_name(pl_party));
		printf("(%d-Add Char, %d-Remove Char, %d-List Party, %d-Edit Char,\n"
		       " %d-Equal levels, %d-Move Char, %d-Clear party, %d-Back)\n:",
		       ADD_CHAR, REMOVE_CHAR, LIST_PARTY, EDIT_CHAR, EQUAL_LEVELS,
		       MOVE_CHAR, CLEAR_PARTY, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return;

		case ADD_CHAR:
			pl_party = option_add_char(pl_party, pl_chars);
			break;

		case REMOVE_CHAR:
			pl_party = option_remove_char(pl_party);
			break;

		case LIST_PARTY:
			option_list_party(pl_party);
			break;

		case EDIT_CHAR:
			option_edit_char(pl_party, orb_list, bag_list);
			break;

		case EQUAL_LEVELS:
			option_equal_levels(pl_party);
			break;

		case MOVE_CHAR:
			option_move_char(pl_party);
			break;

		case CLEAR_PARTY:
			pl_party = option_clear_party(pl_party);
			break;

		default:
			printf("Option %d not recognised\n", option);
		}
	}
}

static void option_list_party(Player_party pl_party)
{
	ep_show_party_info(pl_party, true);
}

static Player_party option_add_char(Player_party pl_party, Pch_list pl_chars)
{
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return pl_party;
	}
	if (pch_list_total(pl_chars) <= 0) {
		printf("Player character list is empty\n");
		return pl_party;
	}

	Player_character added_pch;

	if (player_party_get_num_chars(pl_party) >= PARTY_NUM_POSITIONS) {
		printf("Party is full\n");
		return pl_party;
	}

	added_pch = ep_choose_character_from_list_obj(pl_chars, "add");
	if (added_pch == NULL) {
		printf("Could not add player character\n");
		return pl_party;
	}

	player_party_add_char(pl_party, added_pch);

	return pl_party;
}

static Player_party option_remove_char(Player_party pl_party)
{
	if (pl_party == NULL) {
		printf("No player party defined yet!\n");
		return NULL;
	}

	int char_position;
	Player_character pch;

	option_list_party(pl_party);
	printf("Enter character position: ");
	char_position = flushed_scanf_d(FSD_ERR_VAL);
	if (char_position <= 0 || char_position > PARTY_NUM_POSITIONS) {
		printf("There is no position %d\n", char_position);
		return pl_party;
	}

	--char_position;
	pch = player_party_remove_char(pl_party, char_position);
	if (pch == NULL) {
		printf("No character in position %d\n", char_position + 1);
		return pl_party;
	}

	return pl_party;
}

static void option_move_char(Player_party pl_party)
{
	if (pl_party == NULL) {
		printf("No player party defined yet!\n");
		return;
	}

	int from_pos;
	int to_pos;

	option_list_party(pl_party);

	printf("Choose character by position: ");
	from_pos = flushed_scanf_d(FSD_ERR_VAL);
	if (from_pos <= 0 || from_pos > PARTY_NUM_POSITIONS) {
		printf("There is no position %d\n", from_pos);
		return;
	}

	printf("Choose destination position: ");
	to_pos = flushed_scanf_d(FSD_ERR_VAL);
	if (to_pos <= 0 || to_pos > PARTY_NUM_POSITIONS) {
		printf("There is no position %d\n", to_pos);
		return;
	}

	if (from_pos == to_pos) {
		return;
	}
	--from_pos;
	--to_pos;

	player_party_swap_position(pl_party, from_pos, to_pos);
}

static void option_edit_char(Player_party pl_party, Orb_list orb_list,
                             Bag_list bag_list)
{
	if (pl_party == NULL) {
		printf("No player party defined yet!\n");
		return;
	}

	Player_character editing_pch;
	int char_pos;

	for (;;) {
		option_list_party(pl_party);

		printf("Choose character to edit: ");
		char_pos = flushed_scanf_d(FSD_ERR_VAL);
		if (char_pos <= 0 || char_pos > PARTY_NUM_POSITIONS) {
			printf("There is no position %d\n", char_pos);
			return;
		}

		--char_pos;
		editing_pch = player_party_get_char(pl_party, char_pos);
		if (editing_pch == NULL) {
			printf("There is no character in position %d\n", char_pos + 1);
			return;
		}

		for (;;) {
			enum {BACK = 0, CHANGE_LEVEL, SET_ORB, SET_BAG, VIEW_INFO};
			int option;

			printf("(%d-Change level, %d-Set orb, %d-Set bag,\n"
			       " %d-View info, %d-Back)\n:",
			       CHANGE_LEVEL, SET_ORB, SET_BAG, VIEW_INFO, BACK);
			option = flushed_scanf_d(FSD_ERR_VAL);

			if (option == BACK) {
				break;
			}

			switch (option) {
			case CHANGE_LEVEL:
				printf("Level: %d\n", player_character_get_level(editing_pch));
				ep_enter_character_level(editing_pch);
				break;

			case SET_ORB:
				ep_choose_character_orb(editing_pch, orb_list);
				break;

			case SET_BAG:
				ep_choose_character_bag(editing_pch, bag_list);
				break;

			case VIEW_INFO:
				ep_show_character_info(editing_pch, true, true);
				break;

			default:
				printf("Option %d not recognised\n", option);
				break;
			}
		}
	}
}

static void option_equal_levels(Player_party pl_party)
{
	if (pl_party == NULL) {
		printf("No player party defined yet!\n");
		return;
	}

	Player_character pch;
	int level;

	option_list_party(pl_party);
	printf("Enter new level for all: ");
	level = flushed_scanf_d(FSD_ERR_VAL);
	if (level <= 0) {
		printf("Level must be positive\n");
		return;
	}

	for (int i = 0; i < PARTY_NUM_POSITIONS; ++i) {
		pch = player_party_get_char(pl_party, i);
		player_character_set_level(pch, level);
	}
}

static Player_party option_clear_party(Player_party pl_party)
{
	return player_party_destroy(pl_party);  // Returns NULL
}