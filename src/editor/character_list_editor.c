/* character_list_editor.c */

#include "editor_public.h"
#include "../adt/player_classes_adt.h"
#include "../adt/player_character_adt.h"
#include "../adt/pch_list_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/bag_list_adt.h"
#include "../adt/party_list_adt.h"
#include "../player_class.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int choose_character_class(Player_character pch, Player_classes pc);
static int enter_character_name(Player_character pch);
static Pch_list option_add_character(Pch_list pl_chars, Player_classes pc);
static void option_list_characters(Pch_list pl_chars);
static void option_edit_character(Pch_list pl_chars, Player_classes pc,
                                  Orb_list orb_list, Bag_list bag_list);
static void option_equal_levels(Pch_list pl_chars);
static Pch_list option_generate_characters_from_classes(Pch_list pl_chars,
                                                        Player_classes pc);
static void get_new_char_name(char *name_a, size_t name_len_max,
                              Player_classes pc, int pclass_pos);
static Pch_list option_clear_character_list(Pch_list pl_chars,
                                            Party_list pl_parties);
static void option_save_pch_list(Pch_list pl_chars, Player_classes pc);


Pch_list character_list_editor(Player_classes pc, Pch_list pl_chars,
                               Orb_list orb_list, Bag_list bag_list,
                               Party_list parties)
{
	enum options {BACK = 0, ADD_CHAR, REMOVE_CHAR, LIST_CHARS, EDIT_CHAR,
	                        EQUAL_LEVELS, GENERATE_FROM_CLASSES, CLEAR, SAVE};
	int option;

	for (;;) {
		printf("\nCharacter List Editor\n"
		       "(%d-Add, %d-Remove, %d-List, %d-Edit,\n"
		       " %d-Equal levels, %d-Generate one character per class,\n"
		       " %d-Clear, %d-Save, %d-Back)\n:",
		       ADD_CHAR, REMOVE_CHAR, LIST_CHARS, EDIT_CHAR, EQUAL_LEVELS,
		       GENERATE_FROM_CLASSES, CLEAR, SAVE, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return pl_chars;

		case ADD_CHAR:
			pl_chars = option_add_character(pl_chars, pc);
			break;

		case REMOVE_CHAR:
			ep_option_remove_character(pl_chars, parties);
			break;

		case LIST_CHARS:
			option_list_characters(pl_chars);
			break;

		case EDIT_CHAR:
			option_edit_character(pl_chars, pc, orb_list, bag_list);
			break;

		case EQUAL_LEVELS:
			option_equal_levels(pl_chars);
			break;

		case GENERATE_FROM_CLASSES:
			pl_chars = option_generate_characters_from_classes(pl_chars, pc);
			break;

		case CLEAR:
			pl_chars = option_clear_character_list(pl_chars, parties);
			break;

		case SAVE:
			option_save_pch_list(pl_chars, pc);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Destroys empty list
		if (pl_chars != NULL && pch_list_total(pl_chars) == 0) {
			pl_chars = pch_list_destroy(pl_chars);  // pl_chars becomes NULL
		}
	}
}

/* Player_classes object must come checked!! */
static int choose_character_class(Player_character pch, Player_classes pc)
{
	struct player_class *pclass = ep_choose_class_from_list_obj(pc, "assign");
	if (pclass == NULL) {
		return -1;
	}

	return player_character_set_class(pch, pclass);
}

static int enter_character_name(Player_character pch)
{
	char name[CHAR_NAME_LEN+1];

	printf("Enter name: ");
	read_line(name, CHAR_NAME_LEN);

	return player_character_set_name(pch, name);
}

static Pch_list option_add_character(Pch_list pl_chars, Player_classes pc)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return pl_chars;
	}
	if (player_classes_total(pc) <= 0) {
		printf("Player classes list empty\n");
		return pl_chars;
	}

	Player_character new_char;

	if (pl_chars == NULL) {
		pl_chars = pch_list_create();
		if (pl_chars == NULL) {
			return NULL;
		}
	}

	new_char = player_character_create();
	if (new_char == NULL) {
		return pl_chars;
	}

	if (choose_character_class(new_char, pc) < 0) {
		free(new_char);
		return pl_chars;
	}

	if (ep_enter_character_level(new_char) < 0) {
		free(new_char);
		return pl_chars;
	}

	enter_character_name(new_char);

	pch_list_add(pl_chars, new_char);

	return pl_chars;
}

static void option_list_characters(Pch_list pl_chars)
{
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return;
	}

	printf("Show details? (y)\n:");
	ep_show_character_list(pl_chars, accept_prompt('y'));
}

static void option_edit_character(Pch_list pl_chars, Player_classes pc,
                                  Orb_list orb_list, Bag_list bag_list)
{
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return;
	}
	if (pch_list_total(pl_chars) < 1) {
		printf("Player character list is empty\n");
		return;
	}

	int option;
	Player_character editing_pch;

	editing_pch = ep_choose_character_from_list_obj(pl_chars, "edit");
	if (editing_pch == NULL) {
		return;
	}

	for (;;) {
		enum {BACK = 0, CHANGE_LEVEL, CHANGE_CLASS, SET_NAME, SET_ORB, SET_BAG,
			            VIEW_INFO};

		printf("\nEditing character %s\n",
		       player_character_get_name(editing_pch));
		printf("(%d-Change level, %d-Change class, %d-Set name\n"
		       " %d-Set orb, %d-Set bag, %d-View info, %d-Back)\n:",
		       CHANGE_LEVEL, CHANGE_CLASS, SET_NAME, SET_ORB, SET_BAG,
		       VIEW_INFO, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		if (option == BACK) {
			break;
		}

		switch (option) {
		case CHANGE_LEVEL:
			printf("Current level: %d\n",
			       player_character_get_level(editing_pch));
			ep_enter_character_level(editing_pch);
			break;

		case CHANGE_CLASS:
			printf("Current class:\n");
			ep_show_class_info(*player_character_get_class(editing_pch), true);  // Show details?
			if (pc == NULL) {
				printf("No player classes defined yet!\n");
				break;
			}
			if (player_classes_total(pc) < 1) {
				printf("Player classes list is empty\n");
				break;
			}
			choose_character_class(editing_pch, pc);
			break;

		case SET_NAME:
			enter_character_name(editing_pch);
			break;

		case SET_ORB:
			ep_choose_character_orb(editing_pch, orb_list);
			break;

		case SET_BAG:
			ep_choose_character_bag(editing_pch, bag_list);
			break;

		case VIEW_INFO:
			ep_show_character_info(editing_pch, true, true);
			printf("Class info:\n");
			ep_show_class_info(*player_character_get_class(editing_pch), true);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}

static void option_equal_levels(Pch_list pl_chars)
{
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return;
	}

	Player_character pch;
	int level;
	int num_chars = pch_list_total(pl_chars);

	if (pch_list_total(pl_chars) < 1) {
		printf("Player characters list is empty\n");
		return;
	}

	printf("Enter new level for all: ");
	level = flushed_scanf_d(FSD_ERR_VAL);
	if (level < 1) {
		printf("Level must be positive\n");
		return;
	}

	for (int i = 0; i < num_chars; ++i) {
		pch = pch_list_get_pch(pl_chars, i);
		player_character_set_level(pch, level);
	}
}

static Pch_list option_generate_characters_from_classes(Pch_list pl_chars,
                                                        Player_classes pc)
{
	if (pc == NULL) {
		printf("Player classes not defined yet!\n");
		return pl_chars;
	}

	char new_char_name[CHAR_NAME_LEN+1];
	Player_character pch;
	int level;
	int num_pclasses = player_classes_total(pc);

	if (num_pclasses < 1) {
		printf("Player classes list is empty\n");
		return pl_chars;
	}

	printf("Enter level that will be given to generated characters: ");
	level = flushed_scanf_d(FSD_ERR_VAL);
	if (level < 1) {
		printf("Level must be positive\n");
		return pl_chars;
	}

	if (pl_chars == NULL) {
		pl_chars = pch_list_create();
		if (pl_chars == NULL) {
			return NULL;
		}
	}

	for (int i = 0; i < num_pclasses; ++i) {
		pch = player_character_create();
		if (pch == NULL) {
			fprintf(stderr, "%s: Error creating character %d\n", __func__, i);
			return pl_chars;
		}
		player_character_set_level(pch, level);
		player_character_set_class(pch, player_classes_get_class(pc, i));
		get_new_char_name(new_char_name, CHAR_NAME_LEN, pc, i);
		player_character_set_name(pch, new_char_name);
		pch_list_add(pl_chars, pch);
	}

	return pl_chars;
}

static void get_new_char_name(char *name_a, size_t name_len_max,
                              Player_classes pc, int pclass_pos)
{
	static const char *suffix = "_ch";  // This must be shorter than CHAR_NAME_LEN
	const char *pclass_name;
	size_t pclass_name_len;
	size_t suffix_len = strlen(suffix);

	if (suffix_len >= name_len_max) {
		fprintf(stderr, "%s: suffix is longer than allowed string length,\n"
		                "all names will be empty\n", __func__);
		name_a[0] = '\0';
	}

	pclass_name = player_classes_get_class(pc, pclass_pos)->name;
	pclass_name_len = strlen(pclass_name);

	if (pclass_name_len > name_len_max - suffix_len) {
		strncpy(name_a, pclass_name, name_len_max - suffix_len - 1);
		name_a[name_len_max - suffix_len - 1] = '\0';
		strncat(name_a, "~", 1);
		strncat(name_a, suffix, suffix_len);
	}
	else {
		strncpy(name_a, pclass_name, pclass_name_len);
		name_a[pclass_name_len] = '\0';
		strncat(name_a, suffix, suffix_len);
	}
}

static Pch_list option_clear_character_list(Pch_list pl_chars,
                                            Party_list pl_parties)
{
	if (pl_parties != NULL) {
		party_list_update_removed_chars(pl_parties, NULL, 0);
	}

	return pch_list_destroy(pl_chars);  // Returns NULL
}

static void option_save_pch_list(Pch_list pl_chars, Player_classes pc)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}
	if (player_classes_total(pc) < 1) {
		printf("Player classes list is empty\n");
		return;
	}
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return;
	}
	if (pch_list_total(pl_chars) < 1) {
		printf("Player characters list is empty\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Save player characters file\nEnter file name: ");
	read_line(filename, FILENAME_MAX);

	if (filename[0] == '\0') {
		printf("File name can't be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_classes_chars_file(filename, pc, pl_chars);
	}
	else {
		io_save_classes_chars_file(filename, pc, pl_chars);
	}
}