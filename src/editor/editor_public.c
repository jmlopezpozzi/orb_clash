/* editor_public.c */

#include "editor_public.h"
#include "../adt/player_classes_adt.h"
#include "../adt/player_character_adt.h"
#include "../adt/pch_list_adt.h"
#include "../adt/player_party_adt.h"
#include "../adt/party_list_adt.h"
#include "../adt/spell_adt.h"
#include "../adt/spell_list_adt.h"
#include "../adt/orb_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/g_item_adt.h"
#include "../adt/item_list_adt.h"
#include "../adt/bag_adt.h"
#include "../adt/bag_list_adt.h"
#include "../player_class.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum obj_from_list_modes {CLASS_FROM_LIST, PCH_FROM_LIST, SPELL_FROM_LIST,
                          ORB_FROM_LIST, ITEM_FROM_LIST, BAG_FROM_LIST,
                          SPELL_FROM_ORB, ITEM_FROM_BAG, PARTY_FROM_LIST,
                          NUM_EP_OFL_MODES};

// Information used by the functions operating on objects from lists
static const struct {

	char *obj_type;
	void (*show_func)(void *, bool);
	int (*qty_func)(void *);
	void *(*getobj_func)(void *, int);
	void (*unref_func)(void *, void **, int);
	int (*remove_func)(void *, int);
}
ofl_info[NUM_EP_OFL_MODES] = {

	[CLASS_FROM_LIST] = {
		.obj_type  = "class",
		.show_func = (void (*)(void *, bool)) ep_show_class_list,
		.qty_func  = (int (*)(void *)) player_classes_total,
		.getobj_func = (void *(*)(void *, int)) player_classes_get_class,
		.unref_func  = (void (*)(void *, void **, int)) pch_list_update_removed_classes,
		.remove_func = (int (*)(void *, int)) player_classes_remove,
	},

	[PCH_FROM_LIST] = {
		.obj_type  = "character",
		.show_func = (void (*)(void *, bool)) ep_show_character_list,
		.qty_func  = (int (*)(void *)) pch_list_total,
		.getobj_func = (void *(*)(void *, int)) pch_list_get_pch,
		.unref_func  = (void (*)(void *, void **, int)) party_list_update_removed_chars,
		.remove_func = (int (*)(void *, int)) pch_list_remove,
	},

	[SPELL_FROM_LIST] = {
		.obj_type  = "spell",
		.show_func = (void (*)(void *, bool)) ep_show_spell_list,
		.qty_func  = (int (*)(void *)) spell_list_total,
		.getobj_func = (void *(*)(void *, int)) spell_list_get_spell,
		.unref_func  = (void (*)(void *, void **, int)) orb_list_update_removed_spells,
		.remove_func = (int (*)(void *, int)) spell_list_remove,
	},

	[ORB_FROM_LIST] = {
		.obj_type  = "orb",
		.show_func = (void (*)(void *, bool)) ep_show_orb_list,
		.qty_func  = (int (*)(void *)) orb_list_total,
		.getobj_func = (void *(*)(void *, int)) orb_list_get_orb,
		.unref_func  = (void (*)(void *, void **, int)) pch_list_update_removed_orbs,
		.remove_func = (int (*)(void *, int)) orb_list_remove,
	},

	[ITEM_FROM_LIST] = {
		.obj_type  = "item",
		.show_func = (void (*)(void *, bool)) ep_show_item_list,
		.qty_func  = (int (*)(void *)) item_list_total,
		.getobj_func = (void *(*)(void *, int)) item_list_get_item,
		.unref_func  = (void (*)(void *, void **, int)) bag_list_update_removed_items,
		.remove_func = (int (*)(void *, int)) item_list_remove,
	},

	[BAG_FROM_LIST] = {
		.obj_type  = "bag",
		.show_func = (void (*)(void *, bool)) ep_show_bag_list,
		.qty_func  = (int (*)(void *)) bag_list_total,
		.getobj_func = (void *(*)(void *, int)) bag_list_get_bag,
		.unref_func  = (void (*)(void *, void **, int)) pch_list_update_removed_bags,
		.remove_func = (int (*)(void *, int)) bag_list_remove,
	},

	[SPELL_FROM_ORB] = {
		.obj_type  = "spell",
		.show_func = (void (*)(void *, bool)) ep_show_orb_contents,
		.qty_func  = (int (*)(void *)) orb_get_num_spells,
		.getobj_func = (void *(*)(void *, int)) orb_get_spell,
		.unref_func  = NULL,  // No use
		.remove_func = (int (*)(void *, int)) orb_remove_spell,
	},

	[ITEM_FROM_BAG] = {
		.obj_type  = "item",
		.show_func = (void (*)(void *, bool)) ep_show_bag_contents,
		.qty_func  = (int (*)(void *)) bag_get_num_items,
		.getobj_func = (void *(*)(void *, int)) bag_get_item,
		.unref_func  = NULL,  // No use
		.remove_func = (int (*)(void *, int)) bag_remove_item,
	},

	[PARTY_FROM_LIST] = {
		.obj_type  = "party",
		.show_func = (void (*)(void *, bool)) ep_show_party_list,
		.qty_func  = (int (*)(void *)) party_list_total,
		.getobj_func = (void *(*)(void *, int)) party_list_get_party,
		.unref_func  = NULL,  // No use
		.remove_func = (int (*)(void *, int)) party_list_remove,
	},
};
//char *(*getname_func)(void *, int);  // Possible struct member

// Information provided to the option_load_general function by its clients
struct load_menu_info {
	char *types_name;
	int (*load_text)(const char *, void **, void **);
	int (*load_binary)(const char *, void **, void **);
	void (*unref_func)(void *, void **, int);
	void *(*destroy_primary)(void *);
	void *(*destroy_secondary)(void *);
};


static int choose_from_list_idx(enum obj_from_list_modes mode, void *obj_list,
                                char *operation);
static void *choose_from_list_obj(enum obj_from_list_modes mode, void *obj_list,
                                  char *operation);
static void ep_option_remove(enum obj_from_list_modes mode, void *obj_list,
                             void *referenced_list);
static void option_load_general(const struct load_menu_info *info,
                                void **primary_list_ptr,
                                void **secondary_list_ptr, void *ref_list);


void ep_show_class_list(Player_classes pc, bool show_count)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}

	int num_pclasses;
	const struct player_class *current_pclass = NULL;

	num_pclasses = player_classes_total(pc);
	if (num_pclasses < 1) {
		printf("Player classes list empty\n");
		return;
	}

	if (show_count == true) {
		printf("There are %d classes\n", num_pclasses);
	}
	printf("pos    "CLASSES_HEADER_STRING);

	for (int i = 0; i < num_pclasses; ++i) {
		current_pclass = player_classes_get_class(pc, i);
		printf("%-6d ", i + 1);
		ep_show_class_info(*current_pclass, false);
	}
}

void ep_show_class_info(struct player_class pclass, bool show_header)
{
	if (show_header == true) {
		printf(CLASSES_HEADER_STRING);
	}

	printf("%-6.1f %-6.1f %-6.1f %-6.1f %-6.1f %-6.1f %-6.1f %4d %s\n",
	       pclass.atk, pclass.spd, pclass.itl,
	       pclass.mpt, pclass.def, pclass.hpt,
	       pclass.hpt_floor, pclass.type, pclass.name);
}

void ep_show_character_list(Pch_list pl_chars, bool show_details)
{
	if (pl_chars == NULL) {
		printf("No player character list defined yet!\n");
		return;
	}

	int num_pch;
	Player_character pch;

	num_pch = pch_list_total(pl_chars);
	if (num_pch < 1) {
		printf("Player character list is empty\n");
		return;
	}

	printf("Pos Character                Class\n");

	for (int pos = 0; pos < num_pch; ++pos) {
		pch = pch_list_get_pch(pl_chars, pos);
		printf("%-3d ", pos + 1);
		ep_show_character_info(pch, show_details, false);
	}
}

void ep_show_character_info(Player_character pch, bool show_details,
                            bool show_header)
{
	if (pch == NULL) {
		return;
	}

	if (show_header == true) {
		printf("Character                Class\n");
	}

	printf("%-16s Lvl %-3d %-16s\n",
	       player_character_get_name(pch), player_character_get_level(pch),
	       player_character_get_class(pch)->name);

	if (show_details == true) {
		const int *stats_base = player_character_get_stats_base(pch);
		Orb orb = player_character_get_orb(pch);
		Bag bag = player_character_get_bag(pch);

		printf("       ATK    SPD    ITL    MPT    DEF    HPT\n");
		printf("      ");
		for (int i = 0; i < NUM_STATS_BASE; ++i) {
			printf(" %-6d", stats_base[i]);
		}
		printf("\n       Orb:   %-16s\n       Bag:   %-16s\n",
		       orb == NULL ? " (none)" : orb_get_name(orb),
		       bag == NULL ? " (none)" : bag_get_name(bag));
	}
}

void ep_show_party_list(Party_list pl_parties, bool show_details)
{
	if (pl_parties == NULL) {
		printf("No party list defined yet!\n");
		return;
	}

	int num_parties;
	Player_party party;

	num_parties = party_list_total(pl_parties);
	if (num_parties < 1) {
		printf("Party list is empty\n");
		return;
	}

	printf("Pos Party\n");

	for (int i = 0; i < num_parties; ++i) {
		party = party_list_get_party(pl_parties, i);
		printf("%-3d %s\n", i + 1, player_party_get_name(party));

		if (show_details == true) {
			Player_character pch;

			if (player_party_get_num_chars(party) < 1) {
				printf("     Empty\n");
				continue;
			}

			for (int j = 0; j < PARTY_NUM_POSITIONS; ++j) {
				pch = player_party_get_char(party, j);
				if (pch != NULL) {
					printf("     ");
					ep_show_character_info(pch, false, false);
				}
			}
		}
	}
}

void ep_show_party_info(Player_party pl_party, bool show_details)
{
	if (pl_party == NULL) {
		printf("No player party defined yet!\n");
		return;
	}

	Player_character pch;

	printf("Pos Character\n");

	for (int i = 0; i < PARTY_NUM_POSITIONS; ++i) {
		pch = player_party_get_char(pl_party, i);

		if (pch == NULL) {
			printf("%-3d  EMPTY\n", i + 1);
			continue;
		}

		printf("%-3d ", i + 1);
		ep_show_character_info(pch, show_details, false);
	}
}

void ep_show_spell_list(Spell_list spell_list, bool show_description)
{
	if (spell_list == NULL) {
		printf("No spell list defined yet!\n");
		return;
	}

	int num_spell;
	Spell spell;

	num_spell = spell_list_total(spell_list);
	if (num_spell < 1) {
		printf("Spell list is empty\n");
		return;
	}

	printf("Pos Spell name        Cost\n");

	for (int i = 0; i < num_spell; ++i) {
		spell = spell_list_get_spell(spell_list, i);
		printf("%-3d ", i + 1);
		ep_show_spell_info(spell, show_description, false);
	}
}

void ep_show_spell_info(Spell spell, bool show_description, bool show_header)
{
	if (spell == NULL) {
		return;
	}

	if (show_header == true) {
		printf("Spell name      Cost\n");
	}

	printf("%-16s  %d\n", spell_get_name(spell), spell_get_cost(spell));

	if (show_description == true) {
		printf("     %s\n", spell_get_description(spell));
	}
}

void ep_show_orb_list(Orb_list orb_list, bool show_contents)
{
	if (orb_list == NULL) {
		printf("No orb list defined yet!\n");
		return;
	}

	int num_orb;
	Orb orb;

	num_orb = orb_list_total(orb_list);
	if (num_orb < 1) {
		printf("Orb list is empty\n");
		return;
	}

	printf("Pos  Orb\n");

	for (int i = 0; i < num_orb; ++i) {
		orb = orb_list_get_orb(orb_list, i);
		printf("%-4d %s\n", i + 1, orb_get_name(orb));

		if (show_contents == true) {
			int num_spell;
			Spell spell;

			num_spell = orb_get_num_spells(orb);
			for (int j = 0; j < num_spell; ++j) {
				spell = orb_get_spell(orb, j);
				printf("        %s\n", spell_get_name(spell));
			}
		}
	}
}

void ep_show_orb_contents(Orb orb, bool show_description)
{
	if (orb == NULL) {
		return;
	}

	int num_spell;
	Spell spell;

	num_spell = orb_get_num_spells(orb);
	if (num_spell < 1) {
		printf("Orb is empty\n");
		return;
	}

	printf("Pos  Size  Spell\n");

	for (int i = 0; i < num_spell; ++i) {
		spell = orb_get_spell(orb, i);
		printf("%-4d %-4d %s\n", i + 1, spell_get_cost(spell),
		                         spell_get_name(spell));

		if (show_description == true) {
			printf("        %s\n", spell_get_description(spell));
		}
	}
}

void ep_show_bag_list(Bag_list bag_list, bool show_contents)
{
	if (bag_list == NULL) {
		printf("No bag list defined yet!\n");
		return;
	}

	int num_bags;
	Bag bag;

	num_bags = bag_list_total(bag_list);
	if (num_bags < 1) {
		printf("Bag list is empty\n");
		return;
	}

	printf("Pos  Bag             Size  Used\n");

	for (int i = 0; i < num_bags; ++i) {
		bag = bag_list_get_bag(bag_list, i);
		printf("%-4d %-16s%-4d  %-4d\n", i + 1,
		       bag_get_name(bag), bag_get_size(bag), bag_get_used_space(bag));

		if (show_contents == true) {
			int num_items;
			Item item;

			num_items = bag_get_num_items(bag);
			for (int j = 0; j < num_items; ++j) {
				item = bag_get_item(bag, j);
				printf("        %s (%d)\n", item_get_name(item),
				       item_get_size(item));
			}
		}
	}
}

void ep_show_bag_contents(Bag bag, bool show_description)
{
	if (bag == NULL) {
		return;
	}

	int num_items;
	Item item;

	num_items = bag_get_num_items(bag);
	if (num_items < 1) {
		printf("Bag is empty\n");
		return;
	}

	printf("Pos  Size  Item\n");

	for (int i = 0; i < num_items; ++i) {
		item = bag_get_item(bag, i);
		printf("%-4d %-4d  %s\n", i + 1, item_get_size(item),
		                          item_get_name(item));
		if (show_description == true) {
			printf("        %s\n", item_get_description(item));
		}
	}
}

void ep_show_item_list(Item_list item_list, bool show_description)
{
	if (item_list == NULL) {
		printf("No item list defined yet!\n");
		return;
	}

	int num_items;
	Item item;

	num_items = item_list_total(item_list);
	if (num_items <= 0) {
		printf("Item list is empty\n");
		return;
	}

	printf("Pos Item name       Size\n");

	for (int pos = 0; pos < num_items; ++pos) {
		item = item_list_get_item(item_list, pos);
		printf("%-3d ", pos + 1);
		ep_show_item_info(item, show_description, false);
	}
}

void ep_show_item_info(Item item, bool show_description, bool show_header)
{
	if (item == NULL) {
		return;
	}

	if (show_header == true) {
		printf("Item name       Size\n");
	}

	printf("%-16s  %d\n", item_get_name(item), item_get_size(item));
	if (show_description == true) {
		printf("     %s\n", item_get_description(item));
	}
}


struct player_class *ep_choose_class_from_list_obj(Player_classes pc,
                                                   char *operation)
{
	return choose_from_list_obj(CLASS_FROM_LIST, pc, operation);
}

int ep_choose_class_from_list_idx(Player_classes pc, char *operation)
{
	return choose_from_list_idx(CLASS_FROM_LIST, pc, operation);
}


Player_character ep_choose_character_from_list_obj(Pch_list pl_chars,
                                                   char *operation)
{
	return choose_from_list_obj(PCH_FROM_LIST, pl_chars, operation);
}

int ep_choose_character_from_list_idx(Pch_list pl_chars,char *operation)
{
	return choose_from_list_idx(PCH_FROM_LIST, pl_chars, operation);
}


Orb ep_choose_orb_from_list_obj(Orb_list orb_list, char *operation)
{
	return choose_from_list_obj(ORB_FROM_LIST, orb_list, operation);
}

int ep_choose_orb_from_list_idx(Orb_list orb_list, char *operation)
{
	return choose_from_list_idx(ORB_FROM_LIST, orb_list, operation);
}


Spell ep_choose_spell_from_orb_obj(Orb orb, char *operation)
{
	return choose_from_list_obj(SPELL_FROM_LIST, orb, operation);
}

int ep_choose_spell_from_orb_idx(Orb orb, char *operation)
{
	return choose_from_list_idx(SPELL_FROM_LIST, orb, operation);
}


Bag ep_choose_bag_from_list_obj(Bag_list bag_list, char *operation)
{
	return choose_from_list_obj(BAG_FROM_LIST, bag_list, operation);
}

int ep_choose_bag_from_list_idx(Bag_list bag_list, char *operation)
{
	return choose_from_list_idx(BAG_FROM_LIST, bag_list, operation);
}


Item ep_choose_item_from_bag_obj(Bag bag, char *operation)
{
	return choose_from_list_obj(ITEM_FROM_BAG, bag, operation);
}

int ep_choose_item_from_bag_idx(Bag bag, char *operation)
{
	return choose_from_list_idx(ITEM_FROM_BAG, bag, operation);
}


Spell ep_choose_spell_from_list_obj(Spell_list spell_list, char *operation)
{
	return choose_from_list_obj(SPELL_FROM_LIST, spell_list, operation);
}

int ep_choose_spell_from_list_idx(Spell_list spell_list, char *operation)
{
	return choose_from_list_idx(SPELL_FROM_LIST, spell_list, operation);
}


Item ep_choose_item_from_list_obj(Item_list item_list, char *operation)
{
	return choose_from_list_obj(ITEM_FROM_LIST, item_list, operation);
}

int ep_choose_item_from_list_idx(Item_list item_list, char *operation)
{
	return choose_from_list_idx(ITEM_FROM_LIST, item_list, operation);
}


Player_party ep_choose_party_from_list_obj(Party_list parties, char *operation)
{
	return choose_from_list_obj(PARTY_FROM_LIST, parties, operation);
}

int ep_choose_party_from_list_idx(Party_list parties, char *operation)
{
	return choose_from_list_idx(PARTY_FROM_LIST, parties, operation);
}


static int choose_from_list_idx(enum obj_from_list_modes mode, void *obj_list,
                                char *operation)
{
	if (mode < 0 || mode >= NUM_EP_OFL_MODES) {
		fprintf(stderr, "%s: Mode %d not recognised\n", __func__, mode);
		return -1;
	}

	int choice;

	ofl_info[mode].show_func(obj_list, false);
	printf("Choose %s to %s: ", ofl_info[mode].obj_type, operation);
	choice = flushed_scanf_d(FSD_ERR_VAL);

	if (choice < 1 || choice > ofl_info[mode].qty_func(obj_list)) {
		printf("No %s in position %d\n", ofl_info[mode].obj_type, choice);
		return -2;
	}

	return --choice;
}

static void *choose_from_list_obj(enum obj_from_list_modes mode, void *obj_list,
                                  char *operation)
{
	int choice = choose_from_list_idx(mode, obj_list, operation);

	if (choice < 0) {
		return NULL;
	}

	return ofl_info[mode].getobj_func(obj_list, choice);
}


static void ep_option_remove(enum obj_from_list_modes mode, void *obj_list,
                             void *referenced_list)
{
	if (mode < 0 || mode >= NUM_EP_OFL_MODES) {
		fprintf(stderr, "%s: Mode %d not recognised\n", __func__, mode);
		return;
	}

	int pos;
	int *idx_removed_objs;  // Dynamic array
	int qty_removed = 0;
	const int num_objs = ofl_info[mode].qty_func(obj_list);

	idx_removed_objs = malloc(sizeof(*idx_removed_objs) * num_objs);
	if (idx_removed_objs == NULL) {
		fprintf(stderr, "%s: Could not create idx_removed_objs array\n",
		                __func__);
		return;
	}

	ofl_info[mode].show_func(obj_list, false);

	for (;;) {
	loop_top:

		printf("Enter %s to remove: ", ofl_info[mode].obj_type);
		pos = flushed_scanf_d(FSD_ERR_VAL);
		if (pos <= 0 || pos > num_objs) {
			printf("There is no %s in position %d\n", ofl_info[mode].obj_type,
			                                          pos);
			break;
		}
		--pos;

		for (int i = 0; i < qty_removed; ++i) {
			if (pos == idx_removed_objs[i]) {
				printf("%s %d already removed\n", ofl_info[mode].obj_type,
				                                  pos + 1);  // Good place to use a getname_func
				goto loop_top;
			}
		}

		idx_removed_objs[qty_removed++] = pos;
	}

	if (referenced_list != NULL && qty_removed > 0) {

		if (qty_removed == num_objs){
			ofl_info[mode].unref_func(referenced_list, NULL, 0);
		}
		else {
			void **removed_objs;  // Dynamic array

			removed_objs = malloc(sizeof(*removed_objs) * qty_removed);
			if (removed_objs == NULL) {
				fprintf(stderr, "%s: Could not create removed_objs array\n",
				                __func__);
				free(idx_removed_objs);
				return;
			}

			for (int i = 0; i < qty_removed; ++i) {
				removed_objs[i] = ofl_info[mode].getobj_func(obj_list,
				                                           idx_removed_objs[i]);
			}

			ofl_info[mode].unref_func(referenced_list, removed_objs,
			                          qty_removed);

			free(removed_objs);
		}
	}

	qsort(idx_removed_objs, qty_removed, sizeof(int), compare_ints);
	for (int i = 0; i < qty_removed; ++i) {
		ofl_info[mode].remove_func(obj_list, idx_removed_objs[i] - i);
	}

	free(idx_removed_objs);
}

void ep_option_remove_class(Player_classes pc, Pch_list pl_chars)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}

	ep_option_remove(CLASS_FROM_LIST, pc, pl_chars);
}

void ep_option_remove_character(Pch_list pl_chars, Party_list pl_parties)
{
	if (pl_chars == NULL) {
		printf("No player characters defined yet!\n");
		return;
	}

	ep_option_remove(PCH_FROM_LIST, pl_chars, pl_parties);
}

void ep_option_remove_spell(Spell_list spell_list, Orb_list orb_list)
{
	if (spell_list == NULL) {
		printf("Spell list not defined yet!\n");
		return;
	}

	ep_option_remove(SPELL_FROM_LIST, spell_list, orb_list);
}

void ep_option_remove_item(Item_list item_list, Bag_list bag_list)
{
	if (item_list == NULL) {
		printf("Item list not defined yet!\n");
		return;
	}

	ep_option_remove(ITEM_FROM_LIST, item_list, bag_list);
}

void ep_option_remove_orb(Orb_list orb_list, Pch_list pl_chars)
{
	if (orb_list == NULL) {
		printf("No orbs defined yet!\n");
		return;
	}

	ep_option_remove(ORB_FROM_LIST, orb_list, pl_chars);
}

void ep_option_remove_bag(Bag_list bag_list, Pch_list pl_chars)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return;
	}

	ep_option_remove(BAG_FROM_LIST, bag_list, pl_chars);
}

void ep_option_remove_spell_from_orb(Orb orb)
{
	if (orb == NULL) {
		printf("Orb is NULL\n");
		return;
	}

	ep_option_remove(SPELL_FROM_ORB, orb, NULL);
}

void ep_option_remove_item_from_bag(Bag bag)
{
	if (bag == NULL) {
		printf("Bag is NULL\n");
		return;
	}

	ep_option_remove(ITEM_FROM_BAG, bag, NULL);
}


/* Functions shared by the character list editor and the player party editor */
int ep_enter_character_level(Player_character pch)
{
	int level;

	printf("Set level: ");
	level = flushed_scanf_d(FSD_ERR_VAL);
	if (level <= 0) {
		printf("Level must be positive\n");
		return -1;
	}

	return player_character_set_level(pch, level);
}

int ep_choose_character_orb(Player_character pch, Orb_list orb_list)
{
	if (orb_list == NULL) {
		printf("No orbs defined yet!\n");
		return -1;
	}
	if (orb_list_total(orb_list) < 1) {
		printf("Orb list is empty\n");
		return -2;
	}

	Orb orb = ep_choose_orb_from_list_obj(orb_list, "equip");

	return player_character_set_orb(pch, orb);
}

int ep_choose_character_bag(Player_character pch, Bag_list bag_list)
{
	if (bag_list == NULL) {
		printf("No bags defined yet!\n");
		return -1;
	}
	if (bag_list_total(bag_list) < 1) {
		printf("Bag list is empty\n");
		return -2;
	}

	Bag bag = ep_choose_bag_from_list_obj(bag_list, "equip");

	return player_character_set_bag(pch, bag);
}

/* May leave orb_list empty, will not destroy it */
int ep_remove_empty_orbs(Orb_list orb_list, Pch_list pl_chars)
{
	if (orb_list == NULL) {
		return -1;
	}

	int num_orbs;
	Orb *empty_orbs;  // Dynamic array
	Orb cur_orb;
	int qty_empty_orbs = 0;

	num_orbs = orb_list_total(orb_list);
	if (num_orbs <= 0) {
		return 0;
	}

	empty_orbs = malloc(sizeof(*empty_orbs) * num_orbs);
	if (empty_orbs == NULL) {
		fprintf(stderr, "%s: Could not create empty_orbs array\n", __func__);
		return -2;
	}

	for (int i = 0; i < num_orbs; ++i) {
		cur_orb = orb_list_get_orb(orb_list, i);

		if (orb_get_num_spells(cur_orb) <= 0) {
			empty_orbs[qty_empty_orbs++] = cur_orb;
		}
	}

	if (qty_empty_orbs == num_orbs) {
		printf("All orbs are now empty and removed\n");

		if (pl_chars != NULL) {
			pch_list_update_removed_orbs(pl_chars, NULL, 0);
		}
		free(empty_orbs);

		for (int i = 0; i < num_orbs; ++i) {
			orb_list_remove(orb_list, 0);
		}

		return num_orbs;
	}
	else {
		int orb_pos;

		if (pl_chars != NULL) {
			pch_list_update_removed_orbs(pl_chars, empty_orbs, qty_empty_orbs);
		}
		for (int i = 0; i < qty_empty_orbs; ++i) {
			printf("Orb %s is now empty and removed\n",
			       orb_get_name(empty_orbs[i]));

			orb_pos = orb_list_find_orb_pos(orb_list, empty_orbs[i]);  // Maybe using a sorted array of indexes is less wasteful...
			orb_list_remove(orb_list, orb_pos);
		}
	}

	free(empty_orbs);

	return qty_empty_orbs;
}


void ep_option_load_spells_orbs_lists(Spell_list *spell_list_ptr,
                                      Orb_list *orb_list_ptr,
                                      Pch_list pl_chars)
{
	const struct load_menu_info info = {

		.types_name  = "spell and orb",
		.load_text   = (int (*)(const char *, void **, void **))
			io_text_load_spells_orbs_file,
		.load_binary = (int (*)(const char *, void **, void **))
			io_load_spells_orbs_file,
		.unref_func  = (void (*)(void *, void **, int))
			pch_list_update_removed_orbs,
		.destroy_primary   = (void *(*)(void *)) spell_list_destroy,
		.destroy_secondary = (void *(*)(void *)) orb_list_destroy,
	};

	option_load_general(&info, (void **)spell_list_ptr, (void **)orb_list_ptr,
	                    pl_chars);

}

void ep_option_load_items_bags_lists(Item_list *item_list_ptr,
                                     Bag_list *bag_list_ptr,
                                     Pch_list pl_chars)
{
	const struct load_menu_info info = {

		.types_name  = "item and bag",
		.load_text   = (int (*)(const char *, void **, void **))
			io_text_load_items_bags_file,
		.load_binary = (int (*)(const char *, void **, void **))
			io_load_items_bags_file,
		.unref_func  = (void (*)(void *, void **, int))
			pch_list_update_removed_bags,
		.destroy_primary   = (void *(*)(void *)) item_list_destroy,
		.destroy_secondary = (void *(*)(void *)) bag_list_destroy,
	};

	option_load_general(&info, (void **)item_list_ptr, (void **)bag_list_ptr,
	                    pl_chars);
}

void ep_option_load_classes_chars_lists(Player_classes *pc_ptr,
                                        Pch_list *pl_chars_ptr,
                                        Party_list pl_parties)
{
	const struct load_menu_info info = {

		.types_name  = "classes and characters",
		.load_text   = (int (*)(const char *, void **, void **))
			io_text_load_classes_chars_file,
		.load_binary = (int (*)(const char *, void **, void **))
			io_load_classes_chars_file,
		.unref_func  = (void (*)(void *, void **, int))
			party_list_update_removed_chars,
		.destroy_primary   = (void *(*)(void *)) player_classes_destroy,
		.destroy_secondary = (void *(*)(void *)) pch_list_destroy,
	};

	option_load_general(&info, (void **)pc_ptr, (void **)pl_chars_ptr,
                        pl_parties);
}

static void option_load_general(const struct load_menu_info *info,
                                void **primary_list_ptr,
                                void **secondary_list_ptr, void *ref_list)
{
	char filename[FILENAME_MAX];

	if (*primary_list_ptr != NULL || *secondary_list_ptr != NULL) {
		printf("Current %s lists will be erased, continue? (y)\n:",
		       info->types_name);
		if (accept_prompt('y') == false) {
			return;
		}

		info->unref_func(ref_list, NULL, 0);
		*primary_list_ptr = info->destroy_primary(*primary_list_ptr);
		*secondary_list_ptr = info->destroy_secondary(*secondary_list_ptr);
	}

	printf("Enter file to load: ");
	read_line(filename, FILENAME_MAX - 1);
	if (filename[0] == '\0') {
		printf("File name cannot be empty\n");
		return;
	}

	printf("Load as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		info->load_text(filename, primary_list_ptr, secondary_list_ptr);
	}
	else {
		info->load_binary(filename, primary_list_ptr, secondary_list_ptr);
	}
}

/* Comparison function to use with qsort */
int compare_ints(const void *a, const void *b)
{
	int ia = *(int *)a;
	int ib = *(int *)b;

	if      (ia < ib) return -1;
	else if (ia > ib) return  1;
	else              return  0;
}