/* orbs_manager.c */

#include "editor_public.h"
#include "../adt/spell_adt.h"
#include "../adt/spell_list_adt.h"
#include "../adt/orb_adt.h"
#include "../adt/orb_list_adt.h"
#include "../adt/pch_list_adt.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static Orb_list option_add_orb(Orb_list orb_list, Spell_list spell_list);
static int add_spells_to_orb(Spell_list spell_list, Orb orb);
static void option_edit_orb(Orb_list orb_list, Spell_list spell_list,
                            Pch_list pl_chars);
static void option_list_orbs(Orb_list orb_list);
static void option_save_orb_list(Orb_list orb_list, Spell_list spell_list);

Orb_list orbs_manager(Spell_list spell_list, Orb_list orb_list,
                      Pch_list pl_chars)
{
	enum {BACK = 0, ADD_ORB, REMOVE_ORB, LIST_ORBS, EDIT_ORB, CLEAR_ORBS,
	                SAVE_ORBS};
	int option;

	for (;;) {
		printf("\nOrbs manager\n");
		printf("(%d-Add orb, %d-Remove orb, %d-List orbs, %d-Edit orb,\n"
		       " %d-Clear orb list, %d-Save orb list, %d-Back)\n:",
		       ADD_ORB, REMOVE_ORB, LIST_ORBS, EDIT_ORB, CLEAR_ORBS, SAVE_ORBS,
		       BACK);

		option = flushed_scanf_d(FSD_ERR_VAL);
		switch (option) {
		case BACK:
			return orb_list;

		case ADD_ORB:
			orb_list = option_add_orb(orb_list, spell_list);
			break;

		case REMOVE_ORB:
			ep_option_remove_orb(orb_list, pl_chars);
			break;

		case LIST_ORBS:
			option_list_orbs(orb_list);
			break;

		case EDIT_ORB:
			option_edit_orb(orb_list, spell_list, pl_chars);
			break;

		case CLEAR_ORBS:
			pch_list_update_removed_orbs(pl_chars, NULL, 0);
			orb_list = orb_list_destroy(orb_list);
			break;

		case SAVE_ORBS:
			option_save_orb_list(orb_list, spell_list);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Destroys empty list
		if (orb_list != NULL && orb_list_total(orb_list) == 0) {
			orb_list = orb_list_destroy(orb_list);  // orb_list becomes NULL
		}
	}
}

static Orb_list option_add_orb(Orb_list orb_list, Spell_list spell_list)
{
	char orb_name[ORB_NAME_LEN+1];
	Orb new_orb;

	if (orb_list == NULL) {
		orb_list = orb_list_create();
		if (orb_list == NULL) {
			printf("Could not create orb list\n");
			return NULL;
		}
	}

	printf("Enter name for orb: ");
	read_line(orb_name, ORB_NAME_LEN);
	if (orb_name[0] == '\0') {
		printf("Orb name cannot be empty\n");
		return orb_list;
	}

	new_orb = orb_create(orb_name);
	if (new_orb == NULL) {
		printf("Could not create orb %s\n", orb_name);
		return orb_list;
	}

	if (add_spells_to_orb(spell_list, new_orb) <= 0) {
		printf("Orb cannot be empty\n");
		orb_destroy(new_orb);
		return orb_list;
	}

	if (orb_list_add(orb_list, new_orb) < 0) {
		printf("Could not add orb %s to list\n", orb_name);
		orb_destroy(new_orb);
	}

	return orb_list;
}

static int add_spells_to_orb(Spell_list spell_list, Orb orb)
{
	if (spell_list == NULL) {
		printf("No spells defined yet!\n");
		return -1;
	}
	if (spell_list_total(spell_list) < 1) {
		printf("Spell list is empty\n");
		return -2;
	}

	int spells_added = 0;
	int choice = 0;

	ep_show_spell_list(spell_list, false);
	for (;;) {
		printf("Choose spell to add: ");
		choice = flushed_scanf_d(FSD_ERR_VAL);

		if (choice > 0 && choice <= spell_list_total(spell_list)) {
			Spell spell = spell_list_get_spell(spell_list, --choice);

			if (orb_add_spell(orb, spell) < 0) {
				printf("Error adding spell to orb\n");
				return -1;
			}
			printf("Added %s to %s\n", spell_get_name(spell),
			                           orb_get_name(orb));
			++spells_added;
		}
		else {
			printf("No spell in position %d\n", choice);
			printf("Add another spell? (y)\n:");
			if (accept_prompt('y') == false) {
				break;
			}
		}
	}

	return spells_added;
}

static void option_edit_orb(Orb_list orb_list, Spell_list spell_list,
                              Pch_list pl_chars)
{
	if (orb_list == NULL) {
		printf("No orbs defined yet!\n");
		return;
	}

	enum {BACK = 0, ADD_SPELL, REMOVE_SPELL, SHOW_CONTENTS, CHANGE_NAME};
	Orb orb;
	int choice;

	orb = ep_choose_orb_from_list_obj(orb_list, "edit");
	if (orb == NULL) {
		return;
	}

	for (;;) {
		printf("\nEditing orb %s\n", orb_get_name(orb));
		printf("(%d-Add spell, %d-Remove Spell, %d-Show contents,\n"
		       " %d-Change name, %d-Back)\n:",
		       ADD_SPELL, REMOVE_SPELL, SHOW_CONTENTS, CHANGE_NAME, BACK);
		choice = flushed_scanf_d(FSD_ERR_VAL);

		switch (choice) {
		case BACK:
			ep_remove_empty_orbs(orb_list, pl_chars);
			return;

		case ADD_SPELL:
			add_spells_to_orb(spell_list, orb);
			break;

		case REMOVE_SPELL:
			ep_option_remove_spell_from_orb(orb);
			break;

		case SHOW_CONTENTS:
			printf("Show descriptions? (y)\n:");
			ep_show_orb_contents(orb, accept_prompt('y'));
			break;

		case CHANGE_NAME:
			{
				char new_name[ORB_NAME_LEN+1];

				printf("Enter new name: ");
				read_line(new_name, ORB_NAME_LEN);
				if (new_name[0] == '\0') {
					printf("Orb name cannot be empty\n");
				}
				else {
					orb_set_name(orb, new_name);
				}
			}
			break;

		default:
			printf("Option %d not recognised\n", choice);
			break;
		}
	}
}

static void option_list_orbs(Orb_list orb_list)
{
	if (orb_list == NULL) {
		printf("No orbs defined yet!\n");
		return;
	}

	printf("Show contents? (y)\n:");
	ep_show_orb_list(orb_list, accept_prompt('y'));
}

static void option_save_orb_list(Orb_list orb_list, Spell_list spell_list)
{
	if (orb_list == NULL) {
		printf("No orbs defined yet!\n");
		return;
	}
	if (orb_list_total(orb_list) < 1) {
		printf("Orb list is empty\n");
		return;
	}
	if (spell_list == NULL) {
		printf("No spells defined yet!\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Enter file name: ");
	read_line(filename, FILENAME_MAX - 1);

	if (filename[0] == '\0') {
		printf("File name cannot be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_spells_orbs_file(filename, spell_list, orb_list);
	}
	else {
		io_save_spells_orbs_file(filename, spell_list, orb_list);
	}
}