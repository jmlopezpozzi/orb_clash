/* items_bags.c */

#include "editor_public.h"
#include "../adt/item_list_adt.h"
#include "../adt/bag_list_adt.h"
#include "../adt/pch_list_adt.h"
#include "../readline.h"
#include <stdio.h>

Item_list item_list_editor(Item_list item_list, Bag_list bag_list);
Bag_list bag_manager(Item_list item_list, Bag_list bag_list, Pch_list pl_chars);

void items_bags(Item_list *item_list_ptr, Bag_list *bag_list_ptr,
                Pch_list pl_chars)
{
	enum {BACK = 0, ITEM_LIST_ED, BAG_MANAGER, LOAD};
	int option;

	for (;;) {
		printf("\nItems and Bags\n");
		printf("(%d-Item list editor, %d-Bag Manager, %d-Load file\n"
		       " %d-Back)\n:",
		       ITEM_LIST_ED, BAG_MANAGER, LOAD, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return;

		case ITEM_LIST_ED:
			*item_list_ptr = item_list_editor(*item_list_ptr, *bag_list_ptr);
			break;

		case BAG_MANAGER:
			*bag_list_ptr = bag_manager(*item_list_ptr, *bag_list_ptr,
			                            pl_chars);
			break;

		case LOAD:
			ep_option_load_items_bags_lists(item_list_ptr, bag_list_ptr,
			                                pl_chars);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}