/* character_class_editor.c */

#include "editor_public.h"
#include "../adt/player_classes_adt.h"
#include "../adt/pch_list_adt.h"
#include "../player_class.h"
#include "../io_files.h"
#include "../io_files_text.h"
#include "../readline.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


static bool enter_full_class_data(struct player_class *pclass);
static Player_classes option_add_class(Player_classes pc);
static void option_edit_class(Player_classes pc, Pch_list pl_chars);
static void option_sort_classes(Player_classes pc);
static Player_classes option_clear_classes(Player_classes pc,
                                           Pch_list pl_chars);
static void option_save_classes(Player_classes pc);

Player_classes character_class_editor(Player_classes pc, Pch_list pl_chars)
{
	enum options {BACK = 0, ADD_CLASS, REMOVE_CLASS, LIST_CLASSES, EDIT_CLASS,
	                        SORT_CLASSES, CLEAR, SAVE_CLASSES};
	int option;

	for (;;) {
		printf("\nCharacter Class Editor\n"
		       "(%d-Add, %d-Remove, %d-List, %d-Edit, %d-Sort, %d-Clear,\n"
		       " %d-Save, %d-Back)\n:",
		       ADD_CLASS, REMOVE_CLASS, LIST_CLASSES, EDIT_CLASS, SORT_CLASSES,
		       CLEAR, SAVE_CLASSES, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			return pc;

		case ADD_CLASS:
			pc = option_add_class(pc);
			break;

		case REMOVE_CLASS:
			ep_option_remove_class(pc, pl_chars);
			break;

		case LIST_CLASSES:
			ep_show_class_list(pc, false);
			break;

		case EDIT_CLASS:
			option_edit_class(pc, pl_chars);
			break;

		case SORT_CLASSES:
			option_sort_classes(pc);
			break;

		case CLEAR:
			pc = option_clear_classes(pc, pl_chars);
			break;

		case SAVE_CLASSES:
			option_save_classes(pc);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}

		// Removes empty list
		if (pc != NULL && player_classes_total(pc) == 0) {
			pc = player_classes_destroy(pc);  // pc becomes NULL
		}
	}
}

static bool enter_full_class_data(struct player_class *pclass)
{
	static const char *statnames[] = {"ATK", "SPD", "ITL", "MPT", "DEF", "HPT"};
	float *stataddr[] = {&pclass->atk, &pclass->spd, &pclass->itl,
	                     &pclass->mpt, &pclass->def, &pclass->hpt};

	for (int i = 0; i < NUM_STATS_BASE; ++i) {
		printf("Enter %s: ", statnames[i]);
		*stataddr[i] = flushed_scanf_f(0.0f);
		if (*stataddr[i] < 0.0f) {
			printf("Value must be positive\n");
			return false;
		}
	}

	printf("Enter HP floor: ");
	if ((pclass->hpt_floor = flushed_scanf_f(0.0f)) < 0.0f) {
		printf("Value must be positive\n");
		return false;
	}
	printf("Enter type: ");
	pclass->type = flushed_scanf_d(0);
	printf("Enter name: ");
	read_line(pclass->name, CLASS_NAME_LEN);

	return true;
}

static Player_classes option_add_class(Player_classes pc)
{
	struct player_class new_pclass = {.name = '\0'};

	printf("New Class\n");
	if (enter_full_class_data(&new_pclass) == false) {
		return pc;
	}

	printf(CLASSES_HEADER_STRING);
	ep_show_class_info(new_pclass, false);
	printf("Is this OK? Enter 'y' to accept: ");
	if (accept_prompt('y') == false) {
		return pc;
	}

	if (pc == NULL) {
		pc = player_classes_create();
		if (pc == NULL) {
			return pc;
		}
	}

	player_classes_add(pc, &new_pclass);

	return pc;
}

static void option_edit_class(Player_classes pc, Pch_list pl_chars)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}
	if (player_classes_total(pc) == 0) {
		printf("Player class list is empty\n");
		return;
	}

	enum {BACK = 0, OP_ATK, OP_SPD, OP_ITL, OP_MPT, OP_DEF, OP_HPT, OP_HFLOOR,
	                OP_TYPE, OP_NAME, OP_ALL};
	int option;
	struct player_class *edited_pclass;

	edited_pclass = ep_choose_class_from_list_obj(pc, "edit");
	if (edited_pclass == NULL) {
		return;
	}

	for (;;) {
		printf("\nCurrent:\n");
		printf(CLASSES_HEADER_STRING);
		ep_show_class_info(*edited_pclass, false);

		printf("(%d-ATK, %d-SPD, %d-ITL, %d-MPT, %d-DEF, %d-HPT,\n"
		       " %d-HP floor, %d-Type, %d-Name, %d-All values, %d-Back)\n",
		       OP_ATK, OP_SPD, OP_ITL, OP_MPT, OP_DEF, OP_HPT, OP_HFLOOR,
		       OP_TYPE, OP_NAME, OP_ALL, BACK);
		printf("Choose value to edit: ");
		option = flushed_scanf_d(FSD_ERR_VAL);

		if (option == BACK) {
			break;
		}

		switch (option) {
		case OP_ATK:
			printf("Enter ATK: ");
			edited_pclass->atk = flushed_scanf_f(0.0f);
			break;

		case OP_SPD:
			printf("Enter SPD: ");
			edited_pclass->spd = flushed_scanf_f(0.0f);
			break;

		case OP_ITL:
			printf("Enter ITL: ");
			edited_pclass->itl = flushed_scanf_f(0.0f);
			break;

		case OP_MPT:
			printf("Enter MPT: ");
			edited_pclass->mpt = flushed_scanf_f(0.0f);
			break;

		case OP_DEF:
			printf("Enter DEF: ");
			edited_pclass->def = flushed_scanf_f(0.0f);
			break;

		case OP_HPT:
			printf("Enter HPT: ");
			edited_pclass->hpt = flushed_scanf_f(0.0f);
			break;

		case OP_HFLOOR:
			printf("Enter HP floor: ");
			edited_pclass->hpt_floor = flushed_scanf_f(0.0f);
			break;

		case OP_TYPE:
			printf("Enter type: ");
			edited_pclass->type = flushed_scanf_d(0);
			break;

		case OP_NAME:
			printf("Enter name: ");
			read_line(edited_pclass->name, CLASS_NAME_LEN);
			break;

		case OP_ALL:
			enter_full_class_data(edited_pclass);
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}

	pch_list_update_stats_base(pl_chars);
}

static void option_sort_classes(Player_classes pc)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}
	if (player_classes_total(pc) == 0) {
		printf("No player classes defined\n");
		return;
	}

	int sort_by;
	int order;

	printf("(%d-ATK, %d-SPD, %d-ITL, %d-MPT, %d-DEF, %d-HPT, %d-Type)\n",
	       BY_ATK + 1, BY_SPD + 1, BY_ITL + 1, BY_MPT + 1,
	       BY_DEF + 1, BY_HPT + 1, BY_TYP + 1);
	printf("Sort by: ");
	sort_by = flushed_scanf_d(FSD_ERR_VAL);
	if (sort_by <= 0 || sort_by > NUM_SORT_BY_OPTIONS) {
		printf("Value %d matches no option\n", sort_by);
		return;
	}

	printf("(%d-Ascending, %d-Descending)\n:", ASCENDING + 1, DESCENDING + 1);
	order = flushed_scanf_d(FSD_ERR_VAL);
	if (order <=0 || order > NUM_SORT_ORDER_OPTIONS) {
		printf("Value %d matches no option\n", order);
		return;
	}

	--sort_by;
	--order;

	player_classes_sort(pc, sort_by, order);
}

static Player_classes option_clear_classes(Player_classes pc, Pch_list pl_chars)
{
	pch_list_update_removed_classes(pl_chars, NULL, 0);

	return player_classes_destroy(pc);  // Returns NULL
}

static void option_save_classes(Player_classes pc)
{
	if (pc == NULL) {
		printf("No player classes defined yet!\n");
		return;
	}
	if (player_classes_total(pc) < 1) {
		printf("Player classes list is empty\n");
		return;
	}

	char filename[FILENAME_MAX];

	printf("Save classes file\nEnter file name: ");
	read_line(filename, FILENAME_MAX);

	if (filename[0] == '\0') {
		printf("File name can't be empty\n");
		return;
	}

	printf("Save as text (t) or binary?\n:");
	if (accept_prompt('t') == true) {
		io_text_save_classes_chars_file(filename, pc, NULL);
	}
	else {
		io_save_classes_chars_file(filename, pc, NULL);
	}
}