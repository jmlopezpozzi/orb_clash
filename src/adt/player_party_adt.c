/* player_party_adt.c */

#include "player_party_adt.h"
#include "player_character_adt.h"
#include "../player_class.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct player_party_adt {
	int num_characters;
	Player_character position_table[PARTY_NUM_POSITIONS];
	char name[PARTY_NAME_LEN+1];
};


struct player_party_adt *player_party_create(void)
{
	struct player_party_adt *party;

	party = calloc(sizeof(*party), 1);
	if (party == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for player party\n",
		                __func__);
		return NULL;
	}

	return party;
}

struct player_party_adt *player_party_destroy(struct player_party_adt *party)
{
	if (party != NULL) {
		free(party);
	}

	return NULL;
}

int player_party_add_char(struct player_party_adt *party,
                          Player_character pch)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return -1;
	}
	if (pch == NULL) {
		return -2;
	}
	if (party->num_characters >= PARTY_NUM_POSITIONS) {
		fprintf(stderr, "%s: Player party is full\n", __func__);
		return -3;
	}

	int char_pos;

	for (char_pos = 0; char_pos <= party->num_characters; ++char_pos) {
		if (party->position_table[char_pos] == NULL) {
			party->position_table[char_pos] = pch;
			break;
		}
	}

	++party->num_characters;
	return char_pos;
}

Player_character player_party_remove_char(struct player_party_adt *party,
                                          int position)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return NULL;
	}
	if (position >= PARTY_NUM_POSITIONS || position < 0) {
		fprintf(stderr, "%s: position out of range (0 - %d)\n", __func__,
		                PARTY_NUM_POSITIONS);
		return NULL;
	}

	if (party->position_table[position] == NULL) {
		return NULL;
	}

	Player_character removed_pch;

	removed_pch = party->position_table[position];
	party->position_table[position] = NULL;
	--party->num_characters;

	return removed_pch;
}

int player_party_swap_position(Player_party party, int pos_a, int pos_b)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return -1;
	}
	if (pos_a >= PARTY_NUM_POSITIONS || pos_a < 0) {
		fprintf(stderr, "%s: pos_a out of range (0 - %d)\n", __func__,
		                PARTY_NUM_POSITIONS);
		return -2;
	}
	if (pos_b >= PARTY_NUM_POSITIONS || pos_b < 0) {
		fprintf(stderr, "%s: pos_b out of range (0 - %d)\n", __func__,
		                PARTY_NUM_POSITIONS);
		return -3;
	}

	Player_character temp;

	temp = party->position_table[pos_b];
	party->position_table[pos_b] = party->position_table[pos_a];
	party->position_table[pos_a] = temp;

	return 0;
}

int player_party_get_num_chars(struct player_party_adt *party)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return -1;
	}

	return party->num_characters;
}

const char *player_party_get_name(struct player_party_adt *party)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return NULL;
	}

	return party->name;
}

Player_character player_party_get_char(struct player_party_adt *party,
                                       int position)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return NULL;
	}
	if (position >= PARTY_NUM_POSITIONS || position < 0) {
		fprintf(stderr, "%s: position out of range (0 - %d)\n", __func__,
		                PARTY_NUM_POSITIONS);
		return NULL;
	}

	if (party->position_table[position] == NULL) {
		return NULL;
	}

	return party->position_table[position];
}

int player_party_set_name(struct player_party_adt *party, const char *name)
{
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return -1;
	}

	if (name == NULL) {
		name = "";
	}

	strncpy(party->name, name, PARTY_NAME_LEN);
	party->name[PARTY_NAME_LEN] = '\0';

	return 0;
}

void player_party_update_removed_characters(struct player_party_adt *party,
                                            Player_character removed[],
                                            int qty_removed)
{
	if (party == NULL) {
		return;
	}

	if (removed == NULL) {
		for (int i = 0; i < PARTY_NUM_POSITIONS; ++i) {
			if (party->position_table[i] != NULL) {
				party->position_table[i] = NULL;
				--party->num_characters;
			}
		}

		return;
	}

	for (int i = 0; i < PARTY_NUM_POSITIONS; ++i) {
		for (int j = 0; j < qty_removed; ++j) {
			if (party->position_table[i] == removed[j]) {
				party->position_table[i] = NULL;
				--party->num_characters;
			}
		}
	}
}