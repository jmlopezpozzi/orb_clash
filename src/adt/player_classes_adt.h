/* player_classes_adt.h */

#ifndef PLAYER_CLASSES_ADT_H
#define PLAYER_CLASSES_ADT_H

#include "../player_class.h"

struct player_classes_adt;
typedef struct player_classes_adt *Player_classes;

enum sort_by_options {BY_ATK, BY_SPD, BY_ITL, BY_MPT, BY_DEF, BY_HPT, BY_TYP,
                      NUM_SORT_BY_OPTIONS};
enum order_options {ASCENDING = 0, DESCENDING = 1, NUM_SORT_ORDER_OPTIONS};


Player_classes player_classes_create(void);
Player_classes player_classes_destroy(Player_classes pc);

int player_classes_add(Player_classes pc,
                       const struct player_class *root_stats);
int player_classes_remove(Player_classes pc, int position);

int player_classes_total(Player_classes pc);
struct player_class *player_classes_get_class(Player_classes pc, int position);
int player_classes_find_class_pos(Player_classes pc,
                                  const struct player_class *pclass);

int player_classes_sort(Player_classes pc, enum sort_by_options sort_by,
                        enum order_options order);

#endif

/**
* Object that keeps a collection of player_class structs. A
* Player_classes object is meant to be a general list of unique
* player classes, removing one from the list erases said player class.
*/


/**
* *  Player_classes player_classes_create(void) *
*
* Creates a Player_classes object.
* Returns: The created Player_classes object or NULL on error.
*/

/**
* * Player_classes player_classes_destroy(Player_classes pc) *
*
* Destroys the Player_classes object given by 'pc' (if it is not NULL).
* Player_class structs referenced in 'pc' are freed.
* Returns: NULL.
*/

/**
* * int player_classes_add(Player_classes pc,
                           const struct player_class *root_stats) *
*
* Makes a copy of the struct pointed to by 'root_stats' and adds it to
* the end of the list represented by the Player_classes object 'pc'.
* Returns: Quantity of player_class structs in 'pc' after the operation,
*  or a negative number on error.
*/

/**
* * int player_classes_remove(Player_classes pc, int pos) *
*
* Removes the player_class struct in the position given by 'position'
* from the list represented by the Player_classes object given by
* 'pl_chars'. The removed struct is freed.
* Returns: Quantity of player_class structs in 'pc' after the operation,
*  or a negative number on error.
*/

/**
* * int player_classes_total(Player_classes pc) *
*
* Returns: Quantity of player_class structs in the Player_classes object
*  given by 'pc' or a negative number on error or if 'pc' is NULL.
*/

/**
* * struct player_class *player_classes_get_class(Player_classes pc,
                                                  int position) *
*
* Returns: The player_class struct found in the position given by
*  'position' of the list represented by the Player_classes object given
*  by 'pc', or NULL on error.
*/

/**
* * int player_classes_find_class_pos(Player_classes pc,
                                      const struct player_class *pclass) *
*
* Returns: The position of the player_class struct 'class' in the list
*  represented by the Player_classes object 'pl_chars'. If 'class' is
*  not in 'pc' or if one or both of them are NULL, a negative number is
*  returned.
*/

/**
* * int player_classes_sort(Player_classes pc,
                            enum sort_by_options sort_by,
                            enum order_options order) *
*
* Sorts the player_class structs in the list represented by the
* Player_classes object 'pc', using the category stated by 'sort_by' and
* in the order stated by 'order'.
* Returns: 0, or a negative number on error.
*/