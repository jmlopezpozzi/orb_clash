/* item_list_adt.h */

#ifndef ITEM_LIST_ADT_H
#define ITEM_LIST_ADT_H

#include "g_item_adt.h"

struct item_list_adt;
typedef struct item_list_adt *Item_list;


Item_list item_list_create(void);
Item_list item_list_destroy(Item_list item_list);

int item_list_add(Item_list item_list, Item item);
int item_list_remove(Item_list item_list, int position);

int item_list_total(Item_list item_list);
Item item_list_get_item(Item_list item_list, int position);
int item_list_find_item_pos(Item_list item_list, Item item);

#endif

/**
* Object that keeps a collection of Item objects. An Item_list object is
* meant to be a general list of unique Item objects, removing one from
* the list erases said Item.
*/


/**
* * Item_list item_list_create(void) *
*
* Creates an Item_list object.
* Returns: The created Item_list object or NULL on error.
*/

/**
* * Item_list item_list_destroy(Item_list item_list) *
*
* Destroys the Item_list object given by 'item_list' (if it is not
* NULL). Item objects contained in 'item_list' are also destroyed.
* Returns: NULL.
*/

/**
* * int item_list_add(Item_list item_list, Item item) *
*
* Adds the Item object given by 'item' to the end of the list
* represented by the Item_list object given by 'item_list'.
* Returns: Quantity of Item objects in 'item_list' after the operation
*  if successful or a negative number on error.
*/

/**
* * int item_list_remove(Item_list item_list, int position) *
*
* Removes the Item object in the position given by 'position' from the
* list represented by the Item_list object given by 'item_list'. The
* removed Item object is destroyed.
* Returns: Quantity of Item objects in 'item_list' after the operation
*  if successful or a negative number on error.
*/

/**
* * int item_list_total(Item_list item_list) *
*
* Returns: Quantity of Item objects in the Item_list given by
*  'item_list' or a negative number if 'item_list' is NULL.
*/

/**
* * Item item_list_get_item(Item_list item_list, int position) *
*
* Returns: The Item object found in the position given by 'position' of
*  the list represented by the Item_list object given by 'item_list', or
*  NULL on error.
*/

/**
* * int item_list_find_item_pos(Item_list item_list, Item item) *
*
* Returns: The position of the Item given by 'item' in the list
*  represented by the Item_list given by 'item_list'. If 'item' is
*  not in 'item_list' or if one or both of them are NULL, a negative
*  number is returned.
*/