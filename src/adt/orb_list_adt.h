/* orb_list_adt.h */

#ifndef ORB_LIST_ADT_H
#define ORB_LIST_ADT_H

#include "spell_adt.h"
#include "spell_list_adt.h"
#include "orb_adt.h"

struct orb_list_adt;
typedef struct orb_list_adt *Orb_list;


Orb_list orb_list_create(void);
Orb_list orb_list_destroy(Orb_list orb_list);

int orb_list_add(Orb_list orb_list, Orb orb);
int orb_list_remove(Orb_list orb_list, int position);

int orb_list_total(Orb_list orb_list);
Orb orb_list_get_orb(Orb_list orb_list, int position);
int orb_list_find_orb_pos(Orb_list orb_list, Orb orb);

void orb_list_update_removed_spells(Orb_list orb_list, Spell removed[],
                                    int qty_removed);

#endif

/**
* Object that keeps a collection of Orbs. An Orb_list object is meant
* to be a general list of unique Orb objects, removing one from the
* list erases said Orb.
*/


/**
* * Orb_list orb_list_create(void) *
*
* Creates a Orb_list object
* Returns: The created Orb_list object or NULL on error.
*/

/**
* * Orb_list orb_list_destroy(Orb_list orb_list) *
*
* Destroys the Orb_list object given by 'orb_list' (if it is not
* NULL). Orb objects contained in 'orb_list' are also destroyed.
* Returns: NULL.
*/

/**
* * int orb_list_add(Orb_list orb_list, Orb orb) *
*
* Adds the Orb object given by 'orb' to the end of the list
* represented by the Orb_list object given by 'orb_list'.
* Returns: Quantity of Orbs in the Orb_list after the operation if
*  successful or a negative number on error.
*/

/**
* * int orb_list_remove(Orb_list orb_list, int position) *
*
* Removes the Orb object in the position given by 'position' from
* the list represented by Orb_list object given by 'orb_list'. The
* removed Orb object is destroyed.
* Returns: Quantity of Orbs in the Orb_list after the operation if
*  successful or a negative number on error.
*/

/**
* * int orb_list_total(Orb_list orb_list) *
*
* Returns: Quantity of Orbs in the Orb_list given by 'orb_list' or
*  a negative number if 'orb_list' is NULL.
*/

/**
* * Orb orb_list_get_orb(Orb_list orb_list, int position) *
*
* Returns: The Orb object found in the position given by 'position' of
*  the list represented by the Orb_list object given by 'orb_list', or
*  NULL on error.
*/

/**
* * int orb_list_find_orb_pos(Orb_list orb_list, Orb orb) *
*
* Returns: The position of the Orb given by 'orb' in the list
*  represented by the Orb_list given by 'orb_list'. If 'orb' is not in
*  'orb_list' or if one or both of them are NULL, a negative number is
*  returned.
*/

/**
* * void orb_list_update_removed_spells(Orb_list orb_list,
                                        Spell removed[],
                                        int qty_removed) *
*
* Removes references to the Spell objects contained in the array
* 'removed' that may exist in the Orb objects contained in the
* Orb_list 'orb_list'. 'qty_removed' is the quantity of elements in
* 'removed'. If 'removed' is NULL, all Spells are removed from all
* Orbs in 'orb_list'. Orbs may remain empty. Use before destroying
* the relevant Spell objects so Orbs do not reference non-existant
* Spells.
*/