/* spell_adt.h */

#ifndef SPELL_ADT_H
#define SPELL_ADT_H

#define SPELL_NAME_LEN 15

struct spell_adt;
typedef struct spell_adt *Spell;


Spell spell_create(const char *name, const char *description, int cost);
Spell spell_destroy(Spell spell);

int spell_get_cost(Spell spell);
const char *spell_get_name(Spell spell);
const char *spell_get_description(Spell spell);

int spell_set_cost(Spell spell, int cost);
int spell_set_name(Spell spell, const char *name);
int spell_set_description(Spell spell, const char *description);

#endif

/**
* Spells are objects that Player Characters can use in Clash via their
* equipped Orbs. Spells have a cost value that is used to know how much
* of a Character's MPT stat each Spell consumes when it's used.
*
* Spell objects represent each unique type of game Spell.
*/


/**
* * Spell spell_create(const char *name, const char *description,
                       int cost) *
*
* Creates a Spell object with a name given by the string 'name', a
* description given by the string 'description' and a cost given by
* 'cost', which must be positive. The name of the object won't be longer
* than SPELL_NAME_LEN characters.
* Returns: The created Item object or NULL on error.
*/

/**
* * Spell spell_destroy(Spell spell) *
*
* Destroys the Spell object given by 'spell' (if it is not NULL).
* Returns: NULL.
*/

/**
* * int spell_get_cost(Spell spell) *
*
* Returns: The cost value of the Spell object given by 'spell', or a
*  negative number if 'spell' is NULL.
*/

/**
* * const char *spell_get_name(Spell spell) *
*
* Returns: The name of the Spell object given by 'spell', or NULL if
*  'spell' is NULL.
*/

/**
* * const char *spell_get_description(Spell spell) *
*
* Returns: The description of the Spell object given by 'spell', or NULL
*  if 'spell' is NULL.
*/

/**
* * int spell_set_cost(Spell spell, int cost) *
*
* Sets the cost value of the Spell object 'spell' to the value of 'cost'.
* Returns: The new cost value, or a negative number on error.
*/

/**
* * int spell_set_name(Spell spell, const char *name) *
*
* Sets the name of the Spell object given by 'spell' to the contents of
* the string given by 'name'. The name won't be longer than
* SPELL_NAME_LEN characters. If 'name' is NULL the name of 'spell' will be
* empty.
* Returns: 0 on success, or a negative number on error.
*/

/**
* * int spell_set_description(Spell spell,
                              const char *description) *
*
* Sets the description of the Spell object given by 'spell' to the
* contents of the string given by 'description'. If 'description' is
* NULL the description of 'spell' will be empty.
* Returns: 0 on success, or a negative number on error.
*/