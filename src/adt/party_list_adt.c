/* party_list_adt.c */

#include "party_list_adt.h"
#include "player_party_adt.h"
#include "player_character_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct party_list_adt {
	Linked_list list;  // Nodes are of type Player_party
};


struct party_list_adt *party_list_create(void)
{
	struct party_list_adt *pl_parties;
	void (*node_destructor)(void *);
	Linked_list list;

	pl_parties = malloc(sizeof(*pl_parties));
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Could not create Party_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) player_party_destroy;
	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(pl_parties);
		return NULL;
	}

	pl_parties->list = list;

	return pl_parties;
}

struct party_list_adt *party_list_destroy(struct party_list_adt *pl_parties)
{
	if (pl_parties != NULL) {
		l_list_destroy(pl_parties->list);
		free(pl_parties);
	}

	return NULL;
}

int party_list_add(struct party_list_adt *pl_parties, Player_party party)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return -1;
	}
	if (party == NULL) {
		fprintf(stderr, "%s: Player_party object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(pl_parties->list, party);
}

int party_list_remove(struct party_list_adt *pl_parties, int position)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(pl_parties->list, position);
}

int party_list_total(struct party_list_adt *pl_parties)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(pl_parties->list);
}

Player_party party_list_get_party(struct party_list_adt *pl_parties,
                                  int position)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(pl_parties->list, position);
}

int party_list_find_party_pos(struct party_list_adt *pl_parties,
                              Player_party party)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_find_node_pos(pl_parties->list, party);
}

void party_list_update_removed_chars(Party_list pl_parties,
                                     Player_character removed[],
                                     int qty_removed)
{
	if (pl_parties == NULL) {
		fprintf(stderr, "%s: Party_list object is NULL\n", __func__);
		return;
	}

	Player_party party;
	int num_parties = l_list_qty_nodes(pl_parties->list);

	for (int i = 0; i < num_parties; ++i) {
		party = l_list_get_node_contents(pl_parties->list, i);
		player_party_update_removed_characters(party, removed, qty_removed);
	}
}