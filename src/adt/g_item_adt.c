/* g_item_adt.c */

#include "g_item_adt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct g_item_adt {
	int size_in_bag;
	char *description;
	char name[ITEM_NAME_LEN+1];
};


struct g_item_adt *item_create(const char *name, const char *description,
                               int size)
{
	if (size < 1) {
		fprintf(stderr, "%s: Size (%d) must be positive\n", __func__, size);
		return NULL;
	}

	struct g_item_adt *new_item;

	new_item = malloc(sizeof(*new_item));
	if (new_item == NULL) {
		fprintf(stderr, "%s: Could not create Item object\n", __func__);
		return NULL;
	}

	if (name == NULL) {
		name = "";
	}
	if (description == NULL) {
		description = "";
	}

	strncpy(new_item->name, name, ITEM_NAME_LEN);
	new_item->name[ITEM_NAME_LEN] = '\0';

	new_item->description = malloc(strlen(description) + 1);
	if (new_item->description == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for description\n",
		                __func__);
		free(new_item);
		return NULL;
	}
	strncpy(new_item->description, description, strlen(description) + 1);

	new_item->size_in_bag = size;

	return new_item;
}

struct g_item_adt *item_destroy(struct g_item_adt *item)
{
	if (item != NULL) {
		if (item->description != NULL) {
			free(item->description);
		}

		free(item);
	}

	return NULL;
}

int item_get_size(struct g_item_adt *item)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -1;
	}

	return item->size_in_bag;
}

const char *item_get_name(struct g_item_adt *item)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return NULL;
	}

	return item->name;
}

const char *item_get_description(struct g_item_adt *item)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return NULL;
	}

	return item->description;
}

int item_set_size(struct g_item_adt *item, int size)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -1;
	}
	if (size < 1) {
		fprintf(stderr, "%s: Size (%d) must be positive\n", __func__, size);
		return -2;
	}

	return item->size_in_bag = size;
}

int item_set_name(struct g_item_adt *item, const char *name)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -1;
	}
	if (name == NULL) {
		name = "";
	}

	strncpy(item->name, name, ITEM_NAME_LEN);
	item->name[ITEM_NAME_LEN] = '\0';

	return 0;
}

int item_set_description(struct g_item_adt *item, const char *description)
{
	if (item == NULL) {
		fprintf(stderr, "%s: Item object is NULL\n", __func__);
		return -1;
	}

	if (item->description != NULL) {
		free(item->description);
	}

	if (description == NULL) {
		description = "";
	}

	item->description = malloc(strlen(description) + 1);
	if (item->description == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for description\n",
		                __func__);
		return -2;
	}
	strncpy(item->description, description, strlen(description) + 1);

	return 0;
}