/* player_classes_adt.c */

#include "../player_class.h"
#include "player_classes_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

struct player_classes_adt {
	Linked_list list;  // Node contents are of type struct player_classes *
};

static ptrdiff_t sorting_member_offset_sg;  // Used by player_classes_sort

static int compare_i(const void *a, const void *b);
static int compare_f(const void *a, const void *b);


struct player_classes_adt *player_classes_create(void)
{
	struct player_classes_adt *pc;
	Linked_list list;

	pc = malloc(sizeof(*pc));
	if (pc == NULL) {
		fprintf(stderr, "%s: Can't create Player_classes object\n", __func__);
		return NULL;
	}

	list = l_list_create(free);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(pc);
		return NULL;
	}

	pc->list = list;

	return pc;
}

struct player_classes_adt *player_classes_destroy(struct player_classes_adt *pc)
{
	if (pc != NULL) {
		l_list_destroy(pc->list);
		free(pc);
	}

	return NULL;
}

int player_classes_add(Player_classes pc,
                       const struct player_class *root_stats)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return -1;
	}
	if (root_stats == NULL) {
		fprintf(stderr, "%s: root_stats is NULL\n", __func__);
		return -2;
	}

	struct player_class *new_pc = malloc(sizeof(*root_stats));

	if (new_pc == NULL) {
		fprintf(stderr, "%s: Could not create Player_class\n", __func__);
		return -3;
	}
	memcpy(new_pc, root_stats, sizeof(*new_pc));

	return l_list_add_end(pc->list, new_pc);
}

int player_classes_remove(Player_classes pc, int pos)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(pc->list, pos);
}

struct player_class *player_classes_get_class(struct player_classes_adt *pc,
                                              int pos)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return NULL;
	}

	return (struct player_class *)l_list_get_node_contents(pc->list, pos);
}

int player_classes_total(struct player_classes_adt *pc)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(pc->list);
}

int player_classes_find_class_pos(Player_classes pc,
                                  const struct player_class *pclass)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return -1;
	}
	if (pclass == NULL) {
		fprintf(stderr, "%s: class is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(pc->list, pclass);
}

int player_classes_sort(Player_classes pc, enum sort_by_options sort_by,
                        enum order_options order)
{
	if (pc == NULL) {
		fprintf(stderr, "%s: Player_classes object is NULL\n", __func__);
		return -1;
	}
	if (sort_by >= NUM_SORT_BY_OPTIONS) {
		return -2;
	}
	if (order != ASCENDING && order != DESCENDING) {
		return -3;
	}

	static const ptrdiff_t player_class_member_offsets[] =
		{[BY_TYP] = offsetof(struct player_class, type),
		 [BY_ATK] = offsetof(struct player_class, atk),
		 [BY_SPD] = offsetof(struct player_class, spd),
		 [BY_ITL] = offsetof(struct player_class, itl),
		 [BY_MPT] = offsetof(struct player_class, mpt),
		 [BY_DEF] = offsetof(struct player_class, def),
		 [BY_HPT] = offsetof(struct player_class, hpt)};
	int (*compare_fn)(const void *, const void *);
	struct player_class **sorted_classes;  // Dynamic array of pointers to player_class
	int i;
	int num_classes = l_list_qty_nodes(pc->list);

	if (num_classes <= 0) {
		fprintf(stderr, "%s: Player_classes object has no classes\n", __func__);
		return -4;
	}

	sorted_classes = (struct player_class **)l_list_to_array(pc->list, L_FLUSH);
	if (sorted_classes == NULL) {
		fprintf(stderr, "%s: Could not retrieve contents array\n", __func__);
		return -5;
	}

	sorting_member_offset_sg = player_class_member_offsets[sort_by];

	if (sort_by == BY_TYP) {
		compare_fn = compare_i;  // player_class.type is int
	}
	else {
		compare_fn = compare_f;  // The rest of members are floats
	}

	qsort(sorted_classes, num_classes, sizeof(sorted_classes[0]), compare_fn);

	if (order == ASCENDING) {
		for (i = num_classes - 1; i >= 0; --i) {
			l_list_add_top(pc->list, sorted_classes[i]);
		}
	}
	else {
		for (i = 0; i < num_classes; ++i) {
			l_list_add_top(pc->list, sorted_classes[i]);
		}
	}

	free(sorted_classes);
	return 0;
}

static int compare_i(const void *a, const void *b)
{
	struct player_class *pca = *((struct player_class **)a);
	struct player_class *pcb = *((struct player_class **)b);
	ptrdiff_t off_a = (ptrdiff_t) pca;
	ptrdiff_t off_b = (ptrdiff_t) pcb;

	off_a += sorting_member_offset_sg;
	off_b += sorting_member_offset_sg;


	int *ipca = (int *)off_a;
	int *ipcb = (int *)off_b;

	return *ipca - *ipcb;
}

static int compare_f(const void *a, const void *b)
{
	struct player_class *pca = *((struct player_class **)a);
	struct player_class *pcb = *((struct player_class **)b);
	ptrdiff_t off_a = (ptrdiff_t) pca;
	ptrdiff_t off_b = (ptrdiff_t) pcb;

	off_a += sorting_member_offset_sg;
	off_b += sorting_member_offset_sg;

	float *fpca = (float *)off_a;
	float *fpcb = (float *)off_b;

	float result = *fpca - *fpcb;

	if (result < 0.0) return -1;
	if (result == 0.0) return 0;
	if (result > 0.0) return 1;

	// Function should not reach this point
	printf("%s: Non valid floating point result %f\n", __func__, result);
	return -2;
}