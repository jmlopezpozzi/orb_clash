/* player_character_adt.c */

#include "player_character_adt.h"
#include "orb_adt.h"
#include "bag_adt.h"
#include "../player_class.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct player_character_adt {
	const struct player_class *pclass;
	int level;
	int stats_base[NUM_STATS_BASE];
	Orb cur_orb;
	Bag cur_bag;
	char name[CHAR_NAME_LEN+1];
};


struct player_character_adt *player_character_create(void)
{
	struct player_character_adt *new_char;

	new_char = calloc(1, sizeof(*new_char));
	if (new_char == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for Player_Character\n",
		                __func__);
		return NULL;
	}

	return new_char;
}

struct player_character_adt
*player_character_destroy(struct player_character_adt *pch)
{
	if (pch != NULL) {
		free(pch);
	}

	return NULL;
}

int player_character_set_level(struct player_character_adt *pch, int level)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}

	pch->level = level;
	player_character_build_stats_base(pch);

	return level;
}

int player_character_set_class(struct player_character_adt *pch,
                               const struct player_class *pclass)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}

	if (pclass == NULL) {
		pch->pclass = player_class_dummy();
	}
	else {
		pch->pclass = pclass;
	}

	player_character_build_stats_base(pch);

	return 0;
}

void player_character_build_stats_base(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return;
	}

	int level = pch->level;
	const struct player_class *pclass = pch->pclass;
	int *stats;

	stats = pch->stats_base;

	if (level <= 0 || pclass == NULL) {
		for (int i = 0; i < NUM_STATS_BASE; ++i) {
			stats[i] = 0;
		}
		return;
	}

	// Floats being converted to int on purpose (maybe do: add 1 or use ceil)
	stats[ATK] = level * pclass->atk;
	stats[SPD] = level * pclass->spd;
	stats[ITL] = level * pclass->itl;
	stats[MPT] = level * pclass->mpt;
	stats[DEF] = level * pclass->def;
	stats[HPT] = level * (pclass->hpt + pclass->hpt_floor);
}

int player_character_set_orb(struct player_character_adt *pch, Orb orb)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}

	pch->cur_orb = orb;

	return 0;
}

int player_character_set_bag(struct player_character_adt *pch, Bag bag)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}

	pch->cur_bag = bag;

	return 0;
}

int player_character_set_name(struct player_character_adt *pch,
                              const char *name)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}
	if (name == NULL) {
		name = "";
	}

	strncpy(pch->name, name, CHAR_NAME_LEN);
	pch->name[CHAR_NAME_LEN] = '\0';

	return 0;
}

int player_character_get_level(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return -1;
	}

	return pch->level;
}

int player_character_stat_base(struct player_character_adt *pch,
                               enum stats_idx stat)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return 0;
	}
	if (stat < 0 || stat >= NUM_STATS_BASE) {
		return 0;
	}

	return pch->stats_base[stat];
}

const int *player_character_get_stats_base(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return NULL;
	}

	return pch->stats_base;
}

const struct player_class
*player_character_get_class(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return NULL;
	}

	return pch->pclass;
}

Orb player_character_get_orb(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return NULL;
	}

	return pch->cur_orb;
}

Bag player_character_get_bag(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return NULL;
	}

	return pch->cur_bag;
}

const char *player_character_get_name(struct player_character_adt *pch)
{
	if (pch == NULL) {
		fprintf(stderr, "%s: Player_character object is NULL\n", __func__);
		return NULL;
	}

	return pch->name;
}