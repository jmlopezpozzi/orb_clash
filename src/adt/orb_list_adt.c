/* orb_list_adt.c */

#include "orb_list_adt.h"
#include "spell_adt.h"
#include "orb_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct orb_list_adt {
	Linked_list list;  // Node contents are of type Orb
};


struct orb_list_adt *orb_list_create(void)
{
	void (*node_destructor)(void *);
	struct orb_list_adt *orb_list;
	Linked_list list;

	orb_list = malloc(sizeof(*orb_list));
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Could not create Orb_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) orb_destroy;

	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(orb_list);
		return NULL;
	}

	orb_list->list = list;

	return orb_list;
}

struct orb_list_adt *orb_list_destroy(struct orb_list_adt *orb_list)
{
	if (orb_list != NULL) {
		l_list_destroy(orb_list->list);
		free(orb_list);
	}

	return NULL;
}

int orb_list_add(struct orb_list_adt *orb_list, Orb orb)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return -1;
	}
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(orb_list->list, orb);
}

int orb_list_remove(struct orb_list_adt *orb_list, int position)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(orb_list->list, position);
}

int orb_list_total(struct orb_list_adt *orb_list)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(orb_list->list);
}

Orb orb_list_get_orb(struct orb_list_adt *orb_list, int position)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(orb_list->list, position);
}

int orb_list_find_orb_pos(struct orb_list_adt *orb_list, Orb orb)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return -1;
	}
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(orb_list->list, orb);
}

void orb_list_update_removed_spells(struct orb_list_adt *orb_list,
                                    Spell removed[], int qty_removed)
{
	if (orb_list == NULL) {
		fprintf(stderr, "%s: Orb_list object is NULL\n", __func__);
		return;
	}

	int orb_pos;            //...in list
	int spell_pos;          //...in orb
	int *spells_to_remove;  // Dynamic array, will store positions of repeated spells that are to be removed from an orb
	int qty_to_remove = 0;
	int num_orbs;
	Orb orb;

	num_orbs = l_list_qty_nodes(orb_list->list);

	// If removed is NULL we just remove all spells from all orbs
	if (removed == NULL) {
		for (int i = 0; i < num_orbs; ++i) {
			orb = orb_list_get_orb(orb_list, i);

			while (orb_remove_spell(orb, 0) > 0) { ; }
		}
	}

	// Create spells_to_remove array
	{
		int biggest_orb = 0;  // Stores how many spells holds the orb with most spells
		int qty_spell;

		for (int i = 0; i < num_orbs; ++i) {
			orb = l_list_get_node_contents(orb_list->list, i);
			qty_spell = orb_get_num_spells(orb);
			if (qty_spell > biggest_orb) {
				biggest_orb = qty_spell;
			}
		}

		spells_to_remove = malloc(sizeof(*spells_to_remove) * biggest_orb);
		if (spells_to_remove == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for spells_to_remove"
			                "index array\n",
			                __func__);
			return;
		}
	}

	// In each orb
	for (orb_pos = 0; orb_pos < num_orbs; ++orb_pos) {
		orb = l_list_get_node_contents(orb_list->list, orb_pos);

		// For each removed spell
		for (int i = 0; i < qty_removed; ++i) {
			// See how many of them are in the current orb
			for (spell_pos = 0; spell_pos < orb_get_num_spells(orb);
			     ++spell_pos)
			{
				if (orb_get_spell(orb, spell_pos) == removed[i]) {
					spells_to_remove[qty_to_remove] = spell_pos;
					++qty_to_remove;
				}
			}

			// And remove them
			for (int j = 0; j < qty_to_remove; ++j) {
				orb_remove_spell(orb, spells_to_remove[j] - j);  // Taking into account that each removal advances position of subsequent spell references in orb
			}
			qty_to_remove = 0;
		}
	}

	free(spells_to_remove);
}