/* spell_adt.c */

#include "spell_adt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct spell_adt {
	int cost_in_mp;
	char *description;
	char name[SPELL_NAME_LEN+1];
};


struct spell_adt *spell_create(const char *name, const char *description,
                               int cost)
{
	if (cost < 1) {
		fprintf(stderr, "%s: Cost (%d) must be positive\n", __func__, cost);
		return NULL;
	}

	struct spell_adt *new_spell;

	new_spell = malloc(sizeof(*new_spell));
	if (new_spell == NULL) {
		fprintf(stderr, "%s: Could not create Spell object\n", __func__);
		return NULL;
	}

	if (name == NULL) {
		name = "";
	}
	if (description == NULL) {
		description = "";
	}

	strncpy(new_spell->name, name, SPELL_NAME_LEN);
	new_spell->name[SPELL_NAME_LEN] = '\0';

	new_spell->description = malloc(strlen(description) + 1);
	if (new_spell->description == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for description\n",
		                __func__);
		free(new_spell);
		return NULL;
	}
	strncpy(new_spell->description, description, strlen(description) + 1);

	new_spell->cost_in_mp = cost;

	return new_spell;
}

struct spell_adt *spell_destroy(struct spell_adt *spell)
{
	if (spell == NULL) {
		return NULL;
	}

	if (spell->description != NULL) {
		free(spell->description);
	}
	free(spell);

	return NULL;
}

int spell_get_cost(struct spell_adt *spell)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -1;
	}

	return spell->cost_in_mp;
}

const char *spell_get_name(struct spell_adt *spell)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return NULL;
	}

	return spell->name;
}

const char *spell_get_description(struct spell_adt *spell)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return NULL;
	}

	return spell->description;
}

int spell_set_cost(struct spell_adt *spell, int cost)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -1;
	}

	return spell->cost_in_mp = cost;
}

int spell_set_name(struct spell_adt *spell, const char *name)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -1;
	}

	if (name == NULL) {
		name = "";
	}

	strncpy(spell->name, name, SPELL_NAME_LEN);
	spell->name[SPELL_NAME_LEN] = '\0';

	return 0;
}

int spell_set_description(struct spell_adt *spell, const char *description)
{
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -1;
	}

	if (spell->description != NULL) {
		free(spell->description);
	}

	if (description == NULL) {
		description = "";
	}

	spell->description = malloc(strlen(description) + 1);
	if (spell->description == NULL) {
		fprintf(stderr, "%s: Could not allocate memory for description\n",
		                __func__);
		return -2;
	}
	strncpy(spell->description, description, strlen(description) + 1);

	return 0;
}