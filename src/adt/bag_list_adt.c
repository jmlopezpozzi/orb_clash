/* bag_list_adt.c */

#include "bag_list_adt.h"
#include "bag_adt.h"
#include "g_item_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>

struct bag_list_adt {
	Linked_list list;  // Node contents are of type Bag
};


struct bag_list_adt *bag_list_create(void)
{
	void (*node_destructor)(void *);
	struct bag_list_adt *bag_list;
	Linked_list list;

	bag_list = malloc(sizeof(*bag_list));
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Could not create Bag_list object\n", __func__);
		return NULL;
	}

	node_destructor = (void (*)(void *)) bag_destroy;

	list = l_list_create(node_destructor);
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(bag_list);
		return NULL;
	}

	bag_list->list = list;

	return bag_list;
}

struct bag_list_adt *bag_list_destroy(struct bag_list_adt *bag_list)
{
	if (bag_list != NULL) {
		l_list_destroy(bag_list->list);
		free(bag_list);
	}

	return NULL;
}

int bag_list_add(struct bag_list_adt *bag_list, Bag bag)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return -1;
	}
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(bag_list->list, bag);
}

int bag_list_remove(struct bag_list_adt *bag_list, int position)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(bag_list->list, position);
}

int bag_list_total(struct bag_list_adt *bag_list)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(bag_list->list);
}

Bag bag_list_get_bag(struct bag_list_adt *bag_list, int position)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(bag_list->list, position);
}

int bag_list_find_bag_pos(struct bag_list_adt *bag_list, Bag bag)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return -1;
	}
	if (bag == NULL) {
		fprintf(stderr, "%s: Bag object is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(bag_list->list, bag);
}

void bag_list_update_removed_items(struct bag_list_adt *bag_list,
                                   Item removed[], int qty_removed)
{
	if (bag_list == NULL) {
		fprintf(stderr, "%s: Bag_list object is NULL\n", __func__);
		return;
	}

	int bag_pos;            //...in list
	int item_pos;           //...in bag
	int *items_to_remove;   // Dynamic array, will store positions of repeated items that are to be removed from a bag
	int qty_to_remove = 0;
	int num_bags;
	Bag bag;

	num_bags = l_list_qty_nodes(bag_list->list);

	// If removed is NULL we just empty all the bags
	if (removed == NULL) {
		for (int i = 0; i < num_bags; ++i) {
			bag = l_list_get_node_contents(bag_list->list, i);

			while (bag_remove_item(bag, 0) > 0) { ; }
		}

		return;
	}

	// Create items_to_remove array
	{
		int biggest_bag = 0;  // Stores how many items holds the bag with more items
		int qty_items;

		for (int i = 0; i < num_bags; ++i) {
			bag = l_list_get_node_contents(bag_list->list, i);
			qty_items = bag_get_num_items(bag);
			if (qty_items > biggest_bag) {
				biggest_bag = qty_items;
			}
		}

		items_to_remove = malloc(sizeof(*items_to_remove) * biggest_bag);
		if (items_to_remove == NULL) {
			fprintf(stderr, "%s: Can't allocate memory for to_remove array\n",
			                __func__);
			return;
		}
	}

	// In each bag
	for (bag_pos = 0; bag_pos < num_bags; ++bag_pos) {
		bag = l_list_get_node_contents(bag_list->list, bag_pos);

		// For each removed item
		for (int i = 0; i < qty_removed; ++i) {
			// See how many of them are in the current bag
			for (item_pos = 0; item_pos < bag_get_num_items(bag);
                 ++item_pos)
			{
				if (bag_get_item(bag, item_pos) == removed[i]) {
					items_to_remove[qty_to_remove] = item_pos;
					++qty_to_remove;
				}
			}

			// And remove them
			for (int j = 0; j < qty_to_remove; ++j) {
				bag_remove_item(bag, items_to_remove[j] - j);  // Taking into account that each removal advances position of subsequent item references in bag and that the array comes sorted already
			}
			qty_to_remove = 0;
		}
	}

	free(items_to_remove);
}