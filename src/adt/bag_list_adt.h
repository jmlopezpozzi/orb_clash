/* bag_list_adt.h */

#ifndef BAG_LIST_ADT_H
#define BAG_LIST_ADT_H

#include "bag_adt.h"
#include "g_item_adt.h"

struct bag_list_adt;
typedef struct bag_list_adt *Bag_list;


Bag_list bag_list_create(void);
Bag_list bag_list_destroy(Bag_list bag_list);

int bag_list_add(Bag_list bag_list, Bag bag);
int bag_list_remove(Bag_list bag_list, int position);

int bag_list_total(Bag_list bag_list);
Bag bag_list_get_bag(Bag_list bag_list, int position);
int bag_list_find_bag_pos(Bag_list bag_list, Bag bag);

void bag_list_update_removed_items(Bag_list bag_list, Item removed[],
                                   int qty_removed);

#endif

/**
* Object that keeps a collection of Bag objects. A Bag_list object
* is meant to be a general list of unique Bag objects, removing one
* from the list erases said Bag.
*/


/**
* * Bag_list bag_list_create(void) *
*
* Creates a Bag_list object.
* Returns: The created Bag_list object or NULL on error.
*/

/**
* * Bag_list bag_list_destroy(Bag_list bag_list) *
*
* Destroys the Bag_list object given by 'bag_list' (if it is not
* NULL). Bag objects contained in 'bag_list' are also destroyed.
* Returns: NULL.
*/

/**
* * int bag_list_add(Bag_list bag_list, Bag bag) *
*
* Adds the Bag object given by 'bag' to the end of the list
* represented by the Bag_list object given by 'bag_list'.
* Returns: Quantity of Bags in the Bag_list after the operation if
*  successful or a negative number on error.
*/

/**
* * int bag_list_remove(Bag_list bag_list, int position) *
*
* Removes the Bag object in the position given by 'position' from
* the list represented by the Bag_list object given by 'bag_list'.
* The removed Bag object is destroyed.
* Returns: Quantity of Bags in the Bag_list after the operation if
*  successful or a negative number on error.
*/

/**
* * int bag_list_total(Bag_list bag_list) *
*
* Returns: Quantity of Bags in the Bag_list given by 'bag_list'
*  or a negative number if 'bag_list' is NULL.
*/

/**
* * Bag bag_list_get_bag(Bag_list bag_list, int position) *
*
* Returns: The Bag object found in the position given by 'position' of
*  the list represented by the Bag_list object given by 'bag_list',
*  or NULL on error.
*/

/**
* * int bag_list_find_bag_pos(Bag_list bag_list, Bag bag) *
*
* Returns: The position of the Bag given by 'bag' in the list
*  represented by the Bag_list given by 'bag_list'. If 'bag' is
*  not in 'bag_list' or if one or both of them are NULL, a negative
*  number is returned.
*/

/**
* * void bag_list_update_removed_items(Bag_list bag_list,
                                       Item removed[],
                                       int qty_removed) *
*
* Removes references to the Item objects contained in the array
* 'removed' that may exist in the Bag objects contained in the
* Bag_list 'bag_list'. 'qty_removed' is the quantity of elements in
* 'removed'. If 'removed' is NULL, all Items are removed from all
* Bags in 'bag_list'. Bags may remain empty. Use before
* destroying the relevant Item objects so Bags do not reference
* non-existant Items.
*/