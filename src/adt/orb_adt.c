/* orb_adt.c */

#include "orb_adt.h"
#include "spell_adt.h"
#include "linked_list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct orb_adt {
	Linked_list list;  // Contents are of type Spell (reference)
	char name[ORB_NAME_LEN+1];
};


struct orb_adt *orb_create(const char *name)
{
	struct orb_adt *new_orb;
	Linked_list list;

	new_orb = malloc(sizeof(*new_orb));
	if (new_orb == NULL) {
		fprintf(stderr, "%s: Could not create Orb object\n", __func__);
		return NULL;
	}

	list = l_list_create(NULL);  // Node contents will not be deleted on removal
	if (list == NULL) {
		fprintf(stderr, "%s: Could not create Linked_list\n", __func__);
		free(new_orb);
		return NULL;
	}

	if (name == NULL) {
		name = "";
	}

	new_orb->list = list;
	strncpy(new_orb->name, name, ORB_NAME_LEN);
	new_orb->name[ORB_NAME_LEN] = '\0';

	return new_orb;
}

struct orb_adt *orb_destroy(struct orb_adt *orb)
{
	if (orb != NULL) {
		l_list_destroy(orb->list);
		free(orb);
	}

	return NULL;
}

int orb_add_spell(struct orb_adt *orb, Spell spell)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -1;
	}
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -2;
	}

	return l_list_add_end(orb->list, spell);
}

int orb_remove_spell(struct orb_adt *orb, int position)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -1;
	}

	return l_list_remove_pos(orb->list, position);
}

int orb_get_num_spells(struct orb_adt *orb)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -1;
	}

	return l_list_qty_nodes(orb->list);
}

const char *orb_get_name(struct orb_adt *orb)
{
	if (orb == NULL) {
		return NULL;
	}

	return orb->name;
}

Spell orb_get_spell(struct orb_adt *orb, int position)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return NULL;
	}

	return l_list_get_node_contents(orb->list, position);
}

int orb_find_spell_pos(struct orb_adt *orb, Spell spell)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -1;
	}
	if (spell == NULL) {
		fprintf(stderr, "%s: Spell object is NULL\n", __func__);
		return -2;
	}

	return l_list_find_node_pos(orb->list, spell);
}

int orb_set_name(struct orb_adt *orb, const char *name)
{
	if (orb == NULL) {
		fprintf(stderr, "%s: Orb object is NULL\n", __func__);
		return -1;
	}
	if (name == NULL) {
		name = "";
	}

	strncpy(orb->name, name, ORB_NAME_LEN);
	orb->name[ORB_NAME_LEN] = '\0';

	return 1;
}