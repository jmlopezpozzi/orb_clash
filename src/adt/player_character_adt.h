/* player_character_adt.h */

#ifndef PLAYER_CHARACTER_ADT_H
#define PLAYER_CHARACTER_ADT_H

#include "orb_adt.h"
#include "bag_adt.h"
#include "../player_class.h"

#define CHAR_NAME_LEN 15

struct player_character_adt;
typedef struct player_character_adt *Player_character;


Player_character player_character_create(void);
Player_character player_character_destroy(Player_character pch);

int player_character_set_level(Player_character pch, int level);
int player_character_set_class(Player_character pch,
                               const struct player_class *pclass);
void player_character_build_stats_base(Player_character pch);
int player_character_set_orb(Player_character pch, Orb orb);
int player_character_set_bag(Player_character pch, Bag bag);
int player_character_set_name(Player_character pch, const char *name);

int player_character_get_level(Player_character pch);
int player_character_stat_base(Player_character pch,
                               enum stats_idx stat);
const int *player_character_get_stats_base(Player_character pch);
const struct player_class *player_character_get_class(Player_character pch);
Orb player_character_get_orb(Player_character pch);
Bag player_character_get_bag(Player_character pch);
const char *player_character_get_name(Player_character pch);

#endif

/**
* Player Characters are the units that take turns and perform actions
* in Clash. Player Characters have a level and a class that define the
* values of their base stats, values which are used (among others) to
* compute the outcome of different actions in Clash. Player Characters
* can also equip Orbs and Bags, in order to use the referenced Spells
* and Items, respectively.
*
* Player_character objects represent each unique Player Character.
*/


/**
* * Player_character player_character_create(void) *
*
* Creates a Player_character object. All values are initialized to 0.
* Returns: The created Player_character object, or NULL on error.
*/

/**
* * Player_character player_character_destroy(Player_character pch) *
*
* Destroys the Player_character object given by 'pch' it it is not NULL.
* Returns: NULL.
*/

/**
* * int player_character_set_level(Player_character pch, int level) *
*
* Sets the level value of the Player_character 'pch' to the one given by
* 'level'. Rebuilds the base stats.
* Returns: The new level value, or a negative number on error.
*/

/**
* * int player_character_set_class(Player_character pch,
                                   const struct player_class *pclass) *
*
* Sets the class of the Player_character 'pch' to be the object 'pclass',
* binding 'pclass' to 'pch'. Be sure to unreference 'pclass' from 'pch' if
* 'pclass' were to be erased. If 'pclass' is NULL the class of the
* Player_character object is set to be a special "dummy class" object.
* Rebuilds the base stats.
* Returns: 0, or a negative number on error.
*/

/**
* * void player_character_build_stats_base(Player_character pch) *
*
* Computes the value of each base stat in the Player_character 'pch'
* from its level value and referenced class.
*/

/**
* * int player_character_set_orb(Player_character pch, Orb orb) *
*
* Equips the Orb given by 'orb' to the Player_character 'pch', by
* referencing it. Be sure to unreference 'orb' from 'pch' if 'orb'
* were to be erased. If 'orb' is NULL, 'pch' is set to have no Orb
* equipped.
* Returns: 0, or a negative number on error.
*/

/**
* * int player_character_set_bag(Player_character pch, Bag bag) *
*
* Equips the Bag given by 'bag' to the Player_character 'pch', by
* referencing it. Be sure to unreference 'bag' from 'pch' if 'bag'
* were to be erased. If 'bag' is NULL, 'pch' is set to have no Bag
* equipped.
* Returns: 0, or a negative number on error.
*/

/**
* * int player_character_set_name(Player_character pch,
                                  const char *name) *
*
* Sets the name of the Player_character object 'pch' to the contents of
* the string given by 'name'. The name won't be longer than
* CHAR_NAME_LEN characters. If 'name' is NULL the name of 'pch' will be
* empty.
* Returns: 0, or a negative number on error.
*/

/**
* * int player_character_get_level(Player_character pch) *
*
* Returns: The level value of the Player_character object 'pch', or a
*  negative number on error.
*/

/**
* * int player_character_stat_base(Player_character pch,
                                   enum stats_idx stat) *
*
* Returns: The base stat value given by 'stat' of the Player_character
*  object 'pch', or 0 on error.
*/

/**
* * const int *player_character_get_stats_base(Player_character pch) *
*
* Returns: An array of NUM_STATS_BASE elements with the base stat values
*  of the Player_character object 'pch', or NULL on error.
*/

/**
* * const struct player_class *player_character_get_class(Player_character pch) *
*
* Returns: The class associated with the Player_character object 'pch',
*  or NULL if 'pch' is NULL.
*/

/**
* * Orb player_character_get_orb(Player_character pch) *
*
* Returns: The Orb currently equipped by the Player_character object
*  'pch', or NULL if 'pch' is NULL or has no Orb equipped.
*/

/**
* * Bag player_character_get_bag(Player_character pch) *
*
* Returns: The Bag currently equipped by the Player_character object
*  'pch', or NULL if 'pch' is NULL or has no Bag equipped.
*/

/**
* * const char *player_character_get_name(Player_character pch) *
*
* Returns: The name of the Player_character object 'pch' , or NULL if
*  'pch' is NULL.
*/