/* player_class.h */

#ifndef PLAYER_CLASS_H
#define PLAYER_CLASS_H

#define CLASS_NAME_LEN 15

enum stats_idx {ATK = 0, SPD, ITL, MPT, DEF, HPT, NUM_STATS_BASE};

struct player_class {
	float atk;
	float spd;
	float itl;
	float mpt;
	float def;
	float hpt;
	float hpt_floor;
	int type;
	char name[CLASS_NAME_LEN+1];
};

const struct player_class *player_class_dummy(void);

#endif


/**
* A Player Class is a set of data that define properties of a Player
* Character, primarily it's stats.
*/

/**
* *  const struct player_class *player_class_dummy(void) *
*
* Returns a pointer to a static struct player_class that represents the
* "dummy class". It is meant to be used as a Player Class placeholder
* for Player Characters that do not have a Player Class.
*/