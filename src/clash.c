/* clash.c */

#include "clash.h"
#include "player_class.h"
#include "adt/player_character_adt.h"
#include "adt/player_party_adt.h"
#include "adt/spell_adt.h"
#include "adt/orb_adt.h"
#include "adt/bag_adt.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX_ACTORS PARTY_NUM_POSITIONS * CLASH_NUM_SIDES

static struct {
	int side;
	Player_character character;
	int stats_mod_clash[NUM_STATS_BASE];
	int turn_status;
	int turn_gauge;
	unsigned last_turn;
	bool knock_out;
} actors[MAX_ACTORS];

static int num_actors = 0;
static int last_actor = -1;  // Previous turn actor
static int clash_winner = -1;
static bool turn_ended = false;

static int resolve_tied_turn_status(int closest[], int num_tied_closest);
static void compute_turn_gauges(void);


int clash_init(Player_party clash_sides[])
{
	int pos_in_party;
	int side = 0;
	int current = 0;

	memset(&actors, 0, sizeof(actors));

	for (side = 0; side < CLASH_NUM_SIDES; ++side) {
		for (pos_in_party = 0; pos_in_party < PARTY_NUM_POSITIONS;
		     ++pos_in_party)
		{
			actors[current].character =
				player_party_get_char(clash_sides[side], pos_in_party);
			if (actors[current].character == NULL) {
				continue;
			}

			actors[current].side = side;
			++current;
		}
	}

	last_actor = -1;
	num_actors = current;
	clash_winner = -1;
	turn_ended = false;

	compute_turn_gauges();
	for (int i = 0; i < num_actors; ++i) {
		actors[i].turn_status = actors[i].turn_gauge;
	}

	return 0;
}

int actors_fastest(void)
{
	int fastest = 0;
	int current;

	for (current = 1; current < num_actors; ++current) {
		if (actor_stat_total(current, SPD) > actor_stat_total(fastest, SPD)) {
			fastest = current;
		}
	}

	return fastest;
}

static void compute_turn_gauges(void)
{
	float speed;

	int fastest = actor_stat_total(actors_fastest(), SPD);

	for (int i = 0; i < num_actors; ++i) {
		if (actor_ko(i) == true) {
			continue;
		}

		speed = actor_stat_total(i, SPD);
		actors[i].turn_gauge = fastest * (fastest / speed);

		// This never tests true during init
		if (actors[i].turn_status > actors[i].turn_gauge) {
			actors[i].turn_status = actors[i].turn_gauge;
		}
	}
}

int clash_advance_turn(void)
{
	int turn_status_sub;
	int next = 0;
	int closest[MAX_ACTORS];   // actors with the lowest turn status
	int num_tied_closest = 0;  // how many are in closest

	// Start measuring from a non-KO actor
	for (int i = 0; i < num_actors; ++i) {
		if (actor_ko(i) == false) {
			closest[0] = i;
			break;
		}
	}

	for (int i = 0; i < num_actors; ++i) {
		if (actor_ko(i) == true) {
			continue;
		}

		if (actors[i].turn_status == actors[closest[0]].turn_status) {
			closest[num_tied_closest++] = i;
		}
		else
		if (actors[i].turn_status < actors[closest[0]].turn_status) {
			closest[0] = i;
			num_tied_closest = 1;
		}
	}

	if (num_tied_closest > 1) {
		next = resolve_tied_turn_status(closest, num_tied_closest);
	}
	else {
		next = closest[0];
	}

	turn_status_sub = actors[next].turn_status;
	for (int i = 0; i < num_actors; ++i) {
		actors[i].turn_status -= turn_status_sub;
	}

	actors[next].turn_status = actors[next].turn_gauge;

	for (int i = 0; i < num_actors; ++i) {
		if (actors[i].knock_out == false) {
			actors[i].last_turn++;
		}
	}
	actors[next].last_turn = 0;
	last_actor = next;

	return next;
}

/*
* Determines which actor will play next turn when more than one have the same
* turn_status value.
*/
static int resolve_tied_turn_status(int closest[], int num_tied_closest)
{
	// Find actors with highest SPD
	int fastest[MAX_ACTORS];   // Actors with the highest SPD
	int num_tied_fastest = 0;  // how many are in fastest

	fastest[0] = closest[0];

	for (int i = 0; i < num_tied_closest; ++i) {
		if (actor_stat_base(fastest[0], SPD) ==
		    actor_stat_base(closest[i], SPD))
		{
			fastest[num_tied_fastest++] = closest[i];
		}
		else
		if (actor_stat_base(fastest[0], SPD) <
		    actor_stat_base(closest[i], SPD))
		{
			fastest[0] = closest[i];
			num_tied_fastest = 1;
		}
	}

	if (num_tied_fastest > 1) {
		// If more than one, find actor who's last turn was earliest, priorizing
		// actors on the side opposed to the one that played last turn
		int same_side[MAX_ACTORS];
		int qty_side = 0;
		bool opposite_side = false;
		int earliest;

		for (int i = 0; i < num_tied_fastest; ++i) {
			if (actor_side(fastest[i]) != actor_side(last_actor)) {
				if (opposite_side == false) {
					opposite_side = true;
					qty_side = 0;
				}
				same_side[qty_side++] = fastest[i];
			}
			else
			if (opposite_side == false) {
				same_side[qty_side++] = fastest[i];
			}
		}

		earliest = same_side[0];
		for (int i = 1; i < qty_side; ++i) {
			if (actors[same_side[i]].last_turn > actors[earliest].last_turn) {
				earliest = same_side[i];
			}
		}

		return earliest;
	}

	return fastest[0];
}

int clash_compute_attack(int src_actor, int dst_actor)
{
	int damage;

	damage = actor_stat_total(src_actor, ATK) -
	         actor_stat_total(dst_actor, DEF) / 2;

	actors[dst_actor].stats_mod_clash[HPT] -= damage;

	printf("%s takes %d damage\n", actor_name(dst_actor), damage);

	if (actor_stat_total(dst_actor, HPT) <= 0) {
		actors[dst_actor].stats_mod_clash[HPT] =
			-actor_stat_base(dst_actor, HPT);
		actors[dst_actor].knock_out = true;

		printf("%s is KO\n", actor_name(dst_actor));

		// Checks if entire side is KO
		if (clash_all_ko(actor_side(dst_actor)) == true) {
			clash_end(actors[src_actor].side);
		}

	}

	return damage;
}

int clash_use_spell(int actor, Spell spell)
{
	if (spell_get_cost(spell) > actor_stat_total(actor, MPT)) {
		printf("%s has not enough MPt to use %s\n", actor_name(actor),
		       spell_get_name(spell));
		return actor_stat_total(actor, MPT);
	}

	actors[actor].stats_mod_clash[MPT] -= spell_get_cost(spell);
	printf("%s used %s!\n", actor_name(actor), spell_get_name(spell));

	return actor_stat_total(actor, MPT);
}

void clash_set_turn_ended(bool status)
{
	turn_ended = status;
}

bool clash_turn_ended(void)
{
	return turn_ended;
}

bool clash_all_ko(int side)
{
	for (int i = 0; i < num_actors; ++i) {
		if (actors[i].side == side && actors[i].knock_out == false) {
			return false;
		}
	}

	return true;
}

int clash_ended(void)
{
	return clash_winner;
}

void clash_end(int winner_side)
{
	clash_winner = winner_side;
}


int actors_quantity(void)
{
	return num_actors;
}

int actor_side(int actor)
{
	return actors[actor].side;
}

bool actor_ko(int actor)
{
	return actors[actor].knock_out;
}

int actor_stat_total(int actor, enum stats_idx stat)
{
	return actor_stat_base(actor, stat) + actors[actor].stats_mod_clash[stat];
}

int actor_stat_base(int actor, enum stats_idx stat)
{
	return player_character_get_stats_base(actors[actor].character)[stat];
	// Note: there is a player_character_stat_base(Player_character, int) function in "player_character_adt.h"
}

int actor_level(int actor)
{
	return player_character_get_level(actors[actor].character);
}

const int *actor_get_stats_base(int actor)
{
	return player_character_get_stats_base(actors[actor].character);
}

int *actor_get_stats_mod_clash(int actor)
{
	return &(actors[actor].stats_mod_clash[0]);
}

Orb actor_get_orb(int actor)
{
	return player_character_get_orb(actors[actor].character);
}

Bag actor_get_bag(int actor)
{
	return player_character_get_bag(actors[actor].character);
}

const char *actor_name(int actor)
{
	return player_character_get_name(actors[actor].character);
}


int actor_debug_get_turn_status(int actor)
{
	return actors[actor].turn_status;
}

int actor_debug_get_turn_gauge(int actor)
{
	return actors[actor].turn_gauge;
}

unsigned actor_debug_get_last_turn(int actor)
{
	return actors[actor].last_turn;
}

const struct player_class *actor_debug_get_class(int actor)
{
	Player_character character = actors[actor].character;

	return player_character_get_class(character);
}

void actor_debug_set_turn_status(int actor, int status)
{
	actors[actor].turn_status = status;
}