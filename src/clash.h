/* clash.h */

#include "player_class.h"
#include "adt/player_party_adt.h"
#include "adt/orb_adt.h"
#include "adt/spell_adt.h"
#include "adt/bag_adt.h"
#include <stdbool.h>

int clash_init(Player_party clash_sides[]);
int clash_advance_turn(void);
int clash_compute_attack(int src_actor, int dst_actor);
int clash_use_spell(int actor, Spell spell);
void clash_set_turn_ended(bool status);
bool clash_turn_ended(void);
bool clash_all_ko(int side);
int clash_ended(void);
void clash_end(int winner_side);

int actors_quantity(void);
int actors_fastest(void);
int actor_side(int actor);
bool actor_ko(int actor);
int actor_stat_total(int actor, enum stats_idx stat);
int actor_stat_base(int actor, enum stats_idx stat);
int actor_level(int actor);
const int *actor_get_stats_base(int actor);
int *actor_get_stats_mod_clash(int actor);
Orb actor_get_orb(int actor);
Bag actor_get_bag(int actor);
const char *actor_name(int actor);

int actor_debug_get_turn_status(int actor);
int actor_debug_get_turn_gauge(int actor);
unsigned actor_debug_get_last_turn(int actor);
const struct player_class *actor_debug_get_class(int actor);
void actor_debug_set_turn_status(int actor, int status);


/**
* This module contains the data that models the Clash and the functions
* to manipulate that data.
*/


/**
* * int clash_init(Player_party clash_sides[]) *
*
* Initializes Clash data using the Player_party objects found in the
* array 'clash_sides'. Each Player_Character of each Player_party in
* 'clash_sides' become an actor, and is given an index.
* Returns: 0, or a negative number on error.
*/

/**
* * void compute_turn_gauges(void) *
*
* Computes the turn_gauge value of each actor in Clash using their SPD
* stat. Elsewhere, this value is used to determine the order of the
* turns.
*/

/**
* * int clash_advance_turn(void) *
*
* Computes which actor will play next turn.
* Returns: The index of next turn's actor.
*/

/**
* * int clash_compute_attack(int src_actor, int dst_actor) *
*
* Computes an attack action from 'src_actor' to 'dst_actor', using a
* predefined formula to get a damage value. This value is substracted
* from 'dst_actor' Clash Hit Points (HPT stat). If HPT goes below 0,
* 'dst_actor' is set to be KO.
* Returns: The damage value.
*/

/**
* * int clash_use_spell(int actor, Spell spell) *
*
* Makes the actor defined by 'actor' use the Spell defined by 'spell' if
* said actor has enough Magic Points (MPT stat).
* Returns: The remaining Magic Point of the actor after using the spell.
*/

/**
* * void clash_set_turn_ended(bool status) *
*
* Sets the turn_ended flag to the value of 'status'.
*/

/**
* * bool clash_turn_ended(void) *
*
* Returns: The value of the turn_ended flag.
*/

/**
* * bool clash_all_ko(int side)
*
* Returns: True if all actors in the side indicated by 'side' are KO,
*  false otherwise.
*/

/**
* * int clash_ended(void) *
*
* Returns: The value of clash_winner, which is meant to be a negative
*  number while Clash is happening and the index of the winner side if
*  it has already ended.
*/

/**
* * void clash_end(int winner_side)
*
* Ends Clash, clearing data. Sets the value of clash_winner to the value
* of 'winner_side'.
*/

/**
* * int actors_quantity(void) *
* Returns: Total number of actors in Clash.
*/

/**
* * int actors_fastest(void) *
* Returns: index of the actor with the current highest SPD in Clash.
*/

/**
* * int actor_side(int actor) *
* Returns: The side of the actor with index 'actor'.
*/

/**
* * bool actor_ko(int actor) *
* Returns: Ture if actor with index 'actor' is KO.
*/

/**
* * int actor_stat_total(int actor, enum stats_idx stat) *
* Returns: The current in-Clash value of the stat given by the index
* 'stat' for the actor with the index 'actor'.
*/

/**
* * int actor_stat_base(int actor, enum stats_idx stat) *
* Returns: The base stat value of the stat given by the index 'stat' for
* the actor with the index 'actor'.
*/

/**
* * int actor_level(int actor) *
* Returns: The level value for the actor with index 'actor'.
*/

/**
* * const int *actor_get_stats_base(int actor) *
* Returns: An array of NUM_STATS_BASE elements with the base stat values
* for the actor with index 'actor'.
*/

/**
* * int *actor_get_stats_mod_clash(int actor) *
* Returns: An array of NUM_STATS_BASE elements with the current in-Clash
* stat values for the actor with index 'actor'.
*/

/**
* * Orb actor_get_orb(int actor) *
* Returns: The currently equipped Orb object of the actor with index
* 'actor'.
*/

/**
* * Bag actor_get_bag(int actor) *
* Returns: The currently equipped Bag object of the actor with index
* 'actor'.
*/

/**
* * const char *actor_name(int actor) *
* Returns: The name of the actor with index 'actor'.
*/


/**
* DEBUG FUNCTIONS
*/

/**
* * int actor_debug_get_turn_status(int actor) *
*
* Returns: The turn_status value for the actor with index 'actor'. This
*  is a value that decreases every turn and, usually, means that it is
*  the actor's turn when reaches 0.
*/

/**
* * int actor_debug_get_turn_gauge(int actor) *
*
* Returns: The turn_gauge value for the actor with index 'actor'. This
*  is the value that turn_status rises to after an actor has taken a turn.
*/

/**
* * unsigned actor_debug_get_last_turn(int actor) *
*
* Returns: A value indicating how many turns passed since the actor with
*  index 'actor' took a turn.
*/

/**
* * const struct player_class *actor_debug_get_class(int actor) *
* Returns: The Player Class of the actor with index 'actor'.
*/

/**
* * void actor_debug_set_turn_status(int actor, int status) *
*
* Sets the turn_status value of the actor with index 'actor' to the
* value given by 'status'.
*/