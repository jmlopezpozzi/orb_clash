#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>
#include "readline.h"

int read_line(char str[], int n)
{
	int ch;
	int i = 0;

	while (isblank(ch = getchar())) { ;	}
	while (ch != '\n' && ch != EOF) {
		if (i < n) {
			str[i++] = ch;
		}
		ch = getchar();
	}
	str[i] = '\0';
	return i;
}

int flushed_scanf_d(int error)
{
	int var;

	if (scanf("%d", &var) < 1) {
		var = error;
	}
	while (getchar() != '\n') { ; }

	return var;
}

float flushed_scanf_f(float error)
{
	float var;

	if (scanf("%f", &var) < 1) {
		var = error;
	}
	while (getchar() != '\n') { ; }

	return var;
}

bool accept_prompt(char accept_key)
{
	char input_key = '\0';

	scanf("%c", &input_key);
	while (getchar() != '\n') { ; }

	return (input_key == accept_key);
}