/* clash_mode.c */

#include "clash.h"
#include "adt/player_party_adt.h"
#include "adt/spell_adt.h"
#include "adt/orb_adt.h"
#include "adt/bag_adt.h"
#include "editor/editor_public.h"
#include "readline.h"
#include <stdio.h>

static void turn_of(int actor);
static int choose_target(int side);
static int option_attack(int actor);
static void option_use_magic(int actor);
static void option_bag(int actor);
static void option_defense(int actor);
static void option_pass(void);
static void option_view_stats(int actor);
static void option_view_stats_side(int side);
static void option_leave(int side);

static void option_debug_view(void);
static void debug_view_actors_info(void);
static int debug_actor_prompt();

int clash_mode(Player_party sides[])
{
	unsigned long turns_done = 0;  // Could overflow, does not break anything
	int actor;

	// Init clash
	if (clash_init(sides) < 0) {
		return -1;
	}

	for (;;) {
		// Compute turn
		clash_set_turn_ended(false);
		actor = clash_advance_turn();

		// Do turn actions
		turn_of(actor);
		++turns_done;

		// Check for end of clash
		if (clash_ended() > -1) {
			printf("\nSide %d (%s) wins!\n", clash_ended(),
			       player_party_get_name(sides[clash_ended()]));
			break;
		}
	}

	return turns_done;
}

static void turn_of(int actor)
{
	// Here, control would be given to the user or the CPU depending on actor side
	// Currently both sides are controlled by the user

	enum {ATTACK = 1, USE_MAGIC, USE_FLASH, DEFENSE, PASS, VIEW_STATS,
	                  DEBUG_VIEW, LEAVE};
	int option;

	while (clash_turn_ended() == false) {
		putchar('\n');
		printf("Turn of %s (side %d)\n", actor_name(actor), actor_side(actor));

		// Show menu
		printf("(%d-Attack, %d-Magic, %d-Bag, %d-Defend, %d-Pass,\n"
		       " %d-View Stats, %d-Debug View, %d-Leave)\n:",
		       ATTACK, USE_MAGIC, USE_FLASH, DEFENSE, PASS, VIEW_STATS,
		       DEBUG_VIEW, LEAVE);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case ATTACK:
			option_attack(actor);
			break;

		case USE_MAGIC:
			option_use_magic(actor);
			break;

		case USE_FLASH:
			option_bag(actor);
			break;

		case DEFENSE:
			option_defense(actor);
			break;

		case PASS:
			option_pass();
			break;

		case VIEW_STATS:
			option_view_stats_side(actor_side(actor));
			break;

		case DEBUG_VIEW:
			option_debug_view();
			break;

		case LEAVE:
			option_leave(actor_side(actor));
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}

static int choose_target(int side)
{
	int targets_in_side[PARTY_NUM_POSITIONS];
	int num_targets = 0;
	int target;

	for (int i = 0; i < actors_quantity(); ++i) {
		if (actor_side(i) == side) {
			targets_in_side[num_targets++] = i;
		}
	}

	for (int i = 0; i < num_targets; ++i) {
		printf("%d %-16s", i + 1, actor_name(targets_in_side[i]));
		actor_ko(targets_in_side[i]) == true ? printf("  KO\n") : printf("\n");
	}

	target = flushed_scanf_d(0);  // 0 instead of the usual FSD_ERR_VAL
	--target;
	if (target < 0 || target >= num_targets) {
		printf("%s: no target number %d\n", __func__, target + 1);
		return -1;
	}

	target = targets_in_side[target];
	if (actor_ko(target) == true) {
		printf("%s: Target %d %s is KO\n", __func__, target + 1,
		       actor_name(target));
		return -2;
	}

	return target;
}

static int option_attack(int actor)
{
	int target = -1;

	target = choose_target(1 - actor_side(actor));
	if (target < 0) {
		return target;
	}

	// Set end of turn
	clash_set_turn_ended(true);

	return clash_compute_attack(actor, target);
}

static void option_use_magic(int actor)
{
	Orb orb;
	Spell spell;
	
	orb = actor_get_orb(actor);
	if (orb == NULL) {
		printf("No orb loaded\n");
		return;
	}

	spell = ep_choose_spell_from_orb_obj(orb, "use");
	if (spell == NULL) {
		return;
	}

	clash_use_spell(actor, spell);

	// End turn
	clash_set_turn_ended(true);
}

static void option_bag(int actor)
{
	Bag bag;
	Item item;
	int item_pos;

	bag = actor_get_bag(actor);
	if (bag == NULL) {
		printf("%s is carrying no bag\n", actor_name(actor));
		return;
	}
	if (bag_get_num_items(bag) < 1) {
		printf("No items in bag\n");
		return;
	}

	item_pos = ep_choose_item_from_bag_idx(bag, "use");  // Actual use of idx version!
	if (item_pos < 0) {
		return;
	}
	item = bag_get_item(bag, item_pos);

	bag_remove_item(bag, item_pos);
	printf("%s used %s!\n", actor_name(actor), item_get_name(item));

	// End turn
	clash_set_turn_ended(true);
}

static void option_defense(int actor)
{
	printf("%s defends!\n", actor_name(actor));
	// actor_defend(actor);

	// End turn
	clash_set_turn_ended(true);
}

static void option_pass(void)
{
	printf("Pass\n");
	// End turn
	clash_set_turn_ended(true);
}

static void option_view_stats(int actor)
{
	const int *stats_base;
	const int *stats_mod_clash;

	printf("%-16s Level %3d\n", actor_name(actor), actor_level(actor));
	printf("     ATK    SPD    ITL    MPT    DEF    VIT\n");

	printf("BASE");
	stats_base = actor_get_stats_base(actor);
	for (int i = 0; i < NUM_STATS_BASE; ++i) {
		printf(" %6d", stats_base[i]);
	}
	putchar('\n');

	printf("MODC");
	stats_mod_clash = actor_get_stats_mod_clash(actor);
	for (int i = 0; i < NUM_STATS_BASE; ++i) {
		printf(" %6d", stats_mod_clash[i]);
	}
	putchar('\n');

	printf("TOTL");
	for (int i = 0; i < NUM_STATS_BASE; ++i) {
		printf(" %6d", stats_base[i] + stats_mod_clash[i]);
	}
	putchar('\n');
}

static void option_view_stats_side(int side)
{
	int allies[PARTY_NUM_POSITIONS];
	int num_allies = 0;

	for (int cur = 0; cur < actors_quantity(); ++cur) {
		if (actor_side(cur) == side) {
			allies[num_allies++] = cur;
		}
	}

	for (int cur = 0; cur < num_allies; ++cur) {
		putchar('\n');
		option_view_stats(allies[cur]);
	}
}

static void option_debug_view(void)
{
	enum {BACK = 0, ACTORS_INFO, ACTOR_STATS, ACTOR_ORB, ACTOR_BAG, ACTOR_CLASS,
	                ACTOR_CLASS_STATS, C_TURN_STATUS};
	int option = -1;
	int actor = -1;

	debug_view_actors_info();

	for (; option != BACK;) {
		putchar('\n');
		printf("(%d-View actors info, %d-View actor stats,\n"
               " %d-View actor Orb, %d-View actor Bag,\n"
		       " %d-View actor class info,\n"
		       " %d-View actor class info and stats, %d-Change turn status,\n"
		       " %d-Back)\n:",
		       ACTORS_INFO, ACTOR_STATS, ACTOR_ORB, ACTOR_BAG, ACTOR_CLASS,
		       ACTOR_CLASS_STATS, C_TURN_STATUS, BACK);
		option = flushed_scanf_d(FSD_ERR_VAL);

		switch (option) {
		case BACK:
			break;

		case ACTORS_INFO:
			debug_view_actors_info();
			break;

		case ACTOR_STATS:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				option_view_stats(actor);
			}
			break;

		case ACTOR_ORB:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				Orb orb = actor_get_orb(actor);

				printf("%s Orb (%s) contents:\n", actor_name(actor),
				       orb_get_name(orb));
				ep_show_orb_contents(orb, true);
			}
			break;

		case ACTOR_BAG:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				Bag bag = actor_get_bag(actor);

				printf("%s Bag (%s) contents:\n", actor_name(actor),
			           bag_get_name(bag));
				ep_show_bag_contents(bag, false);
			}
			break;

		case ACTOR_CLASS:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				printf("%s Class info:\n", actor_name(actor));
				ep_show_class_info(*actor_debug_get_class(actor), true);
			}
			break;

		case ACTOR_CLASS_STATS:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				option_view_stats(actor);
				printf("Class info:\n     "CLASSES_HEADER_STRING"     ");
				ep_show_class_info(*actor_debug_get_class(actor), false);
			}
			break;

		case C_TURN_STATUS:
			actor = debug_actor_prompt();
			if (actor >= 0) {
				int new_ts;

				printf("Enter new turn_status: ");
				new_ts = flushed_scanf_d(FSD_ERR_VAL);
				if (new_ts < 0 || new_ts > actor_debug_get_turn_gauge(actor)) {
					printf("Value must be positive and no higher than"
					       " turn_gauge (%d)\n",
					       actor_debug_get_turn_gauge(actor));
					break;
				}
				actor_debug_set_turn_status(actor, new_ts);
			}
			break;

		default:
			printf("Option %d not recognised\n", option);
			break;
		}
	}
}

static void debug_view_actors_info(void)
{
	const char *orb_name;
	const char *bag_name;

	printf("Id Side  Turn  Last  Name            Orb             Bag\n");
	for (int i = 0; i < actors_quantity(); ++i) {
		orb_name = orb_get_name(actor_get_orb(i));
		bag_name = bag_get_name(actor_get_bag(i));

		printf("%-3d%-3d%5d/%-5d %-3d%-16s%-16s%-16s", i, actor_side(i),
		       actor_debug_get_turn_status(i), actor_debug_get_turn_gauge(i),
		       actor_debug_get_last_turn(i), actor_name(i),
		       orb_name == NULL? "(none)" : orb_name,
		       bag_name == NULL? "(none)" : bag_name);
		actor_ko(i) == true ? printf("  KO\n") : putchar('\n');
	}
}

static int debug_actor_prompt()
{
	int choice;

	debug_view_actors_info();
	printf("Choose actor (by id): ");
	choice = flushed_scanf_d(FSD_ERR_VAL);  // This time is 0-indexed!
	if (choice < 0 || choice >= actors_quantity()) {
		printf("No actor with id %d\n", choice);
		return -1;
	}
	return choice;
}

static void option_leave(int side)
{
	// Lose clash
	clash_set_turn_ended(true);
	clash_end(1 - side);
}