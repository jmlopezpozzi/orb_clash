#ifndef READLINE_H
#define READLINE_H

#include <stdbool.h>

#define FSD_ERR_VAL -1  // To be used with flushed_scanf_d as default error value

int read_line(char str[], int n);
int flushed_scanf_d(int error);
float flushed_scanf_f(float error);
bool accept_prompt(char accept_key);

#endif

/**
* This module contains functions that deal with basic user data entry.
*/

/**
* * int read_line(char str[], int n) *
*
* Skips leading blank characters, then reads the remainder of the input
* line and sotres it in 'str'. Truncates the line if its length exceeds
* 'n'. The newline character is always discarded, and if there was only
* white-space characters entered before it the resulting string is empty.
* Returns: the number of characters stored.
*/

/**
* * int flushed_scanf_d(int error) *
* * float flushed_scanf_f(float error) *
*
* Scan one value using the %d or %f conversion specifier and return it
* as an int or float value after flushing the input buffer.
* Returns: The scanned value, or if the scanning fails, the value
*  specified by 'error'.
*/

/**
* * bool accept_prompt(char accept_key) *
*
* Takes the first character of user input and tests it against
* 'accept_key'. Flushes input buffer.
* Returns: The result of the test.
*/